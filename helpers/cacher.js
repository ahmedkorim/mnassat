import { matchRoutes } from "react-router-config";
import routes          from "src/configs/routes";

const LocaleCache = new Map();
const cache = ( duration = 0, api = false ) => ( req, res, next ) => {
    console.log( 'cacheing' );
    if ( duration !== 0 ) {
        const path = req.path;
        if ( !api ) {
            const cacheLifeList = matchRoutes( routes, path )
                .map( ( { route } ) => route.cacheLife || duration )

            // todo fix it
            duration = cacheLifeList.sort( ( a, b ) => b - a )[ 0 ] || duration
        }
        console.log( 'updated' );
        let key = '__express__' + ( req.originalURL || req.url );
        console.log( key, duration );
        let cachedResponse = LocaleCache.get( key )
        if ( cachedResponse ) {
            console.log( ' in cache' );
            console.log( cachedResponse.headers );
            // res.set( cachedResponse.headers )
            res.send( cachedResponse.body )
        } else {
            console.log( 'no in cache' );
            res.sendAndCash = ( { body, headers = {} } ) => {
                console.log( `cache life ${ duration }` );
                LocaleCache.set( key, { body, headers } )
                setTimeout( () => {
                    LocaleCache.delete( key )
                }, duration * 1000 *60 )
                res.send( body )
            }
            next()
        }
    } else {
        next()
    }
}
export default cache
