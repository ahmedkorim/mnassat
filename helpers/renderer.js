import { createGenerateClassName, createMuiTheme, MuiThemeProvider, } from '@material-ui/core/styles'
import { SheetsRegistry }                                             from 'jss';
import React                                                          from 'react';
import { renderToString }                                             from 'react-dom/server'
import JssProvider                                                    from 'react-jss/lib/JssProvider';
import { Provider }                                                   from "react-redux";
import { StaticRouter }                                               from "react-router";
import { matchRoutes, renderRoutes }                                  from "react-router-config";
import routes                                                         from "src/configs/routes";
import getStore                                                       from "src/store/store";
import { ServerStyleSheet }                                           from 'styled-components'
import App                                                            from "src/App";
import Routes                                                         from "src/configs/routes";
import serialize                                                      from 'serialize-javascript';


const renderer = async ( req, res, hostName ) => {
    const sheetsRegistry = new SheetsRegistry();

    const sheetsManager = new Map();

    const theme = createMuiTheme( {} )

    const generateClassName = createGenerateClassName();
    const store = getStore();

    const dataPromises = matchRoutes( routes, req.path )
        .map( ( { route } ) => route.loadData ? route.loadData( store ) : null )
        .map( promise => {
            if ( promise ) {
                return new Promise( ( resolve, _ ) => {
                    promise.then( resolve ).catch( resolve )
                } )
            }
        } )

    await Promise.all( dataPromises )

    const WrappedApp = ( <Provider store={ store }>
            <StaticRouter context={ {} } location={ req.path }>
                <JssProvider registry={ sheetsRegistry } generateClassName={ generateClassName }>
                    <MuiThemeProvider theme={ theme } sheetsManager={ sheetsManager }>
                        <App>
                            { renderRoutes( Routes ) }
                        </App>
                    </MuiThemeProvider>
                </JssProvider>
            </StaticRouter>
        </Provider>
    )

    const sheet = new ServerStyleSheet()
    const content = renderToString( sheet.collectStyles( WrappedApp ) )
    const css = sheetsRegistry.toString()
    const styledTags = sheet.getStyleTags();
    sheet.seal();
    const html = `
    <!doctype html>
    <html lang="ar">
    
     <meta charset="utf-8" />
    <link rel="shortcut icon" href="/favicon.ico" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <meta name="theme-color" content="#000000" />
    <!--
      manifest.json provides metadata used when your web app is installed on a
      user's mobile device or desktop. See https://developers.google.com/web/fundamentals/web-app-manifest/
    -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,700,900">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://fonts.googleapis.com/earlyaccess/notokufiarabic.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css">
      <style id="jss-server-side">${ css }</style>
    ${ styledTags }
    <head>
    </head>
    <body dir="rtl">
    <noscript><div class="no-script">
 this application realy heavly on javascript please enable it to contanue using the app
</div></noscript>
     <div id="root">${ content }</div>

      <script>
          window.INITIAL_STATE = ${ serialize( store.getState() ) }
        </script>
    <script src="${ hostName }:4443/bundle.js"></script>
     </body>
    </html>
    
    
    `

    res.sendAndCash( { body: html } )
}
export default renderer
