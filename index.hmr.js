import '@babel/polyfill';
import { createGenerateClassName, createMuiTheme, jssPreset, MuiThemeProvider } from "@material-ui/core/styles";
import { create }                                                               from "jss";
import rtl                                                                      from 'jss-rtl'
import React                                                                    from 'react';
import ReactDOM                    from 'react-dom';
import JssProvider                 from "react-jss/lib/JssProvider";
import { Provider }                from "react-redux";
import { renderRoutes }            from "react-router-config";
import { BrowserRouter as Router } from "react-router-dom";
import theme                       from "src/configs/Theme";
import App                         from "src/HotApp";
import getStore                    from "src/store/store";
import routes                      from "./src/configs/routes";
import * as serviceWorker          from './src/serviceWorker';


const jss = create( { plugins: [ ...jssPreset().plugins, rtl() ] } );
const generateClassName = createGenerateClassName();
const store = getStore()


ReactDOM.render(
    <JssProvider jss={ jss } generateClassName={ generateClassName }>
        <Provider store={ store }>
            <MuiThemeProvider theme={ theme }>
                <Router>
                    <App>
                        { renderRoutes( routes ) }
                    </App>
                </Router>
            </MuiThemeProvider>
        </Provider>
    </JssProvider>


    ,
    document.getElementById( 'root' )
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
