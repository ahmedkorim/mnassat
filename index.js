import '@babel/polyfill';
import 'react-app-polyfill/ie11';
import 'es5-shim'
import 'react-app-polyfill/stable'
import { createGenerateClassName, createMuiTheme, jssPreset, MuiThemeProvider } from "@material-ui/core/styles";
import { create }                                                               from "jss";
import rtl                                                                      from 'jss-rtl'
import React                       from 'react';
import ReactDOM                    from 'react-dom';
import JssProvider                 from "react-jss/lib/JssProvider";
import { renderRoutes }            from "react-router-config";
import { BrowserRouter as Router } from "react-router-dom";
import theme                       from "src/configs/Theme";
import getStore                    from "src/store/store";
import App                         from './src/App';
import routes                      from "./src/configs/routes";
import * as serviceWorker          from './src/serviceWorker';
import { Provider }                from 'react-redux'


const jss = create( { plugins: [ ...jssPreset().plugins, rtl() ] } );
const generateClassName = createGenerateClassName( );

const store = getStore( { preloadState: window.INITIAL_STATE } )
ReactDOM.render(
    <JssProvider jss={ jss } generateClassName={ generateClassName }>
        <Provider store={ store }>
            <MuiThemeProvider theme={ theme }>
                <Router>
                    <App>
                        { renderRoutes( routes ) }
                    </App>
                </Router>
            </MuiThemeProvider>
        </Provider>
    </JssProvider>
    ,
    document.getElementById( 'root' )
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
