require( 'ignore-styles' )
require( '@babel/register' )( {
    ignore: [ /(node_modules)/ ],
    presets: [ '@babel/preset-env', '@babel/preset-react' ],
    plugins: [
        "babel-plugin-transform-export-extensions",
        "@babel/plugin-proposal-class-properties", [
            "module-resolver", {
                "root": [ ".", "../shared" ],
                "cwd": "packagejson",
                "alias": {
                    "target": "./node_modules",
                    "src": "./src",
                    "react-hot-loader": "./node_modules/react-hot-loader",
                }
            }
        ] ]
} )

require( './server' )
