import '@babel/polyfill'
import Express          from 'express'
import fs               from 'fs'
import { createServer } from 'http';
import path             from 'path'
import cache            from "./helpers/cacher";
import render           from './helpers/renderer'
import proxy            from 'express-http-proxy';
import axios            from 'axios';

const hostName = process.env.NODE_ENV === 'development' ? 'http://localhost' : 'http://162.241.134.28';

console.log( hostName, 'host' );
const app = Express()
app.use( ( req, res, next ) => {
    res.setHeader( 'Access-Control-Allow-Origin', '*' );
    res.setHeader( 'Access-Control-Request-Method', '*' );
    res.setHeader( 'Access-Control-Allow-Methods', 'OPTIONS, GET' );
    res.setHeader( 'Access-Control-Allow-Headers', '*' );
    next()
} )
const server = createServer( app )
app.use( Express.json() )
app.use( Express.urlencoded( { extended: false } ) );

app.use( Express.static( path.join( __dirname, '.', 'views', 'dist', 'static' ) ) )
app.get( '/asset-manifest.json', ( req, res, next ) => {
    const fileStream = fs.createReadStream( path.join( __dirname, '.', 'views', 'dist', 'asset-manifest.json' ) )
    res.contentType( ' application/json' )
    fileStream.pipe( res )
} )
// app.get( '/(**/)?images', proxy( `${ hostName }:4443`, ) )
app.get( '/images/**', proxy( `${ hostName }:4443`, ) )
app.get( '/api/**', proxy( `${ hostName }:4443`, ) )


app.post( '/get-sms', async ( req, res, next ) => {
    const { phone_number, country_code } = req.body;
    let body = new URLSearchParams();
    body.set( 'api_key', '19tekUcy3Ph7sYbTF990bCo9vrEUsfET' );
    body.set( 'via', 'sms' );
    body.set( 'phone_number', phone_number );
    body.set( 'country_code', country_code );
    try {
        const { data } = await axios.post( 'https://api.authy.com/protected/json/phones/verification/start',
            body.toString()
            , {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'

                }
            } )
        res.json( data )

    } catch ( e ) {
        const error = e.response.data
        console.log( error );
        res.status( 422 )
            .send( { error } )
    }


} )

app.post( '/code-verify', async ( req, res, next ) => {
    const { phone_number, country_code, code } = req.body;
    console.log( req.body );
    let body = new URLSearchParams();
    body.set( 'country_code', country_code );
    body.set( 'phone_number', phone_number );
    body.set( 'verification_code', code );
    console.log( body.toString() );
    try {
        const { data } = await axios.get( 'https://gomenassat.com/wp-json/wp/v2/validate_twilio_otp?' + body.toString()

            , {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'

                }
            } )
        if ( data.success ) {
            res.json( data )
        } else {
            res.status( 404 ).send( data )
        }

    } catch ( e ) {
        const error = e.response.data
        console.log( error );
        res.status( 422 )
            .send( { error } )
    }


} )
app.post( '/register', async ( req, res ) => {
    const {
        username,
        email,
        password,
    } = req.body
    try {
        const { data } = await axios.post( 'https://gomenassat.com/wp-json/wp/v2/users/register',
            {
                username,
                email,
                password,

            } )
        console.log( data );
        res.json( data )


    } catch ( e ) {
        const error = e.response.data
        console.log( error );
        res.status( 422 )
            .send( { error } )
    }
} )

app.post( '/login', async ( req, res ) => {
    const {
        username,
        password
    } = req.body;
    try {
        const { data } = await axios.post( 'https://gomenassat.com/wp-json/jwt-auth/v1/token', {
            username,
            password
        } )
        if ( data.token ) {
            console.log( 'logged' );
            res.send( data )
        } else {
            console.log( 'incorrect' );

            res.status( 403 )
                .send( data )
        }
    } catch ( e ) {
        console.log( e.response.data );
        res.status( 403 ).send( e.response.data )

    }
} )

app.get( '/me', async ( req, res ) => {
    const Auth = req.headers[ 'authorization' ] || `Bearer ${ req.headers[ 'cookie' ] }`;
    console.log( req.headers );
    console.log( Auth );

    try {
        const { data } = await axios.get( 'https://gomenassat.com/wp-json/wp/v2/users/me', {
            headers: {
                Authorization: Auth
            }
        } )
        console.log( data );
        res.send( data )
    } catch ( e ) {
        console.log( e.response.data );
        res.status( 401 ).send( e.response.data )


    }
} )

app.get( '*', cache( 60 ), ( req, res ) => {
    render( req, res, hostName );
} )


server.listen( 4444, ( err ) => {

    if ( err ) return console.log( err );
    console.log( `server started > ${ hostName }:4444` );

} )

//



