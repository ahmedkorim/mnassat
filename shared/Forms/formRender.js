import React            from "react";
import ImagePicker      from "shared/ui/FileUploaders/imagePicker";
import FormFilePicker   from "shared/ui/FormFilePicker/FormFilePicker";
import LocationSelector from "shared/ui/LocatinSelector";
import ServiceSelect    from "src/Components/ui/CustomeSelect/CustomeSelect";
import InputField       from "src/Components/ui/InputField/InputField";
import SelectFromInput  from "src/Components/ui/SelectFromInput/SelectFromInput";


const inputSwitch = ( { type, htmlType, ...input }, changeHandler, value ) => {
    switch ( type ) {
        case 'text':
        case void 0:
        case 'email':
        case 'password':
            return <InputField
                type={ htmlType || type }
                changeHandler={ changeHandler( input.name ) }
                { ...input }
                value={ value[ input.name ] }
                key={ input.name }
            />
        case 'long_text':
            return <InputField
                rowsMax={ 4 }
                multiline
                changeHandler={ changeHandler( input.name ) }
                { ...input }
                type="text"
                value={ value[ input.name ] }
                key={ input.name }
            />

        case 'select':
            return <SelectFromInput
                key={ input.name }
                changeHandler={ changeHandler( input.name ) }
                value={ value[ input.name ] }
                allValues={ value }
                { ...input }
            >
            </SelectFromInput>

        case 'service_select':
            return <ServiceSelect
                key={ input.name }
                changeHandler={ changeHandler( input.name ) }
                value={ value[ input.name ] }
                { ...input }
            />
        case 'map':
            return <LocationSelector
                key={ input.name }
                changeHandler={ changeHandler( input.name ) }
                value={ value[ input.name ] }
                { ...input }
            />
        case 'images_picker':
            return <ImagePicker
                key={ input.name }
                changeHandler={ changeHandler( input.name ) }
                value={ value[ input.name ] }
                { ...input }
            />
        case 'file_picker':
            return <FormFilePicker
                key={ input.name }
                changeHandler={ changeHandler( input.name ) }
                value={ value[ input.name ] }
                { ...input }
            />
        default:
            return <div> will be implemented </div>
    }

}

const formRender = ( { body = [], value = {} }, changeHandler, eagerValidation = false, extras = {} ) => body
    .map( ( { gridClass, ...input } ) => <div key={ input.name } className={ gridClass || 'col-12' }>{ inputSwitch( {
        extras,
        ...input,
        eagerValidation
    }, changeHandler, value ) }</div> )

export default formRender
