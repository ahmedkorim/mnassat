// import currencyFromat from 'currency-format'

// export let currencyFormatISO = Object.keys(currencyFromat);

export const shouldBeNumber = ( value = '' ) => isNaN( value ) ? 'MUST BE A NUMBER' : parseFloat( value ) < .01 ? 'ENTER A POSITIVE NUMBER' : "";
export const requiredField = ( value = '' ) => {return ( value + '' ).trim() === '' ? 'هذا الحقل مطلوب' : ''}
export const matchFiledValue = ( fieldName, errorMessage = '' ) => ( value, values, name ) => {

    const standred = values[ fieldName ]
    return standred === value ? '' : errorMessage
}
export const exactNumber = ( chrCount ) => ( value = '' ) => value.length === chrCount ? '' : `MUST BE ${ chrCount } CHARACTERS`

/*export const limitedCharNumber = (min, max = 15) => (value = '') => value.length <= max && value.length >= min ?
 '' : `should be ` + (isFinite(max) ? ` form ${min} to ${max} characters` : `  ${min} characters minimum`)*/

export const limitedCharNumber = ( min, max = 15 ) => ( value = '' ) => ( value.length <= max && value.length >= min ?
    '' : `SHOULD BE ` + ( isFinite( max ) ? ` ${ max } CHARACTERS MAXIMUM` : `  ${ min } CHARACTERS MINIMUM    ` ) + ` ${ value.length }/${ max }` )
export const maxInput = ( max = 40 ) => ( value = '' ) => `${ value.length }/${ max }`
export const mustBeAnEmail = ( value ) => {
    return ( /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i ).test( value ) ? '' : `
PLEASE ENTER A VALID EMAIL`
}
// export const mustBeAValidISOcurrency = (curr) => {
//     return currencyFormatISO.indexOf(curr) >= 0 ? '' : 'PLEASE ENTER A VALID ISO CURRENCY CODE'
// }
export const breakInput = charNumber => val => typeof val === 'string' ? val.trim().length <= charNumber : false
export const defBreakInput = breakInput( 10 )

export const verifySpecialCharacter = ( val ) => {
    const reg = /^[0-9a-zA-Z]*$/g
    return reg.test( val ) ? '' : 'LETTERS AND DIGITS ONLY'
}

export const validLocation = ( { _latitude, _longitude } ) => {
    if ( _latitude === "" || _longitude === "" ) return `هذا الحقل مطلوب برجاء اختيار موقع`
}
