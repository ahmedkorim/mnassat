import { danger, success, warning } from "shared/styles/colors";

const handleInputState = ( {focus, dirty, feedback, touch, eagerValidation, value, requried} ) => {
    // touched  not dirty => showing that field is required warning
    // touched  and dirty showing error
    //focused show helper
    // dirty and no error
    if ( eagerValidation ) {
        return {
            feedbackColor: requried && ( feedback.error ? danger : success ),
            feed: feedback.error
        }
    } else {
        if ( touch && ( !dirty || !value ) ) {
            return {
                feedbackColor: requried && warning,
                feed: feedback.error || feedback.helper
            }
        }
        if ( dirty ) {
            return {
                feedbackColor: requried && ( feedback.error ? touch ? danger : warning
                    : success ),
                feed: ( focus && touch ) ? feedback.helper : feedback.error
            }
        }
    }

    return {}
}


export default handleInputState
