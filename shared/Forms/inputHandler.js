import { requiredField }  from "./formValidation";
import validatorsExecutor from "./validatorsExecutor";

const inputHandler = ( form, state, setState ) => {
    const changeHandler = ( name ) => ( { target: { value } } ) => {
        const filed = form.find( ( input ) => name === input.name );
        let error = null;
        let helper = null;
        if ( filed ) {

            let { validators = [], helpers, required } = filed
            validators = required ? [ requiredField, ...validators ] : validators
            error = validatorsExecutor( value, state.formValue, name, validators )
            helper = validatorsExecutor( value, state.formValue, name, helpers )
        }

        form = form.map( i => i.name === name ? {
            ...i,
            feedback: {
                error, helper
            }
        } : i )

        setState( {
            ...state,
            formValue: { ...state.formValue, [ name ]: value },
            formFeedback: { ...state.formFeedback, [ name ]: error }
        }, () => {
            if ( filed && filed.dependency ) this.reValidate( filed.dependency )

        } )
    }
    const reValidate = ( name ) => {
        if ( name ) return changeHandler( name )( { target: { value: state.formValue[ name ] } } )
        form.forEach( ( i ) => {
            changeHandler( i.name )( { target: { value: state.formValue[ i.name ] } } )
        } )
    }
    return {
        reValidate, changeHandler
    }
}


export default inputHandler;
