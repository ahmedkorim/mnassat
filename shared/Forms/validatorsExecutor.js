const validatorsExecutor = ( value, values, name, validators = [], i = 0 ) => {
    if ( !( validators instanceof Array ) ) return validators( value, values, name );
    let error = typeof validators[ i ] === 'function' ? validators[ i ]( value, values, name ) : '';
    if ( error !== '' ) {
        return error
    } else if ( i < validators.length ) {
        return validatorsExecutor( value, values, name, validators, i + 1 )
    } else {
        return ''
    }
}
export default validatorsExecutor
