import Slide              from "@material-ui/core/Slide";
import React              from "react";
import validatorsExecutor from "shared/Forms/validatorsExecutor";
import { requiredField }  from "./formValidation";


const withFromState = ( formState = { formValue: {}, formFeedback: {} }, form = [] ) => ( WrappedComponent ) => class extends React.Component {


    constructor ( props ) {
        super( props )
        this.state = { ...formState, eagerValidation: false }
        this.form = form
    }

    reValidate ( name ) {
        if ( name ) return this.changeHandler( name )( { target: { value: this.state.formValue[ name ] } } )
        this.form.forEach( ( i ) => {
            this.changeHandler( i.name )( { target: { value: this.state.formValue[ i.name ] } } )
        } )
    }

    updateFrom = ( newFrom ) => {
        this.form = [ ...newFrom ]
    }
    stopEagerValidation = () => {
        this.setState( {
            eagerValidation: false
        } )
    }
    validateNow = ( withStep = false, step = null ) => {
        return new Promise( resolve => {
            this.reValidate()
            this.setState( {
                eagerValidation: true
            }, () => {
                const formState = Object.keys( this.state.formFeedback ).filter( ( key ) => {
                    const field = this.form.find( ( { name } ) => name === key );
                    if ( !field ) {
                        console.error( 'invalid form stricture' + ` form =>${ name } key =>${ key }` )
                    }
                    return ( withStep && this.state.step !== void 0 ) ?
                        ( field || {} ).step === ( step || this.state.step )
                        : true;
                } ).reduce( ( acc, errorKey ) => acc && !this.state.formFeedback[ errorKey ], true )
                resolve( formState )
            } )
        } )

    }
    nextStep = () => {
        if ( this.state.step < this.state.lastStep ) {
            this.setState( prevState => ( {
                ...prevState,
                prevStep: prevState.step,
                step: ( prevState.step || 1 ) + 1
            } ) )
        }
    }
    prevStep = () => {
        console.log( this.state.step );
        if ( this.state.step >= 2 ) {
            this.setState( prevState => ( {
                ...prevState,
                prevStep: prevState.step,
                step: ( prevState.step || 1 ) - 1
            } ) )
        }
    }

    populate = ( formState ) => {
        this.setState( {
            formValue: {
                ...this.state.formValue,
                ...formState
            }
        } )
    }
    changeHandler = ( name ) => ( { target: { value } } ) => {
        const filed = this.form.find( ( input ) => name === input.name );
        let error = null;
        let helper = null;
        if ( filed ) {

            let { validators = [], helpers, required, valueDependacy } = filed
            valueDependacy && valueDependacy()
            validators = required ? [ requiredField, ...validators ] : validators
            error = validatorsExecutor( value, this.state.formValue, name, validators )
            helper = validatorsExecutor( value, this.state.formValue, name, helpers )
        }

        this.form = this.form.map( i => i.name === name ? {
            ...i,
            feedback: {
                error, helper
            }
        } : i )

        this.setState( ( prevState ) => ( {
            ...this.state,
            formValue: { ...prevState.formValue, [ name ]: value },
            formFeedback: { ...prevState.formFeedback, [ name ]: error }
        } ), () => {
            if ( filed && filed.dependency ) this.reValidate( filed.dependency )

        } )
    }


    render () {
        const {
            state: {
                formFeedback,
                formValue,
                eagerValidation,
                step = null,
                prevStep: preStep = null,
                lastStep = null
            },
            form,
            updateFrom,
            validateNow,
            changeHandler,
            nextStep,
            prevStep,
            stopEagerValidation,
            populate
        } = this;
        const direction = preStep > step ? 'right' : 'left'
        const renderFrom = step ? form.filter( i => i.step === step ) : form
        return <div className="position-relative overflow-hidden">
            <Slide direction={ preStep > step ? 'right' : 'left' } key={ step } timeout={ preStep ? 330 : 0 } in={ true } mountOnEnter unmountOnExi>
                <WrappedComponent
                    handleChange={ changeHandler }
                    value={ formValue }
                    eagerValidation={ eagerValidation }
                    form={ renderFrom }
                    validateNow={ validateNow }
                    feedback={ formFeedback }
                    step={ step }
                    populate={ populate }
                    nextStep={ nextStep }
                    prevStep={ prevStep }
                    lastStep={ lastStep }
                    updateFrom={ updateFrom }
                    stopEagerValidation={ stopEagerValidation }
                    renderSteppers={ ( Button ) => {
                        return <div className="d-flex align-items-center justify-content-center w-100">
                            {
                                ( step && step > 1 ) && <>
                                    <Button
                                        onClick={ prevStep }
                                    > الخطوه السابقه </Button>
                                    <span className="px-3"/>
                                </>
                            }
                            {
                                ( step && step < lastStep ) && <Button
                                    onClick={ nextStep }
                                >الخطوه التاليه </Button>

                            }
                        </div>
                    } }
                    { ...this.props }
                />
            </Slide>
        </div>

    }
}


export default withFromState;
