export const serialize = ( obj ) => {
    const str = [];
    for ( let p in obj )
        if ( obj.hasOwnProperty( p ) ) {
            str.push( encodeURIComponent( p ) + "=" + encodeURIComponent( obj[ p ] ) );
        }
    return str.join( "&" );
}

export const deserialize = ( queryString ) => {
    const query = {};
    let pairs = ( queryString[ 0 ] === '?' ? queryString.substr( 1 ) : queryString ).split( '&' );
    for ( var i = 0; i < pairs.length; i++ ) {
        var pair = pairs[ i ].split( '=' );
        query[ decodeURIComponent( pair[ 0 ] ) ] = decodeURIComponent( pair[ 1 ] || '' );
    }
    return query;
}
