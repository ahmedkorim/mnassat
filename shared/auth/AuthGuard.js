import React               from 'react'
import { Redirect, Route } from "react-router-dom";
import { getCookie }       from "shared/auth/cookies.helper";

const token = () => getCookie( '__ssr-auth' );

const PrivateRoute = ( Component, rest = {} ) => (
    <Route
        { ...rest }
        render={ props => !!token() ? <Component { ...props }/>
            :
            <Redirect to="/auth"/>
        }
    />
)

export default PrivateRoute

