export const primary = "#2699FB"
export const danger = "#D50000"
export const warning = "#FF6D00"
export const success = "#00C853"
