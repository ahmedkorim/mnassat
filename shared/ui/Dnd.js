import React                from 'react';
import { DragDropProvider } from "shared/utils/DragAndDropContext";
import styled               from "styled-components";



const DndBackdrop = styled.div`
position: fixed;
top: 0;
left: 0;
height: 100vh;
width:100vw;
z-index: 99;
background-color: rgba(0,0,0,.5);
`

class DnD extends React.Component {
    state = {
        dragged: false,
        dropped: false,
        timeout: null
    }

    toggleDnd = ( dnd = {} ) => {
        dnd = {
            ...this.state,
            ...dnd
        }
        this.setState( {
            ...dnd
        } )
    }
    handleDnd = ( e ) => {
        console.log( e );
        e.preventDefault();
        e.stopPropagation();
        switch ( e.type ) {
            case "dragstart":
            case"dragenter":
            case"dragover":
                return this.toggleDnd( { dragged: true } )
            case "drop": {
                const files = e.dataTransfer.files;
                return this.toggleDnd( { dropped: true } )
            }
            case "dragleave":
                return this.toggleDnd( { dropped: false, dragged: false } )
            default :
                void 0
        }

    }
    prevent = ( e ) => {
        e.stopPropagation();
        e.preventDefault()
        if ( e.type === 'dragover' ) {
            this.setState( {
                timeout: null
            } )
            window.clearTimeout( this.state.timeout )
            this.toggleDnd( {
                dragged: true
            } )
        }
        if ( e.type === 'drop' || e.type === 'dragleave' ) {
            const timeout = setTimeout( () => {
                if ( !this.state.timeout ) return
                console.log( 'drag leaved' );
                this.toggleDnd( {
                    dragged: false,
                    dropped: false
                } )
            }, 10 )
            this.setState( {
                timeout
            } )
        }
    }

    componentDidMount () {
        window.addEventListener( 'dragover', this.prevent );
        window.addEventListener( 'drop', this.prevent );
        window.addEventListener( 'dragleave', this.prevent );
    }

    componentWillUnmount () {
        window.removeEventListener( 'dragover', this.prevent );
        window.removeEventListener( 'drop', this.prevent );
        window.removeEventListener( 'dragleave', this.prevent );
    }

    render () {
        return (
            <DragDropProvider value={ this.state }>
                { this.state.dragged && <DndBackdrop/> }
                { this.props.children }
            </DragDropProvider>
        )
    }
}

export default DnD;
