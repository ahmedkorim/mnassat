import Icon                                from "@material-ui/core/Icon";
import PropTypes                           from 'prop-types'
import React                               from 'react';
import { primary, success, }               from 'shared/styles/colors'
import { DragDropConsumer }                from "shared/utils/DragAndDropContext";
import { greyColor, lightGrey, mainColor } from "src/styles/colors";
import styled, { css }                     from 'styled-components'

const Feedback = styled.div`
position: absolute;
top: 0;
left:0;
width: 100%;
height: 100%;
color: #fff ;
.AddIcon_file{
font-size:5rem;
}
`

const PickerWrapper = styled.div`
position: relative;
z-index: 120;
background-color:#fff;
width:100%;
height: 100%;
${
    ( { state, droppedStyles = '' } ) => {
        switch ( state ) {

            case 'dropped':
                return droppedStyles ? droppedStyles :
                    css`
                    background-color: ${ success };
                    border:1px dashed ${ lightGrey }`


            case 'drag-in':
                return droppedStyles ? droppedStyles : css`
                background-color: ${ lightGrey };
                border:1px dashed ${ success }
                ${ Feedback }{
                                color:${ mainColor } !important;

                }
                `
            default:
            case 'no-drag':
                return droppedStyles ? droppedStyles : css`
                background-color: ${ primary };
                border:1px dashed ${ greyColor }
                `
        }
    }

    }


input[type="file"]{
width: 0;
height: 0;
opacity: 0;
}
`


class FilePicker extends React.Component {

    state = {
        // no-drag drag-in droped
        dragState: 'no-drag',
        fileName: ''
    }
    handleChange = ( e ) => {
        const _files = e.target.files;
        const files = this.props.multiple ? _files : [ _files[ 0 ] ]
        this.props.filesHandler && this.props.filesHandler( files )
        this.setState( {
            dragState: 'dropped',
            fileName: this.props.multiple ? '' : _files[ 0 ].name

        } )

    }
    handleDnd = ( e ) => {
        e.preventDefault();
        switch ( e.type ) {
            case "dragstart":
            case"dragenter":
            case"dragover":
                if ( this.state.dragState !== 'drag-in' ) {
                    this.setState( {
                        dragState: 'drag-in'
                    } )
                }
                break;
            case "drop": {
                console.log(Array.from(e.dataTransfer.files));
                window.testFiles = (Array.from(e.dataTransfer.files))
                const _files = Array.from(e.dataTransfer.files).filter( ( { type } ) => !this.props.accept || this.props.accept.indexOf( type ) > -1 );
                const files = this.props.multiple ? _files : [ _files[ 0 ] ]

                this.props.filesHandler && this.props.filesHandler( files )
                console.log( _files[ 0 ].name );
                this.setState( {
                    dragState: 'dropped',
                    fileName: this.props.multiple ? '' : _files[ 0 ].name
                } )
            }

                break;
            case "dragleave":
                this.setState( {
                    dragState: 'no-drag'
                } )
                break;

            default :
                return void 0
        }

    }

    onClickHandler = ( e ) => {
        this.input.click()
    }

    render () {
        const { multiple, accept = null } = this.props;
        const { dragState, fileName } = this.state;
        return (
            <PickerWrapper
                className={ this.props.className || '' }
                onDrop={ this.handleDnd }
                onDragEnd={ this.handleDnd }
                onDragStart={ this.handleDnd }
                onDragOver={ this.handleDnd }
                onDragLeave={ this.handleDnd }
                onDragEnter={ this.handleDnd }
                onDrag={ this.handleDnd }
                onClick={ this.onClickHandler }
                state={ this.state.dragState }
                droppedStyles={ this.props.droppedStyles }
            >
                <DragDropConsumer>
                    {
                        ( { dragged, dropped } ) => {
                            return <>
                                <input type="file" accept={ accept } multiple={ multiple } onChange={ this.handleChange } ref={ node => this.input = node }/>
                                <Feedback className="d-flex flex-column justify-content-center align-items-center h-100">
                                    <div><Icon fontSize="large" className="AddIcon_file">cloud_upload</Icon></div>
                                    { ( dragState !== 'droped' && !fileName ) ? <div>اسحب وارمي او اضفط لاختيار الملفات</div> :
                                        <div>{ fileName } لقد اخترت الملف</div> }
                                </Feedback>
                            </>
                        }
                    }
                </DragDropConsumer>
            </PickerWrapper>

        )
    }
}

export default FilePicker;

FilePicker.propTypes = {
    droppedStyles: PropTypes.any,
    filesHandler: PropTypes.func.isRequired
}
