import { IconButton }      from "@material-ui/core";
import shadows             from "@material-ui/core/es/styles/shadows";
import Icon                from "@material-ui/core/Icon";
import Tooltip             from "@material-ui/core/Tooltip";
import PropTypes           from 'prop-types'
import React               from 'react';
import { danger, warning } from "shared/styles/colors";
import FilePicker          from "shared/ui/FileUploaders/FilePicker";

import styled, { css } from 'styled-components'

const Wrapper = styled.div`
margin-bottom: 1rem ;
position: relative;
.file_picker-image{
height: 170px;
}

 ${ ( { look } ) => {
    return look
} }; 
width: 100%;
`

const File = styled.div`
background:url(${ ( { uri } ) => uri });
background-size: cover;
box-shadow: ${ shadows[ 1 ] };
height: 100px;
width:100px;
margin: .7rem .7rem 1.5rem;
position: relative;
.file__remove-icon{
position: absolute;
top: 0;
left: 0;
transform: translate3d(-50%,-50%,0);
width:35px !important;
height:35px !important;
padding: 0;
z-index: 99;
transition: all ease-in-out .4s;
background-color: ${ danger };
color: #fff;
box-shadow: ${ shadows[ 1 ] };
&:hover{
background-color: ${ danger };
color: #fff;
box-shadow: ${ shadows[ 4 ] };
}

}
`
const FilesWrapper = styled.div`

margin-bottom: 1rem;
width: 100%;
max-height: 300px;
overflow: auto;
.files__scroller{
display: flex;
padding: .7rem ;
flex-wrap: wrap;
}

`

class ImagePicker extends React.Component {
    state = {
        droppedStyles: "",
        files: [],
        imagesURIs: [],
    }

    componentWillMount () {
        this.fileHandler( this.props.value )
    }

    remove = ( i ) => {
        const files = this.state.files.filter( ( { name, lastModified } ) => name + lastModified !== i )
        const imagesURIs = this.state.imagesURIs.filter( ( { id } ) => id !== i );
        console.log( i );
        this.setState( {
            files,
            imagesURIs
        }, () => {
            this.props.changeHandler( {
                target: {
                    value: this.state.files
                }
            } )
        } )
    }

    render () {
        const { look = ``, feedback ,eagerValidation } = this.props;
        const error = ( feedback || {} ).error
        return (
            <Wrapper look={ look }>
                <h6 className="text-right">{ this.props.label }</h6>
                <FilesWrapper>
                    <div className="files__scroller">
                        {
                            this.state.imagesURIs.map( i => <File key={ i.id } uri={ i.data }
                            >
                                <Tooltip
                                    title="احذف هذا الملف"
                                    children={ <IconButton onClick={ () => this.remove( i.id ) } className="file__remove-icon"
                                                           color="secondary"><Icon>close</Icon></IconButton> }
                                />
                            </File> )
                        }
                    </div>
                </FilesWrapper>
                <div className="file_picker-image">
                    <FilePicker multiple
                                accept={ [ 'image/jpeg', 'image/png', 'image/jpg' ] }
                                filesHandler={ this.fileHandler }/>
                </div>
                { eagerValidation && <div className="text-right alert-danger mt-2">
                    { error }
                </div> }
            </Wrapper>

        )
    }

    fileHandler = async ( files = [] ) => {
        this.setState( {
            files: [],
            imagesURIs: []
        }, () => {
            for ( const file of files ) {
                const { name, lastModified } = file;
                if ( file instanceof Blob ) {
                    const reader = new FileReader()
                    reader.onload = ev => {


                        this.setState( prevState => ( {
                            ...prevState,
                            files: [ ...prevState.files, file ],
                            imagesURIs: [ ...prevState.imagesURIs, {
                                id: name + lastModified,
                                data: ev.target.result
                            } ]
                        } ), () => {
                            this.props.changeHandler( {
                                target: {
                                    value: this.state.files
                                }
                            } )

                        } )
                    }
                    reader.readAsDataURL( file )
                }
            }
        } )
    }
}

export default ImagePicker;

ImagePicker.propTypes = {
    changeHandler: PropTypes.any.isRequired,
    look: PropTypes.any.isRequired
}

ImagePicker.defaultProps = {
    look: () => ``
}
