import React      from 'react'
import FilePicker from "shared/ui/FileUploaders/FilePicker"
import styled     from 'styled-components';

const Wrapper = styled.div`
height: 250px;
`
const FormFilePicker = props => {

    const { label, eagerValidation, feedback, changeHandler } = props;
    const error = (feedback || {}).error
    return (
        <div>
            <h6 className="text-right">{ label }</h6>
            <Wrapper>
                <FilePicker
                    accept="application/pdf"
                    filesHandler={ ( files ) => changeHandler( {
                    target: {
                        value: files
                    }
                } ) }/>
            </Wrapper>
            { eagerValidation && <div className="text-right alert-danger mt-2">
                { error }
            </div> }
        </div>
    )
}
export default FormFilePicker
