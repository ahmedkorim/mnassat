import PropTypes              from 'prop-types'
import React                  from 'react';
import { defaultMapLocation } from "src/configs/maps/googleMaps";
import styled                 from 'styled-components';

const MapWrapper = styled.div`
width: 100%;
height: 400px;
`

class LocationSelector extends React.Component {
    state = {
        mapReady: false,

    }


    componentWillUnmount () {
        this.map = null
    }

    initMap = () => {
        const that = this;
        if ( !window.google ) return;
        const center = new window.google.maps.LatLng( defaultMapLocation.lat, defaultMapLocation.lng );
        this.map = new window.google.maps.Map( this.mapWrapper, {
            center: center,
            zoom: 5,
            mapTypeId: window.google.maps.MapTypeId.ROADMAP
        } );
        this.setState( { mapReady: true } );

        this.infoWindow = new window.google.maps.InfoWindow()
        google.maps.event.addListener( this.map, 'click', function ( event ) {
            that.handler( event.latLng );
        } );
        this.createMarker( this.props.value )
    };
    handler = ( { lat, lng } ) => {
        const _latitude = lat && lat()
        const _longitude = lng && lng()
        this.props.changeHandler( {
            target: {
                value: { _latitude, _longitude }
            }
        } )
    }
    createMarker = ( { _latitude: latitude, _longitude: longitude } ) => {
        const that = this;
        if ( !window.google ) return;
        this.marker && this.marker.setMap( null )
        this.marker = null

        let latLng = new window.google.maps.LatLng( +latitude, +longitude );

        const marker = new window.google.maps.Marker( {
            position: latLng,
            map: that.map
        } )
        that.marker = marker;
    }

    componentWillUpdate ( nextProps, nextState, nextContext ) {
        const nextMap = nextProps.value || {}
        const currentMap = this.props.value || {}
        if ( nextMap._longitude !== currentMap._longitude && nextMap._latitude !== currentMap._latitude ) {
            this.createMarker( nextMap )
        }
    }

    componentDidMount () {
        this.initMap()
    }

    render () {
        const { label, eagerValidation, feedback } = this.props;
        const error = (feedback || {}).error

        return (
            <div>
                <h6 className="text-right">{ label }</h6>
                <MapWrapper role="application" ref={ node => this.mapWrapper = node }/>
                { eagerValidation && <div className="text-right alert-danger mt-2">
                    { error }
                </div> }
            </div>
        )
    }
}

export default LocationSelector;

LocationSelector.propTypes = {
    changeHandler: PropTypes.func.isRequired,
    value: PropTypes.shape( {
        _longitude: PropTypes.any.isRequired,
        _latitude: PropTypes.any.isRequired,
    } ).isRequired
}
