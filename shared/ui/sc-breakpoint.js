import { css } from 'styled-components';

export const size = {
    xs: 370,
    sm: 670,
    md: 990,
    lg: 1200,
    xl: 1400

}
export const above = Object.keys( size ).reduce( ( acc, label ) => {
    acc[ label ] = ( ...args ) => css`
      @media (min-width:${ size[ label ] }px) {
      ${ css( ...args ) }
      }
`
    return acc;
}, {} )

export const WindowSize = () => {
    if ( typeof window !== void 0 + '' ) {
        return {
            width: window.innerWidth,
            height: window.innerHeight
        }
    } else {
        return {
            width: 1200,
            height: 600
        }
    }

}
export const below = Object.keys( size ).reduce( ( acc, label ) => {
    acc[ label ] = ( ...args ) => css`
      @media (max-width:${ size[ label ] }px) {
      ${ css( ...args ) }
      }
`
    return acc;
}, {} )
