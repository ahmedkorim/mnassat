import React from "react";

const dragDropContext = React.createContext( {
    dragged: false,
    dropped: false,
    toggleDnd: () => {}
} )

export const DragDropProvider = dragDropContext.Provider
export const DragDropConsumer = dragDropContext.Consumer

export default dragDropContext
