import React                                     from 'react';
import scriptLoader                              from "react-async-script-loader";
import { connect }                               from "react-redux";
import { withRouter }                            from "react-router";
import { renderRoutes }                          from "react-router-config";
import Dnd                                       from "shared/ui/Dnd";
import AppFooter                                 from "src/Components/shared/Footer";
import AppHeader                                 from "src/Components/shared/Header";
import ReduxSpinner                              from "src/Components/ui/ReduxSpinner";
import ToastMessage                              from "src/Components/ui/ToastMessage/ToastMessage";
import { getServices }                           from "src/store/actions/miscellaneousActions";
import { getAppDimensions, getAppScroll }        from "src/store/actions/uiActions";
import { get_Clients, getAllUsers, setUserData } from "src/store/actions/usersActions";
import { GlobalStyles }                          from "src/styles/GlobalStyled";
import routes                                    from "./configs/routes";

if ( module.hot )
    module.hot.accept()


class App extends React.Component {

    setDimensions = () => {
        this.props.setDimensions( {
            height: window.innerHeight,
            width: window.innerWidth
        } )
    }


    componentDidUpdate ( prevProps ) {

        if ( this.props.location.pathname !== prevProps.location.pathname ) {
            if ( typeof window !== void 0 ) {
                window.scrollTo( 0, 0 );

            }
        }
    }


    componentDidMount () {
        const jssStyles = document.getElementById( 'jss-server-side' );
        if ( jssStyles && jssStyles.parentNode ) {
            jssStyles.parentNode.removeChild( jssStyles );
        }
        this.setDimensions()

        window.addEventListener( 'scroll', ( ev ) => {
            this.props.setScroll( window.scrollY )
        } )
        window.addEventListener( 'resize', this.setDimensions )

        this.props.setUserData()
        this.props.getAllUser()
        this.props.getAllClients()
        this.props.getServices()
    }


    render () {
        return (
            <>
                <GlobalStyles/>
                <AppHeader/>
                <Dnd>
                    <main style={ { paddingTop: this.props.headerHeight + 'px', maxWidth: '100vw', overflow: "hidden" } }>
                        {
                            renderRoutes( routes )
                        }
                    </main>
                    <ToastMessage/>
                    <ReduxSpinner/>
                </Dnd>
                <AppFooter/>
            </>
        )

    }
}

const mapStateToProps = ( { ui } ) => ( {
    headerHeight: ui.headerHeight
} )

const mapDispatchToProps = dispatch => ( {
    setDimensions: ( dim ) => dispatch( getAppDimensions( dim ) ),
    setScroll: ( scroll ) => dispatch( getAppScroll( scroll ) ),
    setUserData: _ => dispatch( setUserData() ),
    getAllUser: _ => dispatch( getAllUsers() ),
    getAllClients: _ => dispatch( get_Clients() ),
    getServices: _ => dispatch( getServices() )
} )

export default scriptLoader(
    [ 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCXbw5wy_UfZMPbf7iKGZO7q0ktmdgLkXw', ],
)( connect( mapStateToProps, mapDispatchToProps )( withRouter( App ) ) );
