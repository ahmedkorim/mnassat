import React      from "react";
import Pagination from "src/Components/ui/Pagination/Paginaiton";

const withPagination = ( Wrapped ) => class extends React.Component {
    // Todo : handle last item from api Call
    constructor ( props ) {
        super( props )
        const search = new URLSearchParams( this.props.location.search );
        this.state = {
            perPage: +search.get( 'per_page' ) || 20,
            current: +search.get( 'current' ) || 1,
        }
        console.log(
            {
                perPage: +search.get( 'per_page' ) || 20,
                current: +search.get( 'current' ) || 1,
            }

        );
    }

    handlePerPage = ( perPage ) => {
        this.setState( {
            perPage
        }, () => {
            this.props.history.push( {
                path: this.props.location.path,
                search: `?per_page=${ perPage }&current=${ this.state.current }`
            } )
        } )
    }
    handler = ( current ) => {
        this.setState( {
            current
        }, () => {
            this.props.history.push( {
                path: this.props.location.path,
                search: `?per_page=${ this.state.perPage }&current=${ current }`
            } )
        } )
    }


    render () {


        const {
            handlePerPage, handler, state: {
                perPage,
                current,
                last
            },
            props: {
                length = 0
            }
        } = this;
        return <Wrapped
            paginationInfo={ {
                current,
                perPage
            } }
            renderPagination={ ( conf = {} ) => <Pagination length={ length } handler={ handler } current={ current } perPage={ perPage }
                                                            handlePerPage={ handlePerPage }
                                                            { ...conf }
            /> } { ...this.props }/>
    }

}
export default withPagination
