import Button                                     from "@material-ui/core/Button";
import React                                      from 'react'
import { connect }                                from "react-redux";
import { withRouter }                             from "react-router";
import { Link }                                   from "react-router-dom";
import formRender                                 from "shared/Forms/formRender";
import withFormState                              from "shared/Forms/withFormState";
import { FormSubmitter }                          from "src/Components/shared/AboutUs/ContactWithUs";
import loginForm                                  from "src/configs/Forms/loginForm";
import { hideSpinner, showSnackbar, showSpinner } from "src/store/actions/uiActions";
import { userLogin }                              from "src/store/actions/usersActions";

const Login = ( {
                    handleChange,
                    value,
                    form,
                    login,
                    showSnackbar,
                    showSpinner,
                    hideSpinner,
                    history,
                    feedback, validateNow, eagerValidation
                } ) => (
    <>
        <form noValidate className="row" onSubmit={ async ( e ) => {
            e.preventDefault()
            const valid = await validateNow();
            if ( valid ) {
                try {
                    showSpinner()
                    showSnackbar( { message: 'جاري تسجيل الدخول' } )

                    await login( {
                        username: value.username,
                        password: value.password
                    } )
                    showSnackbar( { message: 'تم تسجل دخولك بنجاح ' } )

                    history.push( '/' )
                } catch ( e ) {

                } finally {
                    hideSpinner()
                }

            }
        } }>
            {
                formRender( { body: form, value: value }, handleChange, eagerValidation )
            }
            <FormSubmitter variant="extended" color="primary" type="submit"> تسجل الدخول </FormSubmitter>
        </form>
        <div>
            <Button variant="text" color="primary" component="a" href="http://gomenassat.com/banan.php?action=lostpassword">نسيت كلمة المرور</Button>
            <Button variant="text" color="primary" component={ Link } to="/auth/register"> انشاء حساب جديد</Button>
        </div>
    </>

)

const mapDispatchToProps = dispatch => ( {
    login: ( payload ) => dispatch( userLogin( payload ) ),
    showSnackbar: ( conf ) => dispatch( showSnackbar( conf ) ),
    showSpinner: () => dispatch( showSpinner() ),
    hideSpinner: () => dispatch( hideSpinner() ),
} )

export default withFormState( loginForm.formState, loginForm.form )( connect( null, mapDispatchToProps )( withRouter( Login ) ) )
