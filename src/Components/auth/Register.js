import Button                                     from "@material-ui/core/Button";
import * as PropTypes                             from "prop-types";
import React, { Component }                       from 'react'
import { connect }                                from "react-redux";
import { withRouter }                             from "react-router";
import { Link }                                   from "react-router-dom";
import formRender                                 from "shared/Forms/formRender";
import withFormState                              from "shared/Forms/withFormState";
import { FormSubmitter }                          from "src/Components/shared/AboutUs/ContactWithUs";
import registerFrom                               from "src/configs/Forms/registeFrom";
import { hideSpinner, showSnackbar, showSpinner } from "src/store/actions/uiActions";
import { userLogin }                              from "src/store/actions/usersActions";
import { nativeApi }                              from "src/store/store";

class Login extends Component {

    getCallToAction = ( step, nextStep, prevStep ) => {
        switch ( step ) {
            case 1:
                return <div className="col-12 text-center py-2">
                    <FormSubmitter variant="extended" color="primary" type="button" onClick={ this.sendCode }>ارسل الكود</FormSubmitter></div>
            case 2:
                return <>
                    <div className="col-12 text-center py-2">
                        <FormSubmitter variant="extended" color="primary" type="button" onClick={ this.confirmCode }>تاكيد الكود</FormSubmitter>
                    </div>
                    <div>
                        <Button variant="text" color="primary" type="button" onClick={ this.sendCode }>اعد ارسال كود</Button>
                        <span className="px-1"/>
                        <Button variant="text" color="primary" type="button" onClick={ prevStep }>ادخال هاتفي مره اخري</Button>
                    </div>
                </>
            case 3:
                return <div className="col-12 text-center py-2">
                    <FormSubmitter variant="extended" color="primary" type="button" onClick={ this.register }>تسجيل الدخول</FormSubmitter>
                </div>

        }
    }
    confirmCode = async () => {
        const state = await this.props.validateNow( true, 2 );
        ;
        if ( state ) {
            try {
                const { phone_number, country_code, code } = this.props.value;
                this.props.showSpinner()
                this.props.showSnackbar( { message: 'جاري تاكيد الكود' } )
                const { data } = await nativeApi.post( '/code-verify', {
                    phone_number,
                    country_code,
                    code
                } )
                console.log( data );
                this.props.nextStep()
                this.props.stopEagerValidation()
                this.props.showSnackbar( { message: 'تم تاكيد رقم هاتفك برجاء استكمال التسجيل' } )

            } catch ( e ) {
                console.log( e.response.data );
                this.props.showSnackbar( { message: 'الكود المدخل غير صحيح برجاء التاكد او اعاده ارسال الكود' } )

            } finally {
                this.props.hideSpinner()

            }

        }
    }
    register = async () => {
        const state = await this.props.validateNow( true, 3 );
        if ( state ) {
            const { username, email, password } = this.props.value;
            try {
                this.props.showSpinner()
                this.props.showSnackbar( { message: 'جاري تسجيل حسابك' } )
                const { data } = await nativeApi.post( '/register', { username, email, password } )
                this.props.showSnackbar( { message: 'تم انشاء حسابك بنجاح برجاء انتظار تسجيل الدخول' } )
                await this.props.login( {
                    username,
                    password
                } )
                this.props.showSnackbar( { message: 'تم تسجل دخولك بنجاح ' } )
                this.props.history.push( '/' )

            } catch ( e ) {
                console.log( e.response.data );
            }
        }
    }
    sendCode = async () => {
        console.log( this.props.value );
        const state = await this.props.validateNow( true, 1 );
        if ( state ) {
            if ( this.props.step === 1 ) {
                const { phone_number, country_code } = this.props.value;
                try {
                    this.props.showSpinner()
                    this.props.showSnackbar( { message: 'جاري ارسال الكود الي هاتفك' } )
                    const { data } = await nativeApi.post( '/get-sms', {
                        phone_number, country_code
                    } )

                    this.props.nextStep()
                    this.props.showSnackbar( { message: 'تم ارسال الكود اللي هاتفك' } )
                } catch ( e ) {
                    if ( !e.response ) return console.log( e );
                    const error = e.response.data.error;
                    if ( error.error_code === '60033' ) {
                        this.props.hideSpinner()
                        this.props.showSnackbar( { message: "رقم هاتف غير صحيح برجاء التاكد من كود الدوله واعاده المحاوله", variant: "danger" } )
                    }
                } finally {
                    this.props.hideSpinner()
                }

            }
            this.props.stopEagerValidation()
        }
    }

    render () {
        let { handleChange, value, step, form, validateNow, feedback, nextStep, lastStep, prevStep, eagerValidation } = this.props;
        const { sendCode, getCallToAction } = this;

        return (
            <>
                { step === 2 && <div className="text-center">
                    برجاء ادخال الكود المرسل علي هاتفك واستكمال التسجيل
                    <br/>
                </div> }
                <form className="row">
                    {
                        formRender( { body: form, value: value }, handleChange, eagerValidation )
                    }
                    { getCallToAction( step, nextStep, prevStep ) }
                    <div className="px-3">
                        <span>الخطوه</span>
                        { step } من { lastStep }
                        <span>خطوات</span>
                    </div>
                </form>
                <div>
                    <Button variant="text" color="primary" component={ Link } to="/auth/login">تسجل الدخول</Button>
                </div>
            </>

        );
    }
}

Login.propTypes = {
    handleChange: PropTypes.any,
    value: PropTypes.any,
    form: PropTypes.any,
    feedback: PropTypes.any
}


export default withFormState( registerFrom.formState, registerFrom.form )(
    connect( null, dispatch => ( {
        showSnackbar: ( conf ) => dispatch( showSnackbar( conf ) ),
        showSpinner: () => dispatch( showSpinner() ),
        hideSpinner: () => dispatch( hideSpinner() ),
        login: ( payload ) => dispatch( userLogin( payload ) )
    } ) )( withRouter( Login ) ) )
