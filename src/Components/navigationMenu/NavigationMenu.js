import React                                 from 'react'
import { withRouter }                        from "react-router";
import { Link }                              from "react-router-dom";
import { accentColor, darkColor, mainColor } from "src/styles/colors";
import styled                                from 'styled-components';

export const NavMenuItem = styled.li`
padding: 0 .6rem;
display: inline-block;
`
export const NavMenuLink = styled( Link )`
    color:  ${ ( { active } ) => !!active ? mainColor : darkColor };
    font-size:1.3rem;
    position: relative;
&:after{
    display: block;
    content:'';
    background-color:  ${ ( { active } ) => active ? mainColor : accentColor };
    width: ${ ( { active } ) => !!active ? '100%' : '5px' };;
    height:3px;
    opacity: ${ ( { active } ) => !!active ? 1 : 0 };
    position: absolute;
    left: 0;
    right: 0;
    margin-right: auto;
    margin-left: auto;
     bottom:-.6rem;
     transition:all ease .3s ;
}
 &:hover{
     text-decoration: none;
     color: ${ ( { active } ) => !!active ? mainColor : accentColor } !important;
     &:after{
         opacity: 1;
         width: 100%;

     }
 }

    transition:all .35s ease-in-out; 
 
`;
export const NavMenuList = styled.ul`
margin: 0!important;
padding: 0!important;
`
const NavigationMenu = ( props ) => {
    const { links, history } = props;
    return (

        <NavMenuList>
            {
                links.map( ( { name, path, title } ) =>

                    <NavMenuItem
                        key={ name }
                    >
                        <NavMenuLink
                            to={ path }
                            active={ history.location.pathname === path }
                        >
                            { title }
                        </NavMenuLink>
                    </NavMenuItem>
                )
            }
        </NavMenuList>


    )
}
export default withRouter( NavigationMenu )
