import Fab                                  from "@material-ui/core/Fab";
import React, { Component }                 from 'react'
import { darkColor, lightColor, mainColor } from "src/styles/colors";
import styled                               from 'styled-components';
import decore                               from 'assets/Images/fast_realstate.png'
import googlePlay                           from 'assets/Images/google-play.png';
import appStore                             from 'assets/Images/apple.png';


const Wrapper = styled.header`
background-color: ${ mainColor };
color:${ lightColor };
padding: 3rem 0;
`


export const CallToActionButton = styled( Fab )`
img{
width:21px;
display: block;
margin: .2rem .4rem;

}
color: ${ darkColor };
&:hover{
color: ${ darkColor };

}
background-color: #fff !important;
width: 40%!important;
display: block;
`
export const CallToAction = _ => <div className="my-3  d-flex justify-content-start">
    <CallToActionButton variant="extended"
                        component="a" href="https://play.google.com/store/apps/details?id=com.gomenassat.propertyApp2&hl=es_EC"
                        target="_blank"
    >Google play <img src={ googlePlay } alt="Google play"/></CallToActionButton>
    <span className="p-2"></span>
    <CallToActionButton variant="extended"
                        target="_blank"
                        component="a" href="https://itunes.apple.com/us/app/menassat-app/id1446633441"
    >App Store <img src={ appStore } alt="App store"/></CallToActionButton>
</div>

class AboutUsHero extends Component {
    render () {
        return (
            <Wrapper>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-5 col-md-12 text-right d-flex d-md-block flex-column justify-content-center">
                            <div>
                                <h2 className="h1 mb-3">منصات العقارية</h2>
                            </div>
                            <p className="lead">
                                شعارنا هو الرؤية الناجحة,لعلمنا أن اختيار الشريك الصحيح في تقديم خدمات عقاريه احترافيه يعد جزءأَ أساسيا من نجاح رؤيتكم
                                الاستثماريه
                            </p>
                            { !this.props.noButtons &&
                            <>
                                <p className="lead">
                                    حمل التطبيق الان
                                </p>
                                <CallToAction/>
                            </> }
                        </div>
                        <div className="col-lg-5 col-md-12 ">
                            <img src={ decore } alt=""/>
                        </div>
                    </div>
                </div>
            </Wrapper>
        );
    }
}


export default AboutUsHero
