import Fab                                                                      from "@material-ui/core/Fab";
import Icon                                                                     from "@material-ui/core/Icon";
import React                                                                    from 'react'
import formRender                                                               from "shared/Forms/formRender";
import withFormState                                                            from "shared/Forms/withFormState";
import { IconWrapper }                                                          from "src/Components/ui/TeamMember/TeamMember";
import contactWithUs                                                            from "src/configs/Forms/contactWithus";
import { darkMainColor, gradient, lightColor, lightGrey, lightMain, mainColor } from "src/styles/colors";
import styled                                                                   from 'styled-components';
import instagram                                                                from 'assets/Images/instagram.png'
import fb                                                                       from 'assets/Images/facebook.png'
import tw                                                                       from 'assets/Images/twiiter.png'
import snap                                                                     from 'assets/Images/snapchat_PNG9.png'

const ContactUsWrapper = styled.div`
background-color: ${ lightColor };
border-radius: 4rem;
padding-top: 2rem;
padding-bottom: 2rem;
overflow: hidden;

`
const Background = styled.footer`
background: linear-gradient( 45deg,${ gradient.blue } ,${ gradient.foschia });
padding: 4rem 0;
`

export const FormSubmitter = styled( Fab )`
&&{
width: 9rem;
margin: .7rem auto;
&:hover{

}
}
`
const Contacts = styled.section`
&&{
background-color: ${ lightGrey } ;
padding: 1rem;
margin-left: -15px;
margin-right: -15px;
}
text-align: right;
color: ${ darkMainColor };
li{
margin-bottom: .7rem;
}
.contact__header-follow-us{
color: ${ mainColor };
text-align: center;
}
`
const SocialMeidaLinks = styled.ul`
list-style: none;
margin: 0;
padding: 0;
display: flex;
justify-content: center;
align-items: center;
li{
margin: .5rem;
width:44px;
img{
max-width: 100%;
max-height: 45px;
}
}
`

const ContactWithUs = ( { handleChange, value, form, feedback, eagerValidation, stopEagerValidation, validateNow } ) => {
    const sendMail = async ( e ) => {
        e.preventDefault()
        const state = await validateNow();
        if ( state ) {
            const { name, email, message } = value;
            const link = `mailto:info@gomenassat.com?subject=${ name }&body=${ message }`
            const win = window.open( link, 'emailWindow' );
            // if ( win && win.open && !win.closed ) win.close();
            stopEagerValidation()
        }
    }
    return (

        <Background>
            <ContactUsWrapper className="container">
                <div className=" d-flex flex-wrap justify-content-center">
                    <section className="row col-12 py-4 justify-content-center">
                        <div className="col-6">
                            <h3 className="text-right"> تواصل معنا</h3>
                            <form
                                noValidate
                                className="row" onSubmit={ ( e ) => sendMail( e ) }>
                                {
                                    formRender( { body: form, value: value }, handleChange, eagerValidation )
                                }
                                <FormSubmitter variant="extended" color="primary" type="submit"> ارسال</FormSubmitter>
                            </form>
                        </div>
                    </section>
                </div>
                <Contacts>
                    <div className="row no-gutters align-items-center">
                        <div className="col col-md-9">
                            <ul className="list-unstyled">
                                <li>
                                    <div className="d-flex align-items-center">
                                        <IconWrapper>
                                            <Icon>phone</Icon>
                                        </IconWrapper>
                                        <div>+966-920010094</div>
                                    </div>
                                </li>
                                <li>
                                    <div className="d-flex align-items-center">
                                        <IconWrapper><Icon fontSize="small">stay_current_portrait</Icon></IconWrapper>
                                        <div>+966-53899766</div>
                                    </div>
                                </li>
                                <li>
                                    <div className="d-flex align-items-center">
                                        <IconWrapper><Icon fontSize="small">email</Icon></IconWrapper>
                                        <div>info@gomenassat.com</div>
                                    </div>
                                </li>
                                <li>
                                    <div className="d-flex align-items-center">
                                        <IconWrapper><Icon fontSize="small">location_on</Icon></IconWrapper>
                                        <div>شارع الملكة ابن بشير,الربوة,الرياض,المملكة العربيه السعوديه</div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div className=" col-12 col-md-3">
                            <h3 className="contact__header-follow-us">تابعونا علي</h3>
                            <SocialMeidaLinks>
                                <li>
                                    <a target="_blank" href="https://www.instagram.com/gomenassat/">
                                        <img src={ instagram } alt=""/>
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="https://www.facebook.com/gomenassat/">
                                        <img src={ fb } alt=""/>
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="https://twitter.com/gomenassat">
                                        <img src={ tw } alt=""/>
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="https://www.snapchat.com/add/gomenassat">
                                        <img src={ snap } alt=""/>
                                    </a>
                                </li>
                            </SocialMeidaLinks>
                        </div>
                    </div>
                </Contacts>
            </ContactUsWrapper>
        </Background>


    );
}

export default withFormState( contactWithUs.formState, contactWithUs.form )( ContactWithUs )
