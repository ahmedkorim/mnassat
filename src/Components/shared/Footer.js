import React                                from 'react';
import { connect }                          from "react-redux";
import { CallToAction, CallToActionButton } from "src/Components/shared/AboutUs/AboutUsHero";
import VerticalLinksList                    from "src/Components/ui/VerticalLinksList/VerticalLinksList";
import navigationLinks                      from "src/configs/navigationLinks";
import { getAllOffers }                     from "src/store/actions/offersActions";
import { getAllRequests }                   from "src/store/actions/requestsActions";
import { darkColor, lightColor }            from "src/styles/colors";
import styled                               from 'styled-components';
// todo Refactor call to action


const Wrapper = styled.footer`
padding: 5rem 0;
background-color: ${ darkColor };
color:${ lightColor };
`

class AppFooter extends React.Component {

    componentDidMount () {
        this.props.loadData()
    }

    render () {
        const {
            offers,
            requests,

        } = this.props;
        const latestOffers = offers.slice( Math.max( offers.length - 5, 1 ) ).map( i => ( {
            id: i.id,
            title: i.title.slice( 0, 14 ),
            name: i.title,
            path: '/details/' + i.id + '?type=offer'
        } ) )
        const latestRequests = requests.slice( Math.max( requests.length - 5, 1 ) ).map( i => ( {
            id: i.id,
            title: i.title.slice( 0, 14 ),
            name: i.title,
            path: '/details/' + i.id + '?type=request'
        } ) )
        return (
            <Wrapper>
                <div className="container">
                    <div className="row">
                        <div className="col-12 col-md-6 text-right mb-4">
                            <h2 className="h1 mb-3">منصات العقارية</h2>
                            <p className="lead">
                                شعارنا هو الرؤية الناجحة,لعلمنا أن اختيار الشريك الصحيح في تقديم خدمات عقاريه احترافيه يعد جزءأَ أساسيا من نجاح رؤيتكم
                                الاستثماريه
                            </p>
                            <p className="lead">
                                حمل التطبيق الان
                            </p>
                            <CallToAction/>
                        </div>
                        <div className="col-12 col-md-6 ">
                            <nav className="row ">
                                <div className="col-4">
                                    <VerticalLinksList
                                        title={ "الصفحات" }
                                        links={ navigationLinks }/>
                                </div>
                                <div className="col-4">
                                    <VerticalLinksList
                                        title={ "اخر العروض" }
                                        links={ latestOffers }/>
                                </div>
                                <div className="col-4">
                                    <VerticalLinksList
                                        title={ "اخر الطلبات" }
                                        links={ latestRequests }/>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </Wrapper>
        )
    }
}

const loadData = ( { dispatch } ) => {
    dispatch( getAllOffers() )
    dispatch( getAllRequests() )
}


export default connect( ( { offers: { offersList = [] }, requests: { requestsLists = [] } } ) => ( {
    offers: offersList,
    requests: requestsLists
} ), dispatch => ( { loadData: () => loadData( { dispatch } ) } ) )( AppFooter );

