import { Divider }                                           from "@material-ui/core";
import Button                                                from "@material-ui/core/Button";
import Fab                                                   from "@material-ui/core/Fab";
import IconButton                                            from "@material-ui/core/IconButton";
import shadows                                               from "@material-ui/core/styles/shadows";
import Icon                                                  from "@material-ui/core/Icon";
import Tooltip                                               from "@material-ui/core/Tooltip";
import React                                                 from 'react';
import { connect }                                           from "react-redux";
import { withRouter }                                        from "react-router";
import { Link }                                              from "react-router-dom";
import image                                                 from 'assets/Images/menassat-logo-horizontal.png'
import NavigationMenu                                        from "src/Components/navigationMenu/NavigationMenu";
import SideDrawer                                            from "src/Components/ui/Drawer/Drawer";
import DropDown                                              from "src/Components/ui/DropDown/DropDown";
import UserAvatar                                            from "src/Components/ui/UserAvatar/UserAvatar";
import navigationLinks  ,{callToActionLinks}                                     from "src/configs/navigationLinks";
import { getAppHederHeight, openAppDrawer, toggleAppDrawer } from "src/store/actions/uiActions";
import { lightColor, mainColor }                             from "src/styles/colors";
import styled, { css }                                       from 'styled-components';
import { size }                                              from 'shared/ui/sc-breakpoint'

const Logo = styled.img`
display: inline-block;
max-width: 100%;
max-height: 50px;
`

const StyledButton = styled( Button )`

min-width: unset !important;
background-color: ${ mainColor } !important;
border-radius: 0 !important;
color: white !important;
transition: all ease-in-out .7s;

${ ( { compactHeader } ) => `
height:${ compactHeader ? 55 : 85 }px !important;
width: ${ compactHeader ? 55 : 85 }px !important;
` }
`

const options = [
    {
        id: "user-add-offer",
        name: 'اضف عرض',
        to: '/add-offer'
    }, {
        id: "user-add-orders",
        name: 'اضف طلب',
        to: '/add-order'
    }, {
        id: "user-request-service",
        name: 'طلب خدمه',
        to: '/services',

    }
]

const MainHeader = styled.header`

box-sizing: border-box;
position: ${ ( { compactHeader } ) => compactHeader ? 'fixed' : 'absolute' };
width:100vw;
top: 0;
left: 0;
z-index: 1000;
background-color: ${ lightColor };
box-shadow: ${ ( { compactHeader } ) => compactHeader ? shadows[ 3 ] : shadows[ 0 ] };


${ ( { compactHeader } ) => `
height:${ compactHeader ? 55 : 85 }px !important;
` }
.row > div{
height: 100% !important;
}
`
const CallToAction = styled( Fab )`
&&{
position: fixed;
bottom:2rem;
right:2rem;
}
`
const HeaderCallToAction = ( { headerHeight,compactHeader, history: { push }, variant } ) => {

    const handler = ( option ) => {

        const selected = options.find( ( { name } ) => name === option )
        if ( selected ) {
            console.log( 'go to' + selected.to );
            push( selected.to )
        }
    }
    const pTop = variant === 'float' ? 0 : compactHeader ? 30 : 55
    return variant === 'float' ?
        <Tooltip
            title={ 'اضف عرض' }
            children={ <CallToAction
                color="primary"
                onClick={ () => push( '/add-offer' ) }
            >
                <Icon fontSize="large" className="font-weight-bold">add</Icon>
            </CallToAction> }
        />
        :
        <DropDown
            options={ options }
            handleChange={ handler }
            pTop={ pTop }
            pWidth={ 200 }
            paperStyles={ {
                boxShadow: shadows[ 1 ],
                borderRadius: '.2rem',

            } }
            renderTrigger={ ( handler, close ) =>

                <StyledButton
                    onClick={ handler }
                    className="h-100" compactHeader={ compactHeader }>
                    <Icon fontSize="large" className="font-weight-bold">add</Icon>
                </StyledButton>


            }
        />;
}


class AppHeader extends React.Component {
    state = {
        compactHeader: false
    }

    componentDidMount () {
        const $height = this.header ? this.header.offsetHeight : 55;
        const { scroll, height } = this.props;
        const compactHeader = scroll > height / 2
        this.setState( {
            compactHeader,
            headerHeight:$height
        }, () => {
            this.props.setAppHeader( $height )
        } )

    }

    componentWillUpdate ( { scroll = 0, height = 600 }, nextState, nextContext ) {

        const compactHeader = scroll > height / 2
        if ( this.state.compactHeader !== compactHeader )
            this.setState( {
                compactHeader
            }, )
    }

    render () {
        const {
            props: {
                scroll,
                height, currentUser, width,
                history,
                drawerOpen,
                toggleDrawer
            },
            state: {
                compactHeader,
                headerHeight
            }
        } = this;
        console.log( width );
        return (
            <MainHeader compactHeader={ compactHeader } ref={ node => this.header = node }>
                <div className="container h-100">
                    <div className="row h-100 ">
                        { width > size.md ? <>
                                <div className="col col-md-3 d-flex  justify-content-start align-items-center ">
                                    <UserAvatar
                                        user={ currentUser }
                                    />
                                    <HeaderCallToAction headerHeight={headerHeight} compactHeader={ compactHeader } history={ history }/>
                                </div>
                                <nav className="col-6 w-100  d-flex align-items-center justify-content-center ">
                                    <NavigationMenu links={ navigationLinks }/>
                                </nav>
                            </> :
                            <div className="col d-flex align-items-center">
                                <IconButton
                                    onClick={ toggleDrawer }
                                ><Icon>menu</Icon></IconButton>
                                <span className="pr-1"></span>
                                <UserAvatar
                                    user={ currentUser }
                                />
                                <HeaderCallToAction variant="float" compactHeader={ compactHeader } history={ history }/>
                                <SideDrawer
                                    open={ drawerOpen }
                                    links={ navigationLinks }
                                    callToActionLinks={callToActionLinks}
                                    toggleDrawer={ toggleDrawer }
                                >
                                    <div className="p-3">
                                        <div className="w-100">
                                            <Link
                                                to="/"
                                                aria-labelledby="brand-name"
                                            >
                                                <h1 className="sr-only" id="brand-name"> منصه العقاريه</h1>
                                                <Logo src={ image } alt=""/>
                                            </Link>
                                        </div>
                                    </div>
                                    <Divider/>
                                </SideDrawer>
                            </div>
                        }
                        <div className="col col-md-3  d-flex mr-auto justify-content-start align-items-center">
                            <div className="w-100">
                                <Link
                                    to="/"
                                    aria-labelledby="brand-name"
                                >
                                    <h1 className="sr-only" id="brand-name"> منصه العقاريه</h1>
                                    <Logo src={ image } alt=""/>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </MainHeader>
        )
    }
}


const mapDispatchToProps = dispatch => ( {
    setAppHeader: ( height ) => dispatch( getAppHederHeight( height ) ),
    toggleDrawer: () => dispatch( toggleAppDrawer() )
} )
const mapStateTopProps = ( { ui: { scroll, height, width, drawerOpen }, user: { currentUser } } ) => ( { currentUser, width, scroll, drawerOpen, height } )

export default connect( mapStateTopProps, mapDispatchToProps )( withRouter( AppHeader ) );
