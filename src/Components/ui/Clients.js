import React       from 'react';
import Swiper      from 'react-id-swiper/lib/ReactIdSwiper.full';
import { connect } from "react-redux";
import styled      from 'styled-components';

const Image = styled.img`
max-width:150px;

`
const thumbnailSwiperParams = {
    paceBetween: 10,
    slidesPerView: 5,
    spaceBetween: 10,
    touchRatio: 0.2,
    slideToClickedSlide: true
};

class Clients extends React.Component {


    render () {
        const slides = this.props.clients.map( i => ( {
            id: i.id,
            title: i.title.rendered,
            media: i.featured_media,
            image: ( this.props.media.find( image => i.featured_media === image.id ) || {} ).url
        } ) )
        return (
            <div className="py-5">
                <div className="container">
                    { slides.length > 0 && <Swiper { ...thumbnailSwiperParams } >
                        { slides.map( i => <div id={ i.id }>
                            <Image className="img-fluid" src={ i.image } alt={ i.title }/>
                        </div> ) }
                    </Swiper> }
                </div>
            </div>
        )
    }
}

const mapStateToProps = ( { miscellaneous: { media }, user: { clients } } ) => ( {
    media,
    clients
} )

export default connect( mapStateToProps )( Clients );
