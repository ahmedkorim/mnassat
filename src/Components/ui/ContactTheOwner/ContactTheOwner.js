import PropTypes                           from 'prop-types'
import { Card, Divider }                   from "@material-ui/core";
import Button                              from "@material-ui/core/Button";
import React, { Component }                from 'react'
import { connect }                         from "react-redux";
import formRender                          from "shared/Forms/formRender";
import withFormState                       from "shared/Forms/withFormState";
import { FormSubmitter }                   from "src/Components/shared/AboutUs/ContactWithUs";
import Spinner                             from "src/Components/ui/Spinner";
import contactTheAuthorForm                from "src/configs/Forms/ContactTheAuthorForm";
import { sendMessage }                     from "src/store/actions/usersActions";
import { greyColor, lightGrey, lightMain } from "src/styles/colors";
import styled                              from 'styled-components';

const Wrapper = styled( Card )`
display: flex;
flex-direction: column;
text-align: right;
border-radius: .2rem;
overflow: hidden;
min-height: 300px;
position: relative;
header{
background-color: ${ lightMain };
padding: 1.1rem 1rem;
color: ${ greyColor };
}
.section__full-padding{
padding: .7rem 1rem;
}
.section__y-padding{
padding: .7rem 0;
}

.user__title{
color:  ${ greyColor };
}
`

class ContactTheOwner extends Component {
    state = {
        sending: false
    }
    handleSubmit = async ( e ) => {
        e.preventDefault()
        const state = await this.props.validateNow();
        if ( state ) {
            const message = {
                to: this.props.author.id,
                content: this.props.value.content
            };
            this.setState( {
                sending: true
            } )
            try {
                const dispatch = await this.props.sendMessage( message )
                this.setState( {
                    sending: false
                }, () => {
                    this.props.handleChange( 'content' )( {
                        target: {
                            value: ''
                        }
                    } )
                    this.props.stopEagerValidation()
                } )
            } catch ( e ) {
                console.log( e );
            }

        }
    }

    render () {
        let { author, handleChange, value, form, feedback, eagerValidation } = this.props;
        return (
            <Wrapper>
                <header>
                    <h5 className="m-0">تفاصيل صاحب العرض</h5>
                </header>
                {
                    author ? <>
                        <section className="row align-items-center section__full-padding">
                            <div className="col-3">
                                <img className="img-fluid" src={ author[ 'avatar_urls' ][ '96' ] } alt={ author[ 'name' ] }/>
                            </div>
                            <div className="col">
                                <h6 className="m-0">{ author[ 'name' ] }</h6>
                                <span className="user__title">{ author[ 'description' ] }</span>
                            </div>
                        </section>
                        <section className="row align-items-center section__full-padding justify-content-center">
                            <div className="col-6 text-center">
                                <Button component="a" target="_blank" href={ `tel:${ author.mobile }` }>اتصل الان</Button>
                            </div>
                            <div className="col-6 text-center">
                                <Button component="a" target="_blank" href={ `https://api.whatsapp.com/send?phone=${ author.mobile }` }>واتساب</Button>
                            </div>
                        </section>
                        <div className="section__y-padding">
                            <Divider/>
                        </div>
                        <form className="align-items-center section__full-padding justify-content-center row "
                              noValidate
                              onSubmit={ ( e ) => this.handleSubmit( e ) }
                        >
                            {
                                formRender( { body: form, value: value }, handleChange, eagerValidation )

                            }
                            <FormSubmitter className="d-block" variant="extended" color="primary"
                                           type="submit"
                            > ارسال</FormSubmitter>
                        </form>
                    </> : <Spinner/>
                }
                { this.state.sending && <Spinner/> }
            </Wrapper>
        )
    }
}

ContactTheOwner.propTypes = {
    eagerValidation: PropTypes.any,
    feedback: PropTypes.any,
    form: PropTypes.any,
    handleChange: PropTypes.any,
    value: PropTypes.any
}

const mapDispatchToProps = dispatch => ( {
        sendMessage: ( message ) => dispatch( sendMessage( message ) )
    }
)
export default withFormState( contactTheAuthorForm.formState, contactTheAuthorForm.form )(
    connect( void 0, mapDispatchToProps )( ContactTheOwner ) )
