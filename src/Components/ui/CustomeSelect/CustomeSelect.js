import { ButtonBase } from "@material-ui/core";
import Checkbox       from "@material-ui/core/Checkbox";

import shadows                        from "@material-ui/core/styles/shadows";
import * as PropTypes                 from "prop-types";
import React, { Component, useState } from 'react'
import { connect }                    from "react-redux";
import handleInputState               from "shared/Forms/handleInputState";
import { above }                      from "shared/ui/sc-breakpoint";
import { getServices }                from "src/store/actions/miscellaneousActions";
import { lightColor, lightMain }      from "src/styles/colors";
import styled                         from 'styled-components';

import threed   from 'assets/Images/services/3d.png'
import rend     from 'assets/Images/services/Group 544.png'
import install  from 'assets/Images/services/install_pallete.png'
import manage   from 'assets/Images/services/mange.png'
import rate     from 'assets/Images/services/rate.png'
import study    from 'assets/Images/services/stydy.png'
import verified from 'assets/Images/services/verfied.png'

/*

 const services = [
 {
 name: "threed", image: verified, title: "توثيق ملكية العقار", typography: `هذه الخدمة سيزورك موظفنا للاطلاع على اثباتات تبين احقيتك في
 الملكية العقار المراد عرضة على منصتنا والتي ستميز عرضكم عن
 العروض الاخرى التي لم توثق وهذه خطوة ستساعدك في الوصول
 لعدد اكبر من المشترين المتوقعين وتعزز فرص نجاح تسويقك`
 }, {
 name: "verfied", image: threed, title: "تصوير العقار ثرى دى", typography: "هذه الخدمة تعتبر من الخدمات الرائعه والتي تجعل المتصفح يعاين\n" +
 "عقارك بكل سهولة من جواله الشخصي او اي جهاز حاسوب وصول\n" +
 "عرضك العقاري لأكبر عدد من العملاء المتوقعين وتمكنه من التجول\n" +
 "الافتراضي ثلاثي الأبعاد فيه مما يعزز فرص"
 },
 {
 name: "rend", image: rend,
 typography: `تقدم لك هذه الخدمة تجهيز عقد ايجار لعقارك في حال توفر
 مستأجر له ونقوم بتوثيق العقد في منصة ايجار التابعة لوزارة
 الاسكان حسب اشتراطات النظام والذي يعتبر اي عقد ايجار لا يتم
 توثيقه في منصة ايجار عقدا مخالفا للانظمة ( الرسوم تغطي
 رسوم تسجيل العقد الحكومية)`,
 title: "خدمة توثيق عقد الايجار بمنصة ايجار",
 },
 {
 name: "install", image: install, title: "خدمة تركيب لوحة على العقار", typography: `نقوم بتركيب لوحة من لوح شركتنا على موقع العقار الذي ترغبون
 بيعه او تأجيره لتسريع عملية تسويقة`
 },
 {
 name: "manage", image: manage, title: "ادارة املاك", typography: `
 هذه الخدمة سيزورك موظفنا للاطلاع على اثباتات تبين احقيتك في
 الملكية العقار المراد عرضة على منصتنا والتي ستميز عرضكم عن
 العروض الاخرى التي لم توثق وهذه خطوة ستساعدك في الوصول
 لعدد اكبر من المشترين المتوقعين وتعزز فرص نجاح تسويقك`
 },
 {
 name: "rate", image: rate, title: "تقييم عقاري", typography: `
 تتيح لك خدمة تقييم عقارك على معرفة القيمة السوقية الحالية
 لقيمة عقارك سواء القيمة الايجارية او البيعية لتساعدك على وضع
 طلب لقيمة عقارك حقيقيه وواقعية كما انك يمكنك ان تضع قيمة
 التقييم في عرضك للعامة لتعطيهم فكرة عن القيمة السوقية
 لعقارك المعروض فيعطية تميز عن غيره من العروض وتسهل على
 عميلك شراء عقارك دون تخوف من ان سعره اعلى من سعر السوق`
 },
 {
 name: "study", image: study, typography: `نقوم بتركيب لوحة من لوح شركتنا على موقع العقار الذي ترغبون
 بيعه او تأجيره لتسريع عملية تسويقة`,
 title: "دراسات عقارية",

 },
 ]
 */

const StyledCheck = styled.label`
&&{
display: block !important;
${
    above.md`
    width: calc(50% - 2rem);
    `
    };
${
    ( { smallStyle } ) => smallStyle && `    width: calc(100% - 2rem) !important;
`
    };
margin: 1rem 1rem;
position: relative;
transition: all ease .6s;
  box-shadow: ${ ( { activeOP } ) => activeOP ? shadows[ 5 ] : shadows[ 1 ] };
  background-color: ${ ( { activeOP } ) => activeOP ? lightMain : lightColor };
  border-radius: .2rem;
}
`
const StyledCheckWrapper = styled.div`
min-height: 170px;
width: 100%;
  background: ${ ( { BgImage } ) => `url("${ BgImage }")` } no-repeat 100% 0;
  background-size:100px;
justify-content: center;
text-align: center;
display: flex;
flex-wrap: wrap;
flex-direction: column;
padding: 1rem;
h6{
display: block;
margin-right: 100px;
text-align: right;
font-weight: bold;
margin-bottom: 1.5rem;
}
p{
display: block;
margin-right: 100px;

text-align: right;


font-size:.9rem;
}
`

const Wrapper = styled.div`
display: flex;
width: 100%;
flex-wrap: wrap;
`

const CustomSelect = ( { name, value, header, content, handleChange, image, size, ...rest } ) => {
    const selected = value.indexOf( name ) > -1;
    const handlerValue = selected ? value.replace( `${ name },`, '' ) : value + `${ name },`
    return (
        <ButtonBase component={ StyledCheck } smallStyle={ size === 'small' } htmlFor={ 'custom_select' + name }
                    activeOP={ selected }>
            <StyledCheckWrapper BgImage={ image }>
                <h6>
                    { header }
                </h6>
                <div dangerouslySetInnerHTML={ { __html: content.rendered } }>
                    { rest.typography }
                </div>
            </StyledCheckWrapper>
            <Checkbox type="checkbox" name={ name } id={ 'custom_select' + name }
                      color="primary"
                      onChange={ () => handleChange( handlerValue ) }
                      checked={ selected }/>
        </ButtonBase>
    )
}

const onChangeHandler = ( changeHandler ) => ( value ) => {
    console.log( value );

    const e = {
        target: {
            value
        }
    }
    changeHandler( e )
}

const renderPricefromIds = ( idsCs = "", services = [] ) => {
    const idsarray = idsCs.split( ',' ).map( i => +i )
    return services.filter( i => idsarray.indexOf( i.id ) > -1 ).reduce( ( acc, i ) => acc + ( +i.pack_price || 0 ), 0 )
}

class ServiceSelect extends Component {


    componentDidMount () {
        this.props.getServices()
    }

    render () {
        let { value, services, size, name, media, label, placeholder, helperText, feedback, changeHandler, eagerValidation, ...rest } = this.props;


        const { feedbackColor, feed } = handleInputState(
            false, false, feedback, false,
            eagerValidation, value, rest.required )
        helperText = feed;

        let bundedWithMeida = services.map( service => ( {
            ...service,
            featured_media: ( media.find( media => media.id === service.featured_media ) || {} ).url || verified
        } ) )
        return (
            <fieldset>
                <legend className="text-right">{ label }</legend>
                <div>
                    اجمالي التكلفه: { renderPricefromIds( value, services ) } ريال
                </div>
                <Wrapper>
                    {
                        bundedWithMeida.map( i => <CustomSelect
                            key={ i.id }
                            name={ i.id }
                            image={ i.featured_media }
                            header={ i.title.rendered }
                            value={ value }
                            size={ size }
                            content={ i.content }
                            { ...i }
                            handleChange={ onChangeHandler( changeHandler ) }
                        /> )
                    }
                </Wrapper>
                { eagerValidation && <div className="text-right alert-danger mt-2">
                    { ( feedback || {} ).error }
                </div> }
            </fieldset>
        )

    }
}

ServiceSelect.propTypes = {
    value: PropTypes.string,
    services: PropTypes.any,
    size: PropTypes.any,
    name: PropTypes.any,
    label: PropTypes.any,
    placeholder: PropTypes.any,
    helperText: PropTypes.any,
    feedback: PropTypes.shape( {} ),
    changeHandler: PropTypes.any,
    eagerValidation: PropTypes.any
}

ServiceSelect.defaultProps = { value: '', placeholder: null, helperText: null, feedback: {}, changeHandler: void 0 }
export default connect( ( { miscellaneous: { services, media } } ) => ( { services, media } ), { getServices } )( ServiceSelect )
