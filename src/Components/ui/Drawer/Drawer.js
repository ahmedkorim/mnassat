import Divider              from "@material-ui/core/Divider";
import Drawer               from "@material-ui/core/Drawer/Drawer";
import List                 from "@material-ui/core/List";
import ListItem             from "@material-ui/core/ListItem";
import React                from 'react'
import { Link, withRouter } from 'react-router-dom';

const SideDrawer = ( { open, toggleDrawer, links, history, children,callToActionLinks } ) => {
    return (
        <Drawer open={ open }
                anchor="left"
                PaperProps={ {
                    classes: {
                        root: 'MainDrawerPaper'
                    }
                } }
                ModalProps={ {
                    onEscapeKeyDown: toggleDrawer,
                    onBackdropClick: toggleDrawer,
                }
                }
        >
            { children }
            <List
            >
                { links.map( ( { name, path, title } ) => <ListItem
                    button
                    to={ path }
                    selected={ history.location.pathname === path }
                    key={ name }
                    component={ Link }
                >{ title }</ListItem> ) }
            </List>
            <Divider/>
            <List
            >
                { callToActionLinks.map( ( { name, path, title } ) => <ListItem
                    button
                    to={ path }
                    selected={ history.location.pathname === path }
                    key={ name }
                    component={ Link }
                >{ title }</ListItem> ) }
            </List>
        </Drawer>
    )
}

export default withRouter( SideDrawer )
