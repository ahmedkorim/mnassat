import { Badge }     from "@material-ui/core";
import Button        from "@material-ui/core/Button";
import Icon          from "@material-ui/core/Icon";
import Menu_         from '@material-ui/core/Menu';
import MenuItem      from '@material-ui/core/MenuItem';
import PropTypes     from 'prop-types'
import React         from 'react';
import { darkColor } from "src/styles/colors";


class DropDown extends React.Component {
    state = {
        anchorEl: null,
        offset: {
            top: null,
            left: null
        }
    };

    handleClick = event => {
        const anchorEl = event.currentTarget
        if ( this.state.offset.top === null ) {
            this.setState( {
                offset: {
                    top: anchorEl.offsetHeight,
                    left: anchorEl.offsetWidth
                }, anchorEl
            } )
        } else {
            this.setState( { anchorEl } );

        }
    };

    handleClose = () => {
        this.setState( { anchorEl: null } );
    };

    render () {
        const {
            anchorEl, offset: {
                top = 0,
                left = 0
            }
        } = this.state;
        const open = Boolean( anchorEl );
        const {
            itemHeight,
            renderTrigger,
            pWidth,
            fullHeight,
            pTop,
            hiddenBadge,
            paperStyles,
            options, children, theme, selected, handleChange, ...rest
        } = this.props
        return (
            <div { ...rest }>
                { renderTrigger ? renderTrigger( this.handleClick, this.handleClose ) : <Button
                    aria-label="More"
                    aria-owns={ open ? 'long-menu' : undefined }
                    aria-haspopup="true"
                    onClick={ this.handleClick }
                    fullWidth
                >
                    <div className="d-flex align-items-center w-100 ">
                        { children }<Icon fontSize="large" className="light__typography" style={ {
                        color: theme || darkColor,
                        marginRight: 'auto',
                        transition: 'all ease-in-out .2s'
                        , transform: `rotate(${ open ? '180' : '0' }deg)`
                    } }>arrow_drop_down</Icon>
                    </div>
                </Button> }
                <Menu_
                    id="long-menu"
                    anchorEl={ anchorEl }
                    open={ open }
                    onClose={ this.handleClose }
                    PaperProps={ {
                        style: {
                            maxHeight: !fullHeight ? itemHeight * 6 : null,
                            width: pWidth || left,
                            marginTop: ( pTop || top ) + itemHeight / 3,
                            marginLeft: 0,
                            borderRadius: 0,
                            ...paperStyles,
                        },
                    } }
                >
                    { options.map( option =>
                        typeof option !== "object" ? (
                            <MenuItem key={ option } selected={ option === selected } onClick={ () => {
                                this.handleClose()
                                handleChange( option )
                            } }
                            >
                                <span className="option__content">    { option }</span>
                            </MenuItem>
                        ) : <MenuItem key={ option.id } selected={ option.name === selected } onClick={ () => {
                            this.handleClose()
                            handleChange( option.name )
                        } }>
                            <div className={ option.component ? 'd-flex justify-content-center' : 'd-flex' }>
                                { option.component ? option.component() : <span className="option__content">       { option.name }</span> }
                                {/*               { !!( ( option.count && +option.count > 0 ) && !hiddenBadge ) &&
                                 <Badge badgeContent={ option.count + option.count } color="secondary"/> }*/ }
                            </div>
                        </MenuItem>
                    ) }
                </Menu_>
            </div>
        );
    }
}

export default DropDown;

DropDown.propTypes = {
    children: PropTypes.string,
    handleChange: PropTypes.func,
    itemHeight: PropTypes.number,
    options: PropTypes.array.isRequired,
    selected: PropTypes.string
}
DropDown.defaultProps = {
    itemHeight: 40,
    options: []
}
