import PropTypes                                                             from 'prop-types'
import React, { Fragment }                                                   from 'react';
import Toolbar                                                               from "@material-ui/core/Toolbar/Toolbar";
import Tabs                                                                  from "@material-ui/core/Tabs/Tabs";
import Tab                                                                   from "@material-ui/core/Tab/Tab";
import SwipeableViews                                                        from "react-swipeable-views";
import { withRouter }                                                        from 'react-router-dom';
import { withStyles, withWidth }                                             from "@material-ui/core";
import Badge                                                                 from "@material-ui/core/Badge/Badge";
import { darkColor, greyColor, lightColor, lightGrey, lightMain, mainColor } from "src/styles/colors";

const styles = theme => ( {
    shadow: {
        boxShadow: theme.shadows[ 3 ],
        maxHeight: 48,
        minHeight: 'unset',
        display: 'flex',
        justifyContent: 'center'
    },
    badgeContainer: {
        display: 'flex',
        alignItems: 'center'
    },
    badge: {
        position: 'relative !important',
        transform: 'none',
        left: 10,
        backgroundColor: lightMain,
        color: darkColor
    },
    margin: {
        margin: theme.spacing.unit,
    },
    fullWidthTabs: {
        width: '100%',
        display: "flex",
        justifyContent: "center"
    },
    padding: {
        padding: `0 ${ theme.spacing.unit }px`,
    },
} );

class EnhancedTabs extends React.Component {
    constructor ( props ) {
        super( props );
        if ( !props.disableRouting ) {
            const links = props.location.pathname.split( '/' );
            const tabs = props.tab || props.paddges
            const intialIndex = tabs.findIndex( item => item.label === links[ links.length - 1 ] );
            this.state = {
                value: intialIndex > 0 ? intialIndex : ( props.history.push( ( props.match.url !== "/" ? props.match.url : props.location.pathname ) + '/' + tabs[ 0 ].label ), 0 )

            }
        } else {
            this.state = {
                value: 0
            }
        }
    }


    handleChange = ( event, value ) => {
        this.setState( { value } );
        if ( this.props.disableRouting ) return;
        const tabs = this.props.tab || this.props.paddges
        this.props.history.push( ( this.props.match.url !== "/" ? this.props.match.url : this.props.location.pathname ) + '/' + tabs[ value ].label );
    };
    handleChangeIndex = index => {
        this.setState( { value: index } );
    };

    componentWillUpdate ( nextProps, nextState ) {
        if ( this.props.disableRouting ) return;
        if ( nextProps.location.pathname === this.props.location.pathname ) return;
        const links = nextProps.location.pathname.split( '/' );
        const nextTabs = nextProps.tab || nextProps.paddges

        const intialIndex = nextTabs.findIndex( item => item.label === links[ links.length - 1 ] );
        if ( intialIndex > -1 ) {
            this.setState( { value: intialIndex } );

        }
    }

    render () {
        let {
            state: {
                value
            },
            props: {
                children,
                toolbarClasses,
                tab,
                classes,
                centered,
                width,
                withFap,
                indercatorClass,
                rootClass,
                tabRootClass,
                selectClass,
                paddges,
                noscroll,
                noShadow
            },
            handleChange,
            handleChangeIndex

        } = this;
        toolbarClasses = toolbarClasses || [ !noShadow && classes.shadow, "toolBar" ];
        paddges = paddges || [];

        /*
         *
         * [{label:string,countNumber,icon:icon,color:string}]
         *
         * */
        return (
            <Fragment>
                <Toolbar className={ toolbarClasses.join( ' ' ) } style={ { minHeight: 'unset' } }>
                    <Tabs
                        value={ value }
                        onChange={ handleChange }
                        indicatorColor="primary"
                        textColor="primary"
                        variant={ ( !noscroll && width == 'xs' ) ? "scrollable" : '' }
                        scrollButtons={ ( width === 'xs' ) ? 'on' : 'off' }
                        classes={ {
                            scrollButtons: 'scrollingButton',
                            root: [ classes.fullWidthTabs, ...rootClass ].join( ' ' ),
                            indicator: indercatorClass
                        } }
                    >
                        {

                            paddges.length > 0 ? paddges.map( tab => <Tab label={
                                    tab.count > 0 ? <Badge color={ tab.color ? tab.color : "secondary" } badgeContent={ tab.count }
                                                           classes={ {
                                                               badge: classes.badge
                                                           } }
                                                           className={ classes.badgeContainer }
                                    >
                                        { tab.name }
                                    </Badge> : <span> { tab.name }</span>
                                } key={ tab.key ? tab.key : tab.label }
                                                                          classes={ {
                                                                              root: tabRootClass,
                                                                              selected: selectClass
                                                                          } }
                                /> )
                                :
                                tab.map( tab => <Tab label={ tab.label } key={ tab.key ? tab.key : tab.label }
                                                     classes={ {
                                                         root: tabRootClass,
                                                         selected: selectClass
                                                     } }
                                /> ) }
                    </Tabs>
                </Toolbar>
                <SwipeableViews
                    index={ value }
                    slideClassName="EnhancedTabsSlide"
                    onChangeIndex={ handleChangeIndex }
                >
                    { children }
                </SwipeableViews>
            </Fragment>
        )
    }
}


export default withRouter( withWidth()( withStyles( styles )( EnhancedTabs ) ) );

EnhancedTabs.propTypes = {
    disableRouting: PropTypes.bool,
    history: PropTypes.any,
    location: PropTypes.any,
    match: PropTypes.any,
    tab: PropTypes.array.isRequired
}
