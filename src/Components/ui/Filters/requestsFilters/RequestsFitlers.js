import { Button, Icon }                                  from "@material-ui/core";
import shadows                                           from "@material-ui/core/styles/shadows";
import Typography                                        from "@material-ui/core/Typography";
import React                                             from 'react'
import { connect }                                       from "react-redux";
import { below, size }                                   from "shared/ui/sc-breakpoint";
import DropDown                                          from "src/Components/ui/DropDown/DropDown";
import { accentColor, darkColor, lightColor, lightMain } from "src/styles/colors";
import styled                                            from 'styled-components';

const FilterWrapper = styled.section`

background-color: ${ ( { variantcolor } ) => variantcolor };
padding-top: .5rem;
padding-bottom: .5rem;
min-width: 100%;
box-shadow: ${ shadows[ 5 ] };
`
export const LightTypography = styled( Typography )`
&&{
color: ${ ( { variantcolor } ) => variantcolor }; !important;
}
`
const GoldenButton = styled( Button )`
&&{
background-color: ${ accentColor } !important;
border-radius: .2rem !important;
width:130px;
height: 50px;
}
`
const VerticalDivider = styled.div`
height: 100%;
width: 2px;
    background-color:${ lightMain };
    opacity: .2;
    `
const MenuWrapper = styled.div`
min-width:100px;
max-width: 210px !important;
${
    below.md`
    width:150px;
    margin-bottom:.5rem;
    `
    }
`
const RequestsFilter = ( { handler, width, options, filters = [], variant, search, extra, ...rest } ) => {
    const typegraphyColor = variant === 'light' ? darkColor : lightColor;
    const wrapperColor = variant === 'light' ? lightColor : darkColor;
    return (
        <FilterWrapper className="row align-items-center" variantcolor={ wrapperColor }>
            <div className="col-lg-10 col-12 col-md-8">
                <div className="d-flex flex-wrap justify-content-center justify-content-md-start">
                    {
                        extra &&
                        <MenuWrapper>  {
                            extra( {
                                theme: typegraphyColor,
                                style: { flexGrow: 1 },
                                pWidth: 170
                            }, LightTypography )
                        }
                            { width > size.md && <VerticalDivider/> }
                        </MenuWrapper>

                    }
                    {
                        filters.map( filter => <MenuWrapper
                            key={ filter }
                            className=" d-flex">
                            <DropDown
                                options={ options[ filter ] }
                                theme={ typegraphyColor }
                                style={ { flexGrow: 1 } }
                                selected={ rest[ filter ] }
                                pWidth={ 170 }
                                handleChange={ handler( filter ) }
                            >
                                <LightTypography variant="body2"
                                                 variantcolor={ typegraphyColor }
                                >
                                    { rest[ filter ] }</LightTypography>
                            </DropDown>
                            { width > size.md && <VerticalDivider/> }
                        </MenuWrapper> )
                    }
                </div>
            </div>
            <div className="col-lg-2   col-12 col-md-4 d-flex d-md-block py-4 py-md-0 justify-content-center"
                 style={ {
                     minWidth: 140
                 } }
            >
                <GoldenButton variant="contained" className="m-md-auto" { ...( search ? search.buttonProps : {} ) }>
                    <Icon>add</Icon>
                    { search ? search.buttonText : ' اضف طلبك' }
                </GoldenButton>
            </div>
        </FilterWrapper>
    );
}
export default connect( ( { ui: { width } } ) => ( {
    width
} ) )( RequestsFilter )
