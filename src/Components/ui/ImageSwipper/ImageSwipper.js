import shadows                  from "@material-ui/core/styles/shadows";
import React, { Component }     from 'react';
import Swiper                   from 'react-id-swiper/lib/ReactIdSwiper.full';
import { lightMain, mainColor } from "src/styles/colors";
import styled                   from 'styled-components';

const Image = styled.div`
background: ${ ( { imageUrl } ) => `url(${ imageUrl })` } no-repeat center center;
background-size:${ ( { variant } ) => variant || "cover" };
height:100%;
`

const Wrapper = styled.div`
height: 100%;
display: flex;
padding-bottom: .5rem;
flex-direction: column;
.gallery{
margin-bottom: 1%; 
flex:6;
}

.swiper-container{
height: 100%;
max-height:100% !important;
}
.thumbnails{
flex:1;
.swiper-slide-active{
box-shadow: ${ shadows[ 1 ] };
border:2px solid ${ lightMain };
padding: 2px;
}
.swiper-container{
height:100% !important;
}
.img-fluid{
max-height: 100%;
width: 150px;
}
.swiper-slide{
max-width: 150px;
}
}
.thumb-slide{
display: flex;
justify-content: center;
align-items: center;
}
`


const thumbnailSwiperParams = {
    paceBetween: 10,
    centeredSlides: true,
    slidesPerView: 5,
    spaceBetween: 10,
    touchRatio: 0.2,
    slideToClickedSlide: true
};

const gallerySwiperParams = {
    spaceBetween: 10,
    /*    navigation: {
     nextEl: '.swiper-button-next',
     prevEl: '.swiper-button-prev',
     }*/
};


export default class GalleryImage extends Component {
    constructor ( props ) {
        super( props );
        this.state = {
            gallerySwiper: null,
            thumbnailSwiper: null
        };

        this.galleryRef = this.galleryRef.bind( this );
        this.thumbRef = this.thumbRef.bind( this );
    }

    componentWillUpdate ( nextProps, nextState ) {
        if ( nextState.gallerySwiper && nextState.thumbnailSwiper ) {
            const { gallerySwiper, thumbnailSwiper } = nextState

            gallerySwiper.controller.control = thumbnailSwiper;
            thumbnailSwiper.controller.control = gallerySwiper;
        }
    }

    galleryRef ( ref ) {
        if ( ref ) this.setState( { gallerySwiper: ref } )
    }

    thumbRef ( ref ) {
        if ( ref ) this.setState( { thumbnailSwiper: ref } )
    }

    render () {

        const {
            props: {
                imagesArray
            }

        } = this
        return (
            <Wrapper>
                <div className="gallery">
                    <Swiper { ...gallerySwiperParams } getSwiper={ this.galleryRef }>
                        { imagesArray.map( i => <div key={ i }>
                            <Image imageUrl={ i }/>
                        </div> ) }
                    </Swiper>
                </div>
                <div className="thumbnails">
                    <Swiper { ...thumbnailSwiperParams } getSwiper={ this.thumbRef }>
                        { imagesArray.map( i => <div className="swiper-slide" key={ i + 'thumb' }>
                            <Image imageUrl={ i } variant="cover"/>
                        </div> ) }
                    </Swiper>
                </div>
            </Wrapper>
        );
    }
}
