import { TextField }       from "@material-ui/core";
import PropTypes           from 'prop-types'
import React, { useState } from 'react'
import handleInputState    from "shared/Forms/handleInputState";
import { greyColor }       from "src/styles/colors";

import styled from 'styled-components'


const CustomeInputField = styled( TextField )`

.input__textFiled-input {


}
.input__textFiled-input::placeholder{
color: ${ ( { feedbackcolor } ) => feedbackcolor } !important;
display: none;
}
.input__textFiled-label{
color: ${ ( { feedbackcolor } ) => feedbackcolor }  !important;
    margin-top: -5px !important;
    font-size:.9rem;
}
&&{

fieldset{
border-color:${ ( { feedbackcolor } ) => feedbackcolor }  !important;
border-radius: .5rem !important;
}


.input__textFiled-helper{
margin: 4px 4px;
color: ${ ( { feedbackcolor } ) => feedbackcolor } ;
}
}

`
const InputField = ( { value = '', name, label, placeholder = null, helperText = null, feedback = {}, changeHandler = void 0, readOnly, eagerValidation, ...rest } ) => {
    // instant feed back
    const [ focus, setFocus ] = useState( false );
    // after leaving the input feedback
    const [ dirty, setDirty ] = useState( false );

    const [ touch, setTouch ] = useState( false );


    const { feedbackColor, feed } = handleInputState(
        {
            focus, dirty, feedback, touch,
            eagerValidation, value, requried: rest.required
        } )
    helperText = feed

    return (

        <CustomeInputField
            fullWidth
            margin="dense"
            variant="outlined"
            placeholder={ placeholder }
            label={ label }
            value={ value }
            name={ name }
            readOnly={ readOnly }
            onChange={ ( e ) => {
                setDirty( true )
                !readOnly && changeHandler( e )
            } }
            onFocus={ () => {
                setFocus( true )
            } }
            onBlur={ ( e ) => {
                !readOnly && changeHandler( e )
                setFocus( false )
                setTouch( true )
            } }
            helperText={ helperText }
            feedbackcolor={ readOnly ? greyColor : feedbackColor }
            FormHelperTextProps={ {
                className: 'input__textFiled-helper',
                margin: "dense",
            } }
            inputProps={ {
                className: "input__textFiled-input"
            } }
            InputFieldLabelProps={ {
                className: "input__textFiled-label"

            } }
            { ...rest }
        /> )
}
export default InputField

InputField.propTypes = {
    changeHandler: PropTypes.any.isRequired,
    name: PropTypes.any.isRequired,
    helperText: PropTypes.any,
    label: PropTypes.any.isRequired,
    placeholder: PropTypes.any,
    value: PropTypes.string.isRequired
}

InputField.defaultProps = {
    changeHandler: void 0,
    helperText: null,
    placeholder: null,
    value: ''
}
