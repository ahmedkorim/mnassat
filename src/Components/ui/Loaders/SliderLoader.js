import React                                         from 'react'
import { ContentLoaderWrapper, StyledContentLoader } from "src/Components/ui/OfferCard/OfferCardLoader";

export const SliderLoader = _ => {
    return (
        <ContentLoaderWrapper>
            <StyledContentLoader
                height={250}
                width={400}
                speed={ 2 }
                primaryColor="#f3f3f3"
                secondaryColor="#ecebeb"
            >
                <rect x="2" y="3" rx="5" ry="5" width="393" height="172" />
                <rect x="12" y="192" rx="0" ry="0" width="77" height="37" />
                <rect x="114" y="192" rx="0" ry="0" width="77" height="37" />
                <rect x="206" y="192" rx="0" ry="0" width="77" height="37" />
                <rect x="303" y="192" rx="0" ry="0" width="77" height="37" />
            </StyledContentLoader>
        </ContentLoaderWrapper>
    )
}
export const ImageLoader = _ => ( <ContentLoaderWrapper>
    <StyledContentLoader
        height={ 300 }
        width={ 400 }
        speed={ 2 }
        primaryColor="#f3f3f3"
        secondaryColor="#ecebeb"
    />
</ContentLoaderWrapper> )

export const DetialsList = _ => <ContentLoaderWrapper>
    <StyledContentLoader
        height={ 210 }
        width={ 400 }
        speed={ 2 }
        primaryColor="#f3f3f3"
        secondaryColor="#ecebeb"
    >
        <rect x="309" y="14" rx="0" ry="0" width="82" height="14" />
        <rect x="167" y="39" rx="0" ry="0" width="226" height="16" />
        <rect x="165" y="115" rx="0" ry="0" width="226" height="16" />
        <rect x="89" y="62" rx="0" ry="0" width="301" height="8" />
        <rect x="90" y="101" rx="0" ry="0" width="301" height="8" />
        <rect x="166" y="79" rx="0" ry="0" width="226" height="16" />
    </StyledContentLoader>
</ContentLoaderWrapper>


export const OwnerDetailsLoader = _ => <ContentLoaderWrapper style={ { maxWidth: 300, margin: 'auto' } }>
    <StyledContentLoader
        height={ 400 }
        width={ 350 }
        speed={ 2 }
        primaryColor="#f3f3f3"
        secondaryColor="#ecebeb"
    >
        <rect x="220" y="20" rx="0" ry="0" width="96" height="0"/>
        <rect x="126" y="20" rx="0" ry="0" width="203" height="21"/>
        <rect x="270" y="60" rx="0" ry="0" width="60" height="47"/>
        <rect x="133" y="61" rx="0" ry="0" width="116" height="12"/>
        <rect x="77" y="80" rx="0" ry="0" width="173" height="13"/>
        <rect x="182" y="138" rx="20" ry="20" width="145" height="38"/>
        <rect x="9" y="139" rx="20" ry="20" width="145" height="38"/>
        <rect x="146" y="199" rx="0" ry="0" width="45" height="27"/>
        <rect x="31" y="244" rx="0" ry="0" width="276" height="83"/>
        <rect x="95" y="351" rx="20" ry="20" width="145" height="38"/>
    </StyledContentLoader>
</ContentLoaderWrapper>


export const ButtonLoader = _ => <ContentLoaderWrapper style={ { maxWidth: 110, minWidth: 90 } }>
    <StyledContentLoader
        height={ 45 }
        width={ 110 }
        speed={ 2 }
        primaryColor="#f3f3f3"
        secondaryColor="#ecebeb"
    >
        <circle cx="23" cy="24" r="20"/>
        <circle cx="80" cy="24" r="20"/>
    </StyledContentLoader>
</ContentLoaderWrapper>


export const DetailsHeader = _ => <ContentLoaderWrapper style={ {
    maxWidth: 250,
    minWidth: 220
} }>
    <StyledContentLoader
        height={ 70 }
        width={ 200 }
        speed={ 2 }
        primaryColor="#f3f3f3"
        secondaryColor="#ecebeb"
    >
        <rect x="48" y="13" rx="0" ry="0" width="149" height="21"/>
        <rect x="80" y="48" rx="0" ry="0" width="40" height="7"/>
        <rect x="123" y="48" rx="0" ry="0" width="72" height="7"/>
        <rect x="10" y="12" rx="0" ry="0" width="23" height="21"/>
    </StyledContentLoader>
</ContentLoaderWrapper>
