import React                                         from "react";
import { OfferCardWrapper }                          from "src/Components/ui/OfferCard/OfferCard";
import { ContentLoaderWrapper, StyledContentLoader } from "src/Components/ui/OfferCard/OfferCardLoader";


const TeamMemberLoader = () => {


    return <OfferCardWrapper>
        <ContentLoaderWrapper>
            <StyledContentLoader
                height={ 330 }
                width={ 270 }
                speed={ 2 }
                primaryColor="#f3f3f3"
                secondaryColor="#ecebeb"
            >
                <rect x="-11" y="-81" rx="5" ry="5" width="396" height="252"/>
                <circle cx="168" cy="137" r="1"/>
                <rect x="93" y="189" rx="0" ry="0" width="158" height="15"/>
                <circle cx="369" cy="390" r="16"/>
                <rect x="0" y="0" rx="0" ry="0" width="0" height="0"/>
                <circle cx="369" cy="335" r="16"/>
                <rect x="137" y="0" rx="335" ry="290" width="197" height="26"/>
                <circle cx="369" cy="290" r="16"/>
                <rect x="115" y="214" rx="0" ry="0" width="132" height="9"/>
                <rect x="84" y="245" rx="0" ry="0" width="146" height="12"/>
                <rect x="188" y="249" rx="0" ry="0" width="0" height="0"/>
                <rect x="86" y="270" rx="0" ry="0" width="146" height="12"/>
                <rect x="87" y="292" rx="0" ry="0" width="146" height="12"/>
                <circle cx="249" cy="276" r="9"/>
                <circle cx="249" cy="297" r="9"/>
                <circle cx="249" cy="253" r="9"/>
            </StyledContentLoader>
        </ContentLoaderWrapper>
    </OfferCardWrapper>
}

export default TeamMemberLoader
