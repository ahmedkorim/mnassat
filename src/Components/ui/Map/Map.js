import React                             from 'react';
import scriptLoader                      from 'react-async-script-loader';
import { withRouter }                    from "react-router";
import Spinner                           from "src/Components/ui/Spinner";
import { defaultMapLocation, locations } from "src/configs/maps/googleMaps";
import { lightMain, mainColor }          from "src/styles/colors";
import styled                            from 'styled-components';
import * as  MarkerClusterer             from '@google/markerclusterer';
import clusterImage                      from 'assets/Images/m1.png'
import pointer                           from 'assets/Images/mapAssets/pointer.png'

// console.log( clusterImage );
const MapWrapper = styled.div`
width: 100%;
height: ${ ( { variant: { height } } ) => height || 500 }px;
`

class Map extends React.Component {
    state = {
        mapReady: false,
    }
    markers = [];
    markerImage = null;
    initMap = () => {
        if ( !window.google ) return;
        const that = this;
        if ( typeof window.google !== void 0 + '' ) {
            this.markerImage = new window.google.maps.MarkerImage( 'http://chart.googleapis.com/chart?chst=d_map_spin&chld=1.15|0|' + lightMain.replace( '#', '' ) +
                '|40|_|%E2%80%A2',
                new window.google.maps.Size( 21, 34 ),
                new window.google.maps.Point( 0, 0 ),
                new window.google.maps.Point( 10, 34 ),
                new window.google.maps.Size( 21, 34 ) )
            // console.log( this.markerImage );
        }
        const center = new window.google.maps.LatLng( defaultMapLocation.lat, defaultMapLocation.lng );
        this.map = new window.google.maps.Map( this.mapWrapper, {
            center: center,
            zoom: that.props.zoom || 7,
            mapTypeId: window.google.maps.MapTypeId.ROADMAP
        } );
        this.setState( { mapReady: true } );

        this.infoWindow = new window.google.maps.InfoWindow()
        if ( this.props.locations.length !== 0 ) {
            this.createMarkers( this.props.locations )
        }
    };


    createMarkers = ( locations = [] ) => {
        const that = this;
        if ( !window.google ) return;
        this.markers.forEach( i => i.setMap( null ) )
        this.markers = [];
        const bounds = new window.google.maps.LatLngBounds();

        locations.forEach( ( { longitude, latitude, title, id } ) => {
            let latLng = new window.google.maps.LatLng( +latitude, +longitude );

            const marker = new window.google.maps.Marker( {
                position: latLng,
                title,
                id,
                animation: window.google.maps.Animation.DROP,
                map: that.map,
                icon: {
                    url: pointer,
                    scaledSize: new google.maps.Size( 26, 36 ), // scaled size
                    origin: new google.maps.Point( 0, 0 ), // origin
                    anchor: new google.maps.Point( 0, 0 )
                }
            } )
            bounds.extend( marker.position );

            this.markers.push( marker )
            if ( that.props.showInfoWindow ) {
                marker.addListener( 'click', _ => {
                    that.populateInfoWindow( marker, that.infoWindow )
                } )
            }

        } )
        this.map.fitBounds( bounds );

        this.markerCluster = new MarkerClusterer( this.map, this.markers, {
            maxZoom: 9,
            gridSize: 140,
            styles: [ {
                url: '/Images/mapAssets/cloud1.png',
                width: 72,
                height: 72,
                textColor: '#fff',
            } ]

        } )
        // console.log( this.markerCluster );
    }
    populateInfoWindow = ( marker, infoWindow ) => {
        if ( infoWindow.marker !== marker ) {
            // console.log( marker );
            const that = this;
            const product = this.props.locations.find( i => i.id === marker.id ) || {}
            const key = this.props.showInfoWindow.keys
            const filters = ( ( this.props.showInfoWindow.data || {} )[ key ] ) || [];
            const value = filters.find( i => i.id === ( product[ key ] || [] )[ 0 ] )
            window.__key = key
            window.__filter = filters
            window.__handle_map_Click = () => that.props.detials && that.props.detials(marker.id)
            infoWindow.setContent( `
                   <div class="google__maps-infowindow-PC">
                    ${ product.featured_img_url ? `<img src="${ product.featured_img_url }" alt="${ marker.title }"/>` : '' }
                    <h5>${ marker.title }</h5>
                    <div class="google__maps-api-cat"> 
                   ${ value ? value.name : '' }
                    </div>
                    <button onclick="__handle_map_Click()">التفاصيل</button>
                ${ product.property_price ? `        
                  <div class="google__maps-api-price">
                   ${ product.property_price } السعر 
                    </div>` : '' }
                    </div>
            ` );
            infoWindow.marker = marker;
            addEventListener( 'closeclick', function () {
                infoWindow.marker = null;
            } );
            infoWindow.open( this.map, marker )
        }


    };

    componentWillUnmount () {
        this.map = null
    }

    shouldComponentUpdate ( nextProps, nextState, nextContext ) {
        if ( ( nextProps.locations.length === this.props.locations.length ) && this.state.mapReady ) {
            return false
        }
        console.log( MarkerClusterer );
        return true;
    }

    componentWillUpdate ( { locations }, nextState, nextContext ) {
        if ( !this.state.mapReady ) this.initMap();

        if ( ( locations.length !== this.props.locations.length && locations.length !== 0 ) || this.map ) {
            this.createMarkers( locations )

        }
    }


    componentWillReceiveProps ( nextProps, nextContext ) {
        if ( ( nextProps.locations.length !== this.props.locations.length ) ) {
            this.initMap();

        }
    }

    componentDidMount () {

        if ( !this.map ) {
            // console.log( 'mounted without the map' );
            this.initMap()
        }
        this.map && this.createMarkers( locations )

    }

    render () {
        const { locations, loading, height } = this.props;
        return (
            <div style={ { position: 'relative' } }>
                <MapWrapper variant={ { height } } ref={ node => this.mapWrapper = node } id="map" role="application"/>
                { ( locations.length === 0 && loading ) && <Spinner/> }
            </div>
        )
    }
}

export default withRouter( Map )
