import CardContent                                      from "@material-ui/core/CardContent";
import shadows                                          from "@material-ui/core/styles/shadows";
import PropTypes                                        from 'prop-types'
import { Icon, IconButton, Button, Tooltip, CardMedia } from "@material-ui/core";
import Card                                             from "@material-ui/core/Card";
import React                                            from 'react'
import { connect }                                      from "react-redux";
import { above, below }                                 from "shared/ui/sc-breakpoint";
import ShareButton                                      from "src/Components/ui/shareButton/ShareButton";
import { toggleFavorites }                              from "src/store/actions/usersActions";
import { accentColor, darkColor, lightMain, mainColor } from "src/styles/colors";
import styled                                           from 'styled-components';
import { Link }                                         from 'react-router-dom'
import verfied                                          from 'assets/Images/Group 553.svg';
import specical                                         from 'assets/Images/Group 554.svg';

const Padge = styled.div`
color: ${ accentColor };
background-color: ${ ( { transparent } ) => !transparent && darkColor };
padding: .3rem .8rem;
border-radius:.2rem ;
 ${ ( { toRight } ) => toRight ? 'left:.7rem' : 'right:.7rem' };
top: .7rem;
font-weight: bold;
position: absolute;
z-index: 5;
display: flex;
font-size:.9rem;
justify-content: center;
align-items: center;
box-shadow: ${ ( { transparent } ) => !transparent && shadows[ 1 ] };
img{
width:32px;
}

`


export const OfferCardWrapper = styled( Card )`
margin: .6rem;
display: flex;
    width: calc(100% - 1.2rem);

${
    above.sm`
    width: calc(50% - 1.2rem);
    `
    };
${
    above.md`
     width: calc(33.33% - 1.2rem);

    `
    };

    ${ above.lg`
     width: calc(25% - 1.2rem);

    `
    };
border-radius: 0 !important;
flex-direction: column;
text-align:right;
flex-wrap: wrap;
position: relative;

.card__media-wrapper{
width: 100%;

position: relative;

}
.card__content-offer{
padding: 1rem 1rem .5rem !important;
display: flex;
flex-wrap: wrap;
flex-grow: 1;
flex-direction: row;



}
.card__img-team-member{
   height: 0;
    padding-top: 67.77%;
}
`
export const DetailsButton = styled( Button )`
&&{
color:${ mainColor }
background-color: ${ lightMain };
width: 100px;
}
`
const OfferCard = ( { data: { title, property_status, prop_featured, featured_img_url, property_county, inUserFavorites, property_address, _street, id, property_size, property_price, ...r }, toggleFavorites } ) => {

    return (
        <OfferCardWrapper>
            <Padge
                toRight
            >
                { property_price } ريال
            </Padge>
            { property_status === 'verified' || prop_featured && <Padge
                transparent
            >
                { property_status === 'verified' && <img src={ verfied } alt=""/> }
                { prop_featured && <img src={ specical } alt=""/> }
            </Padge> }
            <div className="card__media-wrapper">
                <CardMedia
                    image={ featured_img_url || 'http://via.placeholder.com/442' }
                    title="Paella dish"
                    className="card__img-team-member"
                />
            </div>
            <CardContent className="card__content-offer">
                <div className="w-100">
                    <h5>{ title }</h5>
                    <h6>{ `${ property_county }  ${ property_address }` }</h6>
                </div>
                <div className="d-flex align-items-center pt-4 mt-auto w-100">
                    <div className="ml-auto">
                        <DetailsButton color="primary" component={ Link } to={ `/details/${ id }?type=offer` }>التفاصيل</DetailsButton>
                    </div>
                    <div>
                        <ShareButton
                            url={ `/details/${ id }?type=offer` }
                        />
                        <Tooltip title={ inUserFavorites ? 'احذف من المفضله' : 'اضف الي المفضله' }
                                 children={ <IconButton color={ inUserFavorites ? 'secondary' : 'default' }
                                                        onClick={ () => toggleFavorites( id ) }><Icon>favorite</Icon></IconButton> }
                        />
                    </div>
                </div>
            </CardContent>
        </OfferCardWrapper>
    )
}
export default connect( null, dispatch => ( { toggleFavorites: ( id ) => dispatch( toggleFavorites( id ) ) } ) )( OfferCard )

OfferCard.propTypes = {
    name: PropTypes.any
}
