import React                from "react";
import ContentLoader        from "react-content-loader";
import { OfferCardWrapper } from "src/Components/ui/OfferCard/OfferCard";
import styled               from 'styled-components';

export const ContentLoaderWrapper = styled.div`
  display: block;
  flex-grow:1;
  width: 100%;
`
export  const StyledContentLoader = styled( ContentLoader )`
display: block;
`
const OfferCardLoader = () => {

    return <OfferCardWrapper>
        <ContentLoaderWrapper>
            <StyledContentLoader
                height={ 300 }
                width={ 270 }
                speed={ 2 }
                primaryColor="#f3f3f3"
                secondaryColor="#ecebeb"
            >
                <rect x="-11" y="-81" rx="5" ry="5" width="396" height="252"/>
                <circle cx="168" cy="137" r="1"/>
                <rect x="93" y="189" rx="0" ry="0" width="158" height="15"/>
                <circle cx="369" cy="390" r="16"/>
                <rect x="0" y="0" rx="0" ry="0" width="0" height="0"/>
                <circle cx="369" cy="335" r="16"/>
                <rect x="137" y="0" rx="335" ry="290" width="197" height="26"/>
                <circle cx="369" cy="290" r="16"/>
                <rect x="118" y="223" rx="0" ry="0" width="127" height="12"/>
                <rect x="157" y="265" rx="0" ry="0" width="93" height="25"/>
                <circle cx="73" cy="281" r="15"/>
                <circle cx="26" cy="281" r="15"/>
            </StyledContentLoader>
        </ContentLoaderWrapper>
    </OfferCardWrapper>
}

export default OfferCardLoader
