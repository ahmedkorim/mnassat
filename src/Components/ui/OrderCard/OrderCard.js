import PropTypes                             from 'prop-types'
import { Icon, IconButton, Button, Tooltip } from "@material-ui/core";
import Card                                  from "@material-ui/core/Card";
import React                                 from 'react'
import { connect }                           from "react-redux";
import ShareButton                           from "src/Components/ui/shareButton/ShareButton";
import { toggleFavorites }                   from "src/store/actions/usersActions";
import { lightMain, mainColor }              from "src/styles/colors";
import styled                                from 'styled-components';
import { above }                             from "shared/ui/sc-breakpoint";
import { Link }                              from 'react-router-dom'

export const OrderCardWrapper = styled( Card )`
padding: 1rem;
flex-grow: 1;
box-sizing: border-box;

    width: calc(100% - 1.2rem);

${
    above.sm`
    width: calc(50% - 1.2rem);
    max-width: calc(50% - 1.2rem);
    `
    };
${
    above.md`
     width: calc(33.33% - 1.2rem);
      max-width: calc(33.33% - 1.2rem);

    `
    };

    ${ above.lg`
     width: calc(25% - 1.2rem);
      max-width: calc(25% - 1.2rem);

    `
    };
margin: .4rem;
display: block;
border-radius: 0 !important;
text-align:right;
`
const DetailsButton = styled( Button )`
&&{
color:${ mainColor }
background-color: ${ lightMain };
width: 100px;
}
`
const OrderCard = ( { data: { inUserFavorites, id, title, demand_city, _street }, requestFilters, toggleFavorites } ) => {
    demand_city = requestFilters[ 'demand_city' ].find( i => i.id === +demand_city )
    return (
        <OrderCardWrapper>
            <h5>{ title }</h5>
            <h6>{ `${ demand_city ? demand_city.name : "" }${ demand_city && _street ? ' ,' : ' ' } ${ _street }` }</h6>
            <div className="d-flex align-items-center pt-4">
                <div className="ml-auto">
                    <DetailsButton component={ Link } to={ `/details/${ id }?type=request` } color="primary">التفاصيل</DetailsButton>
                </div>
                <div>
                    <ShareButton url={ `/details/${ id }?type=request` }/>
                    <Tooltip title={ inUserFavorites ? 'احذف من المفضله' : 'اضف الي المفضله' }
                             children={ <IconButton color={ inUserFavorites ? 'secondary' : 'default' }
                                                    onClick={ () => toggleFavorites( id ) }><Icon>favorite</Icon></IconButton> }
                    />
                </div>
            </div>
        </OrderCardWrapper>
    )
}
export default connect( ( { requests: { requestFilters } }, ) => ( { requestFilters } ),
        dispatch => ( { toggleFavorites: ( id ) => dispatch( toggleFavorites( id ) ) } ) )( OrderCard )

OrderCard.propTypes = {
    name: PropTypes.any
}
