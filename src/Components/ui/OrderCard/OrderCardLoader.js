import React                                         from "react";
import { ContentLoaderWrapper, StyledContentLoader } from "src/Components/ui/OfferCard/OfferCardLoader";
import { OrderCardWrapper }                          from "src/Components/ui/OrderCard/OrderCard";

const OrderCardLoader = () => {

    return <OrderCardWrapper> <ContentLoaderWrapper>
        <StyledContentLoader
            height={ 150 }
            width={ 230 }
            speed={ 2 }
            primaryColor="#f3f3f3"
            secondaryColor="#ecebeb"
        >
            <rect x="103" y="79" rx="0" ry="0" width="0" height="0"/>
            <rect x="67" y="45" rx="0" ry="0" width="0" height="0"/>
            <rect x="58" y="23" rx="0" ry="0" width="150" height="17"/>
            <rect x="65" y="52" rx="0" ry="0" width="142" height="8"/>
            <rect x="144" y="89" rx="0" ry="0" width="68" height="17"/>
            <circle cx="59" cy="98" r="12"/>
            <circle cx="21" cy="99" r="12"/>
        </StyledContentLoader>
    </ContentLoaderWrapper>
    </OrderCardWrapper>
}

export default OrderCardLoader
