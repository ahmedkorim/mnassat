import PropTypes                            from 'prop-types'
import { Button, Tooltip, Typography }      from "@material-ui/core";
import Icon                                 from "@material-ui/core/Icon";
import React                                from 'react'
import { WindowSize }                       from "shared/ui/sc-breakpoint";
import DropDown                             from "src/Components/ui/DropDown/DropDown";
import { LightTypography }                  from "src/Components/ui/Filters/requestsFilters/RequestsFitlers";
import { darkColor, lightColor, mainColor } from "src/styles/colors";
import styled                               from "styled-components";

const Wrapper = styled.div`
padding: 1rem 0;
display: flex;
justify-content: center;
align-items: center;
`

const PaginationButton = styled( Button )`
&&{
border-radius: 0 !important;
min-height: unset !important;
height:40px !important;
&:first-of-type{
border-radius: 0 10px  10px 0 !important;
}
&:last-of-type{
border-radius: 10px 0 0   10px !important;

}
}
`
const Pagination = ( { variant, renderedLength, current, length, handler, handlePerPage, perPage } ) => {
    current = Math.max( +current, 1 )
    const _current = current;
    length = +length || 1
    const last = Math.ceil( renderedLength / perPage );
    current = ( ( _current < last ) && length ) ? _current : last || _current;
    current !== _current && handler( current )
    const { width } = WindowSize()
    return (
        <Wrapper>
        {/*    {width > 580 && <DropDown
                    options={ [ 10, 20, 30, 45
                    ] }
                    theme={ darkColor }
                    style={ { maxWidth: 150 } }
                    key={ 'asdf' }
                    selected={ perPage }
                    handleChange={ ( value ) => handlePerPage( value ) }
                >
                    <Typography variant="body1">
                        { perPage }عنصر \ صفحه
                    </Typography>
                </DropDown>
            }*/}
            <span className="px-2"/>
            <Tooltip title="الصفحه السابقه"
                     children={ <PaginationButton size="small" variant="outlined" onClick={ () => handler( current - 1 ) }
                                                  disabled={ current === 1 }
                                                  color="primary"><Icon>navigate_next</Icon></PaginationButton>

                     }/>
            { current !== 1 && <PaginationButton variant="outlined" size="small" color="primary" onClick={ () => handler( 1 ) }>الاولي</PaginationButton> }
            { current - 2 > 1 &&
            <PaginationButton variant="outlined" size="small" color="primary" onClick={ () => handler( current - 2 ) }>{ current - 2 }</PaginationButton> }
            { current - 1 > 1 &&
            <PaginationButton variant="outlined" size="small" color="primary" onClick={ () => handler( current - 1 ) }>{ current - 1 }</PaginationButton> }
            <PaginationButton variant="raised" size="small"
                              color="primary">{ current === 1 || current === last ? current === 1 ? 'الاولي' : 'الاخيره' : current }</PaginationButton>
            { length !== 0 && <> {
                current + 1 < last &&
                <PaginationButton variant="outlined" size="small" color="primary" onClick={ () => handler( current + 1 ) }>{ current + 1 }</PaginationButton>
            }
                { current + 2 < last &&
                <PaginationButton variant="outlined" size="small" onClick={ () => handler( current + 2 ) } color="primary">{ current + 2 }</PaginationButton>


                }
                { current !== last &&
                <PaginationButton variant="outlined" size="small" color="primary" onClick={ () => handler( last ) }>الاخيره</PaginationButton> }
            </> }
            <Tooltip title="الصفحه التاليه"
                     children={ <PaginationButton size="small" variant="outlined" onClick={ () => handler( current + 1 ) }
                                                  disabled={ current === last }
                                                  color="primary"><Icon>navigate_before</Icon></PaginationButton>
                     }/>
        </Wrapper>
    )
}
export default Pagination

Pagination.propTypes = {
    current: PropTypes.number.isRequired,
    handler: PropTypes.func.isRequired,
    last: PropTypes.number.isRequired
}
