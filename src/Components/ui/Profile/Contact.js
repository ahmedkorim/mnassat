import { ListItemIcon, Typography } from "@material-ui/core";
import Avatar                       from '@material-ui/core/Avatar';
import Divider                      from '@material-ui/core/Divider';
import Icon                         from "@material-ui/core/Icon";
import ListItem                     from '@material-ui/core/ListItem';
import ListItemAvatar               from '@material-ui/core/ListItemAvatar';
import ListItemText                 from '@material-ui/core/ListItemText';
import React                        from 'react';


const Contact = ( { message, messageFrom, selected, selectContact } ) => {
    const name = messageFrom ? messageFrom.name : 'مستخدم منصات';
    const avatar = messageFrom ? messageFrom.avatar_urls[ '96' ] : 'account_circle'
    const date = new Date( message.date )
    const day = date.getDay()
    const month = date.getMonth() + 1;
    const year = date.getFullYear()
    const messageText = message.message.replace( '</p>', '' ).replace( '<p>', '' )
    const contactedId = message.contactId
    return (
        <>
            <ListItem alignItems="flex-start"
                      button
                      selected={ selected === contactedId }
                      onClick={ () => selectContact( contactedId ) }
                      className="position-relative"
            >
                {
                    messageFrom ? <ListItemAvatar>
                            <Avatar alt={ name } src={ avatar }/>
                        </ListItemAvatar> :
                        <ListItemIcon>
                            <Icon>{ avatar }</Icon>
                        </ListItemIcon>
                }
                <ListItemText
                >
                    <div>
                        <div className="d-flex align-items-center justify-content-between">
                            <Typography
                                component="div"
                                variant="body1"
                                className="d-inline font-weight-bold"
                            >{ name.slice( 0, 7 ) + ( name.length > 7 ? '..' : '' ) }</Typography>
                            <Typography
                                component="div"
                                variant="body2"
                                className=""
                                color="textPrimary"
                            >{ `${ day }/${ month }/${ year }` }</Typography>
                        </div>
                        <div>
                            { messageText.slice( 0, 13 ) + ( messageText.length > 50 ? '...' : '' ) }
                        </div>
                    </div>
                </ListItemText>
            </ListItem>
            <Divider variant="inset" component="li"/>
        </>
    );
}
export default Contact
