import Axios             from "axios";
import React             from 'react';
import { connect }       from "react-redux";
import { getCookie }     from "shared/auth/cookies.helper";
import formRender        from "shared/Forms/formRender";
import withFormState     from "shared/Forms/withFormState";
import { FormSubmitter } from "src/Components/shared/AboutUs/ContactWithUs";
import axios             from 'axios';
import editProfile       from "src/configs/Forms/editProfileForm";
import { wpHome }        from "src/configs/urls";
import { setUserData }   from "src/store/actions/usersActions";

class EditProfile extends React.Component {


    componentDidMount () {
        if ( this.props.currentUser.native ) {
            const first_name = ( this.props.currentUser.native || {} ).first_name || ""
            const last_name = ( this.props.currentUser.native || {} ).last_name || ""
            const name = ( this.props.currentUser.native || {} ).name || ""
            const email = ( this.props.currentUser.native || {} ).email || ""
            const phone = ( this.props.currentUser.native || {} ).phone || ""
            this.props.populate( {
                first_name,
                last_name,
                name,
                email,
                phone,
            } )
        }

    }


    componentWillUpdate ( nextProps, nextState, nextContext ) {
        if ( nextProps.currentUser && this.props.currentUser && nextProps.currentUser.native && !this.props.currentUser.native ) {
            const first_name = ( nextProps.currentUser.native || {} ).first_name || ""
            const last_name = ( nextProps.currentUser.native || {} ).last_name || ""
            const name = ( nextProps.currentUser.native || {} ).name || ""
            const email = ( nextProps.currentUser.native || {} ).email || ""
            const phone = ( nextProps.currentUser.native || {} ).phone || ""
            this.props.populate( {
                first_name,
                last_name,
                name,
                email,
                phone,
            } )
        }
    }

    editProfile = async ( e ) => {
        e.preventDefault();
        const state = await this.props.validateNow();
        if ( state ) {
            const token = getCookie( "__ssr-auth" );
            axios.post( `${ wpHome }/me`, this.props.value, {
                headers: {
                    Authorization: `Bearer ${ token }`
                }
            } )

        }

    }

    render () {
        const { handleChange, value, form, eagerValidation, currentUser } = this.props;


        return (
            <form noValidate className="row px-4 py-5"
                  onSubmit={ this.editProfile }
            >
                {
                    formRender( { body: form, value: value }, handleChange, eagerValidation )
                }
                <FormSubmitter variant="extended" color="primary" type="submit"> عدل البيانات</FormSubmitter>
            </form>
        )
    }
}

export default connect( ( { user: { currentUser } } ) => ( { currentUser } ), { setUserData } )(
    withFormState( editProfile.formState, editProfile.form )( EditProfile )
);
