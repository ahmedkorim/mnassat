import Button                    from "@material-ui/core/Button";
import React                     from 'react';
import PropertyCardProfile       from "src/Components/ui/Profile/PropertyCard";
import { darkColor, lightColor } from "src/styles/colors";


class Favorites extends React.Component {
    state = {
        page: 'offers'
    }
    setPage = ( page ) => () => {
        this.setState( {
            page
        } )
    }

    render () {
        const {
            state: {
                page
            },
            setPage,
            props: {
                favorites,
                offers = [],
                requests = [],
            }
        } = this
        const favOffers = offers.filter( ( { id } ) => favorites.find( ( fav ) => fav == id ) )
        const favRequest = requests.filter( ( { id } ) => favorites.find( ( fav ) => fav == id ) )
        console.log( favRequest );
        return (
            <div>
                <div className="d-flex align-items-center justify-content-center rtl">
                    <Button
                        onClick={ setPage( 'offers' ) }
                        style={ {
                            backgroundColor: page === 'offers' ? darkColor : '',
                            color: page === 'offers' ? lightColor : '',
                            borderRadius: 0
                        } }
                    >عروض</Button><Button
                    style={ {
                        backgroundColor: page === 'requests' ? darkColor : '',
                        color: page === 'requests' ? lightColor : '',
                        borderRadius: 0
                    } }
                    onClick={ setPage( 'requests' ) }
                >طلبات</Button>
                </div>
                {
                    page === 'offers' ?
                        <div>
                            { favOffers.length > 0 ? favOffers.map( ( { id, featured_img_url, title, property_price, property_county, property_address } ) =>
                                <PropertyCardProfile
                                    id={ `profile__offer ${ id }` }
                                    _id={ id }
                                    imgSrc={ featured_img_url }
                                    price={ property_price }
                                    title={ title }
                                    inUserFavorites
                                    address={ `${ property_county } ${ property_address }` }
                                /> ) : <div className="p-4 text-center font-weight-bold">لا توجد عروض</div> }
                        </div>
                        :
                        <div>
                            { favRequest.length > 0 ? favRequest.map( ( { id, title, demand_city, _street } ) =>
                                <PropertyCardProfile
                                    id={ `profile__request ${ id }` }
                                    title={ title }
                                    _id={ id }
                                    inUserFavorites
                                    address={ `${ demand_city } ${ _street }` }
                                /> ) : <div className="p-4 text-center font-weight-bold">لا توجد طلبات</div> }
                        </div> }
            </div>
        )
    }
}

export default Favorites;
