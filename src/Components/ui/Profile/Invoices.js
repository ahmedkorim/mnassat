import Button                                 from "@material-ui/core/Button";
import React                                  from 'react';
import { Link }                               from 'react-router-dom'
import { accentColor, darkColor, lightColor } from "src/styles/colors";
import styled                                 from 'styled-components';

const Wrapper = styled.div`
direction: rtl;
margin-bottom: .5rem;
padding: 2rem 1rem;
border-radius: .25rem;
background-color: ${ darkColor };
display: flex;
color: ${ lightColor } !important;
ul{
list-style: none;
padding: 0;
margin: 0;
li{
margin-bottom: .5rem;
}
dt,dd{
display: inline;     
}
}
`

const Price = styled.div`
font-size:4rem;
color: ${ accentColor };
`

const Invoices = ( { invoices = [] } ) => {
    console.log(invoices);
    return (
        <>
            { invoices.map( ( { id, get_only_meta, item_id, invoice_type, item_price } ) => <Wrapper
                key={ id }
            >
                <div className="col-6">
                    <ul>
                        <li>
                            <dt>اسم الخدمه</dt>
                            <dd>{ invoice_type }</dd>
                        </li>
                        { get_only_meta && get_only_meta.purchase_date && <li>
                            <dt>تاريخ الشراء</dt>
                            <dd>{ get_only_meta.purchase_date[ 0 ] }</dd>
                        </li> }
                        <li>
                            <dt>رقم الفاتوره</dt>
                            <dd>{ item_id }</dd>
                        </li>
                    </ul>
                </div>
                <div className="col-6 text-center">
                    <div>
                        المبلغ المدفوع
                    </div>
                    <Price>
                        { item_price }
                    </Price>
                    <div>ريال</div>
                </div>
            </Wrapper> ) }
            <Button component={ Link } to={ "/services" }>اطلب خدمه { invoices.length > 0 ? "جديدة" : '' }</Button>
        </>

    )
}

export default Invoices;
