import Button        from "@material-ui/core/Button";
import List          from "@material-ui/core/List";
import React         from 'react';
import { connect }   from "react-redux";
import Contact       from "src/Components/ui/Profile/Contact";
import MessagesView  from "src/Components/ui/Profile/MessagesView";
import { greyColor } from "src/styles/colors";
import styled        from 'styled-components';
import { size }      from "shared/ui/sc-breakpoint";

const MessageWrapper = styled.div`
direction:rtl;
border-radius: .25rem;
border: 1px solid ${ greyColor };
overflow: hidden;
 header{
 height: 60px;
 display: flex;
 align-items: center;
 box-sizing: border-box;
  padding: .4rem 1rem;
  text-align: center;
 }

 
 .message__body{
   border:  1px solid ${ greyColor };
   border-left-color:transparent ;
   border-right-color:transparent ;
  border-bottom-color:transparent ;

 }
 .chat__col{
   border:1px solid ${ greyColor };
  border-right-color:transparent ;
  border-top-color:transparent ;
  border-bottom-color:transparent ;
 }
 .message__col{
    border:  1px solid transparent;

 }
 .chat__body{
   border:1px solid ${ greyColor };
  border-right-color:transparent ;
  border-left-color:transparent ;
  border-bottom-color:transparent ;
 }
 
`


const StyledList = styled( List )`
        width: 100%;
        padding: 0 !important;
        margin: 0 !important;
        max-width: 360px;
`

class Messages extends React.Component {

    state = {
        selectedContact: null,
        show: "contacts"
    }


    selectContact = ( selectedContact ) => {
        console.log( selectedContact );
        this.setState( { selectedContact } )
    }

    render () {
        const {
            props: {
                messages,
                currentUser,
                width,
                height,
                currentUser: { id }
            },
            state: {
                selectedContact
            },
            selectContact,
        } = this;
        console.log( this.props );
        const contacts = messages.reduce( ( acc, message ) => acc.find( ( { contactId } ) => contactId === message.contactId ) ? acc : [ ...acc, message ]
            , [] )
        const selectedContactMess = contacts.find( ( { messageFrom } ) => messageFrom && messageFrom.id === selectedContact )
        const name = selectedContactMess ? selectedContactMess.messageFrom.name : 'مستخدم منصات'
        const tabsStyle = { height: height * .65, overFlowY: 'auto', overflowX: 'hidden' }
        console.log( width );
        return (
            <MessageWrapper className="w-100 h-100">
                <div className="row no-gutters h-100">
                    {
                        ( width >= size.md || !selectedContact ) && <div className=" col-12 col-lg-3 chat__col h-100 ">
                            <header className="chat__header">
                                <h4>جهات الاتصال</h4>
                            </header>
                            <div className="chat__body"
                            >
                                {
                                    contacts.length > 0 ? <div
                                            style={ { ...tabsStyle } }
                                        >
                                            <StyledList> {
                                                contacts.map( i =>
                                                    <Contact
                                                        key={ i.id }
                                                        selectContact={ selectContact }
                                                        selected={ selectedContact }
                                                        message={ i }
                                                        messageFrom={ i.messageFrom }
                                                    /> )
                                            }
                                            </StyledList>
                                        </div>
                                        : <div className="font-weight-bold py-5 text-center"> لا توجد جهات اتصال</div>
                                }
                            </div>
                        </div>
                    }
                    { selectedContact ?
                        <div className="col col-lg-9 message__col">
                            <>
                                <header>
                                    <h4>{ selectedContact ? name : '' }</h4>
                                    { width < size.md && <Button color="primary" onClick={ ( () => selectContact( '' ) ) }>اعرض جهات الاتصال</Button> }
                                </header>
                                <div className="message__body">
                                    <MessagesView
                                        style={ tabsStyle }
                                        className="overflow-auto"
                                        selected={ selectedContact }
                                        messages={ messages }
                                    >
                                    </MessagesView>
                                </div>
                            </>
                        </div> : <div className="py-5 col d-none d-lg-block text-center"> لا توجد رسائل</div> }
                </div>
            </MessageWrapper>
        )
    }
}

export default connect( ( { user: { currentUser }, ui: { height, width } } ) => ( { height, currentUser, width } ) )( Messages );
