import { Avatar }                from "@material-ui/core";
import Button                    from "@material-ui/core/Button";
import Icon                      from "@material-ui/core/Icon";
import * as PropTypes            from "prop-types";
import React, { Component }      from 'react'
import { connect }               from "react-redux";
import { sendMessage }           from "src/store/actions/usersActions";
import { lightColor, lightMain } from "src/styles/colors";
import styled                    from 'styled-components';


const MessengerWrapper = styled.div`
position: relative;
height: 100%;
overflow: hidden;
.messenger__box-send{
height: 80px;
background-color: #fff;
position: absolute;
bottom:0;
left:0;
width: 100%;
display: flex;
textarea{
display: block;
width: 90%;
border:0;
resize:none;
}
.messenger__box-sender{
width: 10%;
min-width: 60px;
.messenger__box-send-icon{
transform: rotatez(180deg);

}

}
}
.messenger__box-messages{
height: calc(100% - 80px);
overflow: auto;
}

`

const MessageBox = styled.div`

blockquote{
background: ${ ( { me } ) => me ? lightMain : lightColor };
padding: 1rem 1rem .1rem;
margin-right: .5rem;
}
display: flex;
`
const MessageWrapper = styled.div`
display: flex;
align-items: center;
justify-content: ${ ( { me } ) => me ? 'flex-start' : 'flex-end' };
margin: .5rem;
`

class MessagesView extends Component {
    state = {
        messagesToSend: {},
        sending: {}
    }

    handleChange = ( target, { target: { value } } ) => {

        this.setState( {
            messagesToSend: {
                ...this.state.messagesToSend,
                [ target ]: value
            }
        } )
    }
    sendMessage = async () => {
        const selected = ( { ...this.props } ).selected;
        console.log( 'sending' );
        if ( this.state.sending[ selected ] || this.state.messagesToSend[ selected ] === "" ) return;
        const message = {
            to: selected,
            content: this.state.messagesToSend[ selected ]
        };
        try {
            this.setState( {
                sending: {
                    [ selected ]: true
                }
            } )

            const dispatch = await this.props.sendMessage( message )
            this.setState( {
                sending: {
                    [ selected ]: false
                },
                messagesToSend: {
                    [ selected ]: ""
                }
            } )
        } catch ( e ) {

        }

    }

    render () {
        let { selected, messages, currentUser, ...rest } = this.props;
        console.log( selected );
        const converstinMessages = messages.filter( ( { contactId } ) => contactId === selected ).reverse()
        return (
            <div
                { ...rest }
            >
                {
                    converstinMessages.length > 0 ?

                        <MessengerWrapper>
                            <div className=" messenger__box-messages">
                                { converstinMessages.map( ( { author, message, messageFrom, id }, index ) => {
                                        const me = author === +currentUser.id;
                                        return <MessageWrapper key={ id + 'conv' }
                                                               me={ me }
                                        >
                                            <cite title="Ahmed">
                                                { messageFrom ? <Avatar src={ messageFrom.avatar_urls[ '96' ] } alt={ messageFrom.name }/> :
                                                    <Icon fontSize="large" color="disabled">account_circle</Icon> }
                                            </cite>
                                            <MessageBox
                                                me={ me }
                                            >
                                                <blockquote
                                                    dangerouslySetInnerHTML={ { __html: message } }
                                                >
                                                </blockquote>
                                            </MessageBox>
                                        </MessageWrapper>;
                                    }
                                ) }
                            </div>
                            <div className="  messenger__box-send">
                                <textarea
                                    value={ this.state.messagesToSend[ selected ] || "" }
                                    onChange={ ( e ) => this.handleChange( selected, e ) }
                                />
                                <div className="d-flex align-items-center justify-content-center messenger__box-sender">
                                    <Button
                                        disabled={ this.state.sending[ selected ] === true }
                                        variant="fab" color="primary"><Icon className="messenger__box-send-icon"
                                                                            onClick={ this.sendMessage }
                                    >send</Icon></Button>
                                </div>
                            </div>
                        </MessengerWrapper> : <div className="py-5 text-center"> لا توجد رسائل</div>
                }
            </div>
        )
    }
}

MessagesView.propTypes = {
    selected: PropTypes.any,
    messages: PropTypes.any,
    currentUser: PropTypes.any
}
export default connect( ( { user: { currentUser } } ) => ( { currentUser } ), dispatch => ( {
    sendMessage: ( message ) => dispatch( sendMessage( message ) )
} ) )( MessagesView )
