import PropTypes                     from 'prop-types'
import { Icon, IconButton, Tooltip } from "@material-ui/core";
import React                         from 'react'
import { connect }                   from "react-redux";
import { Link }                      from "react-router-dom";
import { above, below, size }        from "shared/ui/sc-breakpoint";
import { DetailsButton }             from "src/Components/ui/OfferCard/OfferCard";
import ShareButton                   from "src/Components/ui/shareButton/ShareButton";
import { toggleFavorites }           from "src/store/actions/usersActions";
import { mainColor }                 from "src/styles/colors";
import styled                        from 'styled-components';

const Wrapper = styled.div`
display: flex;
direction: rtl;
flex-wrap: wrap;
`

const FlexItem = styled.div`

.prop_card_media{
width: 100%;
display: block;
}
${
    above.md`
    flex: ${ ( { flex } ) => flex }

    `
    };
${
    below.md`
    .prop_card_actions{
        width:50%
    }
    `
    }
`

const PropImage = styled.img`

${
    above.md`
   max-width:200px

    `
    };
${
    below.md`
        width: 100%;

    `
    };
`
const Price = styled.div`
color: ${ mainColor };
`
const PropertyCardProfile = ( { _id: id, imgSrc, title, address, price, toggleFavorites, width, inUserFavorites } ) => {
    return (
        <Wrapper>
            {
                imgSrc && <FlexItem flex="1" className="p-2 prop_card_media">
                    <PropImage className=" img-fluid " src={ imgSrc } alt=""/>
                </FlexItem>
            }
            <FlexItem flex="5 1" className="p-2 prop_card_actions  ">
                <div className="d-flex flex-column w-100 h-100">
                    <h4>{ title }</h4>
                    <h6> { address }</h6>
                    { price && <Price>{ price }</Price> }
                    <div className="mt-2 d-flex align-items-center">
                        <div><DetailsButton color="primary" component={ Link } to={ `/details/${ id }?type=offer` }>التفاصيل</DetailsButton>
                        </div>
                        { width < size.md && <FlexItem flex="1 1 100px" className="p-2 prop_card_actions mr-auto">
                            <div className="d-flex align-items-center justify-content-end w-100">
                                <ShareButton url={`/details/${ id }?type=offer`}/>
                                <Tooltip title={ inUserFavorites ? 'احذف من المفضله' : 'اضف الي المفضله' }
                                         children={ <IconButton color={ inUserFavorites ? 'secondary' : 'default' }
                                                                onClick={ () => toggleFavorites( id ) }><Icon>favorite</Icon></IconButton> }
                                />
                            </div>
                        </FlexItem>

                        }
                    </div>
                </div>
            </FlexItem>
            { width > size.md && <FlexItem flex="1 1 100px" className="p-2 prop_card_actions mr-auto">
                <div>
                    <ShareButton url={ `/details/${ id }?type=offer` }/>
                    <Tooltip title={ inUserFavorites ? 'احذف من المفضله' : 'اضف الي المفضله' }
                             children={ <IconButton color={ inUserFavorites ? 'secondary' : 'default' }
                                                    onClick={ () => toggleFavorites( id ) }><Icon>favorite</Icon></IconButton> }
                    />
                </div>
            </FlexItem>
            }
        </Wrapper>
    )
}
export default connect( ( { ui: { width } } ) => ( { width } ), dispatch => ( { toggleFavorites: ( id ) => dispatch( toggleFavorites( id ) ) } ) )( PropertyCardProfile )


PropertyCardProfile.propTypes = {
    address: PropTypes.any,
    id: PropTypes.any,
    imgSrc: PropTypes.any,
    price: PropTypes.any,
    title: PropTypes.any
}
