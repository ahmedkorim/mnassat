import React                      from 'react';
import { connect }                from "react-redux";
import formRender                 from "shared/Forms/formRender";
import withFormState              from "shared/Forms/withFormState";
import { FormSubmitter }          from "src/Components/shared/AboutUs/ContactWithUs";
import { SectionHeader }          from "src/Components/ui/SectionHeader/SectionHeader";
import requestAService            from "src/configs/Forms/requestAserviceFrom";
import { sendService }            from "src/store/actions/usersActions";
import { accentColor, greyColor } from "src/styles/colors";
import styled                     from 'styled-components';
import Button                     from '@material-ui/core/Button'

const Wrapper = styled.div`
 padding: 3rem 0;
 .typo_call-to-acٰtion{
 color:${ greyColor }
 }
`

const GoldenButton = styled( Button )`
&&{
background-color: ${ accentColor };
border-radius: .2rem;
padding: .7rem 4rem;
font-weight: bold;
}
`

class PropertyService extends React.Component {


    componentDidMount () {
        if ( this.props.getRef && this.node ) {
            this.props.getRef( this.node )
            console.log( this.node );
        }
    }

    sendService = async ( e ) => {
        e.preventDefault()
        const state = await this.props.validateNow();
        if ( state ) {

            this.props.sendService( this.props.value )
            this.props.stopEagerValidation()

        }
    }
    handleNextStep = async () => {
        console.log( 'nest step' );
        const state = await this.props.validateNow( true, 1 );
        console.log( state );
        if ( state ) {
            console.log( 'valid' );
            this.props.nextStep()
            this.props.stopEagerValidation()

        }
    }

    render () {
        const {
            props: { handleChange, value, prevStep, form, feedback, renderSteppers, step, eagerValidation }
        } = this

        return (
            <Wrapper ref={ ref => this.node = ref }>
                <SectionHeader>اتريد خدمة عقارية</SectionHeader>
                { step == 1 && <p className="text-center typo_call-to-action">
                    اختتار الخدمات العقارية التي تناسبك واسرع من عمليات الشراء والبيع والاتثمار
                </p> }
                <div className="container">
                    <div className="row justify-content-center">
                        <div className={ step === 1 ? "col-12" : "col col-lg-6" }>
                            <form noValidate className="row" onSubmit={ this.sendService }>
                                { formRender( { body: form, value: value }, handleChange, eagerValidation )
                                }
                                {
                                    step === 2 && <FormSubmitter variant="extended" color="primary" type="submit">ارسال طلبك</FormSubmitter>

                                }
                            </form>
                        </div>
                    </div>
                    <br/>
                    <br/>
                    <div className="text-center">
                        <GoldenButton
                            onClick={ step === 1 ? this.handleNextStep : prevStep }>{ step === 1 ? 'الخطوه التاليه' : 'الخطوه السابقه' }</GoldenButton>
                    </div>
                </div>
            </Wrapper>
        )
    }
}

export default withFormState( requestAService.formState, requestAService.form )( connect( null, { sendService } )( PropertyService ) );
