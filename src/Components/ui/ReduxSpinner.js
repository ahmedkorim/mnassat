import CircularProgress from "@material-ui/core/CircularProgress/CircularProgress";
import React            from 'react'
import { connect }      from "react-redux";
import styled           from 'styled-components';

const SpinnerWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  position: fixed;
  z-index: 999;
  top: 0;
  left: 0;
  opacity: 1;
  transition: all ease-in-out 1.5s;
  background-color:rgba(0,0,0,.1);
  &.hide{
    opacity: 0;
  }


.spinner {
    width: 60px;
    height: 60px;
    margin: 100px auto;
    background-color: #29B6F6;

    border-radius: 100%;
    -webkit-animation: sk-scaleout 1.0s infinite ease-in-out;
    animation: sk-scaleout 1.0s infinite ease-in-out;
}

@-webkit-keyframes sk-scaleout {
    0% { -webkit-transform: scale(0) }
    100% {
        -webkit-transform: scale(1.0);
        opacity: 0;
    }
}

@keyframes sk-scaleout {
    0% {
        -webkit-transform: scale(0);
        transform: scale(0);
    } 100% {
          -webkit-transform: scale(1.0);
          transform: scale(1.0);
          opacity: 0;
      }
}

`

const ReduxSpinner = ( { spinner } ) => spinner && <SpinnerWrapper className="spinner-wrapper">
    <CircularProgress size={ 100 } thickness={ 3 }/>
</SpinnerWrapper>


export default connect( ( { ui: { spinner } } ) => ( {
    spinner: spinner
} ) )( ReduxSpinner )
