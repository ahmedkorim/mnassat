import React           from 'react'
import { accentColor } from "src/styles/colors";
import styled          from 'styled-components';

export const SectionHeader = styled.h3`
margin-bottom: 2rem;
width: 100%;
  position: relative;
  text-align: center;
  color:${ ( { variant } ) => variant === 'light' ? '#fff' : 'initial' };
  &:after{
  position: absolute;
  bottom:-.8rem;
  display: block;
  content: '';
  width:4rem;
  height:3px;
  border-radius: 5px;
  background-color: ${ accentColor };
  left: 0;
  right: 0;
  margin-left: auto;
  margin-right: auto;
  }
`
