import Typography          from '@material-ui/core/Typography'
import React               from 'react'
import handleInputState    from "shared/Forms/handleInputState";
import DropDown            from "src/Components/ui/DropDown/DropDown";
import { LightTypography } from "src/Components/ui/Filters/requestsFilters/RequestsFitlers";
import { mainColor }       from "src/styles/colors";
import styled              from 'styled-components';

const Feedback = styled( Typography )`
&&{
font-size:.75rem;
text-align: right;
}

`
const Wrapper = styled.div`
margin-bottom: .3rem  ;
margin-top: .3rem  ;
.select__drop_wrapper{
border: 1px solid ${ ( { feedback } ) => feedback || mainColor };
border-radius: .4rem;
}
${ Feedback }{
color:${ ( { feedback } ) => feedback || mainColor } !important;
}
`
const SelectFromInput = ( { value = '', allValues, name, label, placeholder = null, optionValueKey = 'value', options, helperText = null, feedback = {}, filterValues, changeHandler = void 0, eagerValidation, extras = {}, _selectValue, ...rest } ) => {

    options = ( options || extras[ name ] || [] ).filter( i => ( i.id + '' ).indexOf( 'default' ) === -1 );
    options = filterValues ?  filterValues( options, allValues ) :options
    const handler = ( option ) => {
        const selected = options.find( ( { name } ) => name === option );
        changeHandler( {
            target: {
                value: selected[ _selectValue || optionValueKey ]
            }
        } )

    }
    const { feedbackColor, feed } = handleInputState(
        {
            feedback,
            eagerValidation, value, requried: rest.required
        } )
    const selected = options.find( opt => opt[ _selectValue || optionValueKey ] === value )

    console.log( options );
    return ( <Wrapper feedback={ feedbackColor }>
            <div className="select__drop_wrapper">
                <DropDown
                    options={ options }
                    style={ { flexGrow: 1 } }
                    hiddenBadge
                    selected={ selected ? selected.name : label }
                    handleChange={ handler }
                >
                    <LightTypography>
                        { selected ? selected.name : label }
                    </LightTypography>
                </DropDown>
            </div>
            <Feedback>{ feed }</Feedback>
        </Wrapper>
    )
}
export default SelectFromInput
