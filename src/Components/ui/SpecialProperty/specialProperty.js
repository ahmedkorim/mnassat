import Icon            from "@material-ui/core/Icon";
import shadows         from "@material-ui/core/styles/shadows";
import React           from 'react'
import { accentColor } from "src/styles/colors";
import styled          from 'styled-components';

const Wrapper = styled.div`
background-color: ${ accentColor };
padding: .3rem .8rem;
border-radius:.2rem ;
right: 1rem;
top: 1rem;
font-weight: bold;
position: absolute;
z-index: 9;
display: flex;
justify-content: center;
align-items: center;
box-shadow: ${ shadows[ 1 ] };
`

const SpecialProperty = props => {
    return (
        <Wrapper>
            <Icon>star</Icon> عقار مميز
        </Wrapper>
    )
}
export default SpecialProperty
