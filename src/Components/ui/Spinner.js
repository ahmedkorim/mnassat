import { CircularProgress } from "@material-ui/core";
import React                from 'react'
import styled, { css }      from 'styled-components';

const SpinnerBackdrop = styled.div`
${ ( { fixed, variant } ) => fixed ? css`

position: relative;
height: 30vh;
width: 100%;

`

    : css`
position: absolute;
height: 100%;
width: 100%;
${ variant !== 'light' && `background-color: rgba(0,0,0,.3)` }
` };

z-index: 999;

top: 0;
left: 0;
display: flex;
justify-content: center;
align-items: center;
`

const Spinner = ( { fixed, variant , progressProps = {} } ) => {
    return (
        <SpinnerBackdrop fixed={ fixed } variant={ variant }>
            <CircularProgress
                size={ 100 }
                thickness={ 2 }
                { ...progressProps }
            />
        </SpinnerBackdrop>
    )
}
export default Spinner
