import { CardMedia, List, ListItem } from "@material-ui/core";
import Card                          from "@material-ui/core/Card";
import CardContent                   from "@material-ui/core/CardContent";
import Icon                          from "@material-ui/core/Icon";
import Tooltip                       from "@material-ui/core/Tooltip";
import React                         from 'react'
import { OfferCardWrapper }          from "src/Components/ui/OfferCard/OfferCard";
import Spinner                       from "src/Components/ui/Spinner";
import { greyColor, mainColor }      from "src/styles/colors";
import styled                        from 'styled-components';
import whatsapp                      from 'assets/Images/whatsapp-16.png'

export const StyledTeamMemberCardContent = styled( CardContent )`
text-align: right;

.list-unstyled{
padding: 0 !important;
margin: 0!important;
color: ${ greyColor };
}
padding: .5rem 1rem !important ; 
`
const TMname = styled.h6`
color: ${ mainColor };
font-weight: bold;
font-size:1.2rem;
`

export const IconWrapper = styled.div`
margin-left: .4rem;
display: flex;
align-items: center;
`
const ContactListItem = styled( ListItem )`
padding: .2rem 0 !important;
margin-bottom: .1rem !important;
&:hover{
color: initial !important;
}
`
const TeamMember = ( {
                         data,
                         media
                     } ) => {
    const {
        first_name,
        last_name,
        agent_position,
        agent_email,
        agent_phone,
        agent_mobile,
        agent_skype,
        featured_media,
        id,
    } = data
    // console.log( data );
    const { url = "", alt_text = "" } = media.find( ( image ) => image.id === featured_media ) || {}
    return (
        <OfferCardWrapper
        >
            { url ? <CardMedia
                    image={ url }
                    title={ alt_text }
                    className="card__img-team-member"
                /> :
                <div
                    className="card__img-team-member position-relative"
                >
                    <Spinner variant="light" progressProps={ {
                        size: 40,
                        thickness: 2
                    } }/>
                </div>

            }
            <StyledTeamMemberCardContent>
                <TMname>{ `${ first_name } ${ last_name }` }</TMname>
                <h6>{ agent_position }</h6>
                <div>
                    <List className="list-unstyled">
                        <Tooltip
                            title="هاتف"
                            children={ <ContactListItem button component="a" target="_blank" href={ `tel:${ agent_phone }` }>
                                <div className="d-flex align-items-center">
                                    <IconWrapper><Icon fontSize="small">phone</Icon></IconWrapper>
                                    <div>{ agent_phone }</div>
                                </div>
                            </ContactListItem>
                            }/>
                        <Tooltip
                            title="جوال"
                            children={ <ContactListItem button component="a" target="_blank" href={ `tel:${ agent_mobile }` }>
                                <div className="d-flex align-items-center">
                                    <IconWrapper><Icon fontSize="small">stay_current_portrait</Icon></IconWrapper>
                                    <div>{ agent_mobile }</div>
                                </div>
                            </ContactListItem> }/>
                        <Tooltip
                            title="واتساب"
                            children={ <ContactListItem button>
                                <div className="d-flex align-items-center">
                                    <IconWrapper> <img src={whatsapp} alt=""/></IconWrapper>
                                    <div>{ agent_skype }</div>
                                </div>
                            </ContactListItem> }
                        />
                        <Tooltip
                            title="ميل"
                            children={ <ContactListItem button component="a" target="_blank" href={ `mailto:${ agent_email }` }>
                                <div className="d-flex align-items-center">
                                    <IconWrapper><Icon fontSize="small">email</Icon></IconWrapper>
                                    <div>{ agent_email }</div>
                                </div>
                            </ContactListItem> }/>
                    </List>
                </div>
            </StyledTeamMemberCardContent>
        </OfferCardWrapper>
    )
}
export default TeamMember
