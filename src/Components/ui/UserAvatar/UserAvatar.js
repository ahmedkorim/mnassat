import { Avatar }          from "@material-ui/core";
import Icon                from "@material-ui/core/Icon";
import IconButton          from "@material-ui/core/IconButton";
import shadows             from "@material-ui/core/styles/shadows";
import React, { useState } from 'react'
import { connect }         from "react-redux";
import { withRouter }      from "react-router";
import { above }           from "shared/ui/sc-breakpoint";
import DropDown            from "src/Components/ui/DropDown/DropDown";
import { logout }          from "src/store/actions/usersActions";
import styled              from 'styled-components';
import { Link }            from 'react-router-dom';
import source              from 'assets/Images/ahmed-ajame.png'

const Wrapper = styled( Link )`
${
    above.md`
    margin: 0 2rem;

    `
    };
text-decoration: none!important;
`

const optionsWithAuth = [

    {
        id: "user-offers",
        name: 'عروضي',
        to: '/profile/offers'
    }, {
        id: "user-orders",
        name: 'طلباتي',
        to: '/profile/orders'
    }, {
        id: "user-favourites",
        name: 'مفضله',
        to: '/profile/favourites',

    }, {
        id: "user-messages",
        name: 'رسايل',
        to: '/profile/messages'
    }, {
        id: "user-invoices",
        name: 'فواتير',
        to: '/profile/invoices'
    }, {
        id: 'log-out',
        name: 'تسجيل الخروج',
        action: 'logout'
    },
]
const withoutAuthOption = [ {
    id: "user-login",
    name: 'تسجيل الدخول',
    to: '/auth'
}, {
    id: "user-register",
    name: 'انشاء حساب',
    to: '/auth/register'
} ]


const UserAvatar = ( { user: { auth, name, email, username, avatar }, history: { push }, logout, headerHeight } ) => {
    const options = auth ? optionsWithAuth : withoutAuthOption
    const [ activePage, setAtivePage ] = useState( '' );
    const handler = ( option ) => {
        setAtivePage( option )
        const selected = options.find( ( { name } ) => name === option )
        if ( selected ) {
            if ( selected.action ) {
                selected.action === 'logout' && logout()
            } else {
                push( selected.to )

            }
        }
    }
    return (
        <div>
            <DropDown
                handleChange={ handler }
                options={ options }
                selected={ activePage }
                pWidth={ 200 }
                fullHeight
                paperStyles={ {
                    boxShadow: shadows[ 1 ],
                    borderRadius: '.2rem',
                    marginTop: headerHeight -10
                } }
                renderTrigger={ ( handler, close ) => <Wrapper
                    onClick={ ( e ) => handler( e ) }
                    // onMouseLeave={ ( e ) => close( e ) }
                >
                    { ( auth && avatar ) ? <Avatar src={ avatar } alt={ username }/> : <IconButton><Icon fontSize="large">account_circle</Icon></IconButton> }
                </Wrapper>
                }
            />
        </div>
    )
}
export default withRouter( connect( ( { ui: { headerHeight } } ) => ( { headerHeight } ), dispatch => ( {
    logout: () => dispatch( logout() )
} ) )( UserAvatar ) )
