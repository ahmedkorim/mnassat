import PropTypes                   from 'prop-types'
import React                       from 'react'
import { accentColor, lightColor } from "src/styles/colors";
import styled                      from "styled-components";
import { Link }                    from "react-router-dom";

export const StyledLink = styled( Link )`
margin-bottom: .5rem;
display: inline-block;
text-decoration: none !important;
color: ${ lightColor };
transition:all .35s ease-in-out;
&:hover{
color: ${ accentColor };
}
`
const Wrapper = styled.div`
text-align: right;
flex-wrap: wrap;
.header__vertical-list{
margin-bottom: 1rem;
color: ${ lightColor };
font-size: 1.1rem;
}
`
const VerticalLinksList = ( { links, title } ) => {
    return (
        <Wrapper>
            <h4 className="header__vertical-list">{ title }</h4>
            <ul className="list-unstyled m-0 p-0">
                { links.map( ( { title, path, ...rest } ) => <li
                    key={ path + title }
                >
                    <StyledLink
                        to={ path }
                        { ...rest }
                    >{ title }</StyledLink>
                </li> ) }
            </ul>
        </Wrapper>
    )
}
export default VerticalLinksList

VerticalLinksList.propTypes = {
    links: PropTypes.array.isRequired,
    title: PropTypes.any
}
