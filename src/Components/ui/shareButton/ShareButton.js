import { Tooltip }  from "@material-ui/core";
import Icon         from "@material-ui/core/Icon";
import IconButton   from "@material-ui/core/IconButton";
import React        from 'react';
import DropDown     from "src/Components/ui/DropDown/DropDown";
import { homePage } from "src/configs/urls";
import styled       from 'styled-components';
import fb           from 'assets/Images/facebook.png'
import tw           from 'assets/Images/twiiter.png'
import whatsapp     from 'assets/Images/icons8-whatsapp-48.png'

const StyledImage = styled.img`
display: block;
max-width: 80%;
`
const shareMenu = [ {
    id: "facebook-share",
    name: 'FB',
    action: 'facebook-share',
    component: () => <StyledImage src={ fb } className="img-fluid" alt="facebook"/>,

}, {
    id: "twitter-share",
    name: 'twitter',
    component: () => <StyledImage src={ tw } className="img-fluid" alt="twitter"/>,
    action: 'twitter-share'
}, {
    id: "whatsapp-share",
    name: 'whatsapp',
    component: () => <StyledImage src={ whatsapp } className="img-fluid" alt="whatsapp"/>,
    action: 'whatsapp-share'
} ]
const Wrapper = styled.div`
display: inline-block;
`


const shareToFaceBook = ( url ) => {
    if ( typeof window !== void 0 + '' ) {
        const facebookWindow = window.open( `https://www.facebook.com/sharer/sharer.php?u=${ homePage + url }`, 'facebook-popup', 'height=350,width=600' )
        if ( facebookWindow.focus ) {
            facebookWindow.focus();
        }
        return false;
    }
}


const shareToTwitter = ( url ) => {
    if ( typeof window !== void 0 + '' ) {

        const twitterWindow = window.open( `https://twitter.com/share?url=${ homePage + url }`, 'twitter-popup', 'height=350,width=600' );
        if ( twitterWindow.focus ) {
            twitterWindow.focus();
        }
        return false
        console.log( 'twitter share' );
    }
}

const shareToWatsapp = ( url ) => {
    window.open( 'https://web.whatsapp.com/send?text=' + homePage + url, "_blank" )
}


const ShareButton = ( { url } ) => {


    const handler = ( option ) => {
        const selected = shareMenu.find( ( { name } ) => name === option )
        if ( selected ) {
            if ( selected.action ) {
                switch ( selected.action ) {
                    case 'facebook-share':
                        return shareToFaceBook( url );
                    case 'twitter-share':
                        return shareToTwitter( url );
                    case 'whatsapp-share':
                        return shareToWatsapp( url )
                }
            } else {
                push( selected.to )

            }
        }
    }

    return (
        <Wrapper>
            <DropDown
                fullHeight
                options={ shareMenu }
                pWidth={ 60 }
                handleChange={ handler }
                paperStyles={ {
                    margin: `10px 0 0 60px`,
                    borderRadius: '6px'

                } }
                renderTrigger={ ( handler, close ) => <Tooltip title="شارك" children={ <IconButton
                    onClick={ ( e ) => handler( e ) }
                ><Icon>share</Icon></IconButton> }/>

                }
            />
        </Wrapper>
    )
}

export default ShareButton;
