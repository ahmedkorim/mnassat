import { Divider } from "@material-ui/core";
import source      from 'assets/Images/khaled_shaker.png'
import React       from 'react';
import { connect } from "react-redux";

import AboutUsHero                                     from "src/Components/shared/AboutUs/AboutUsHero";
import ContactWithUs                                   from "src/Components/shared/AboutUs/ContactWithUs";
import TeamMemberLoader                                from "src/Components/ui/Loaders/TeamMemberLoader";
import { SectionHeader }                               from "src/Components/ui/SectionHeader/SectionHeader";
import TeamMember                                      from "src/Components/ui/TeamMember/TeamMember";
import { getAllTeamMembers }                           from "src/store/actions/usersActions";
import { darkColor, lightColor, lightGrey, mainColor } from "src/styles/colors";
import styled                                          from "styled-components";

const Wrapper = styled.div`
background-color: ${ ( { bgColor } ) => bgColor || lightColor };
color:${ ( { color } ) => color || darkColor };
padding: 4rem 0;
`
export const MainDivider = styled( Divider )`
background-color: ${ mainColor } !important;
height: 2px !important;
margin: .5rem 0 .75rem !important;
`

class AboutUs extends React.Component {

    constructor ( props ) {
        super( props );
    }

    componentDidMount () {
        this.props.getTeamMember();
    }

    render () {
        const { props: { teamMembers, media } } = this;
        return (
            <>
                <div style={ { backgroundColor: lightColor } }>
                    <AboutUsHero/>
                    {/* company message*/ }
                    <section className="container">
                        <Wrapper as="article" className="row align-items-start">
                            <div className="col-lg-6 col-12 ">
                                <figure>
                                    <img className="d-block m-auto" src={ source } alt="khaled shaker"/>
                                    <figcaption className="text-center">أ/خالد شاكر <br/> رئيس مجلس الادارة</figcaption>
                                </figure>
                            </div>
                            <div className="col-lg-6 col-12 pt-5">
                                <h3 className="text-right">رساله الشركة </h3>
                                <MainDivider/>
                                <p className="Lead text-right">
                                    لوريم ايبسوم هو نموذج افتراضي يوضع في التصاميم لتعرض على العميل ليتصور طريقه وضع النصوص بالتصاميم سواء كانت تصاميم
                                    مطبوعه
                                    ...
                                    بروشور
                                    او فلاير على سبيل المثال ... او نماذج مواقع انترنت ...
                                    وعند موافقه العميل المبدئيه على التصميم يتم ازالة هذا النص من التصميم ويتم وضع النصوص النهائية المطلوبة للتصميم ويقول
                                    البعض
                                    ان
                                    وضع
                                    النصوص التجريبية بالتصميم قد تشغل المشاهد عن وضع الكثير من الملاحظات او الانتقادات للتصميم الاساسي.
                                    وخلافاَ للاعتقاد السائد فإن لوريم إيبسوم ليس نصاَ عشوائياً، بل إن له جذور في الأدب اللاتيني الكلاسيكي منذ العام 45 قبل
                                    الميلاد.
                                    من
                                    كتاب "حول أقاصي الخير والشر"
                                </p>
                            </div>
                        </Wrapper>
                    </section>
                </div>
                <section style={ { backgroundColor: lightGrey } }>
                    <div className="container">
                        <Wrapper
                            bgColor={ lightGrey }
                            className="d-flex flex-wrap"
                        >
                            <SectionHeader> فريق العمل</SectionHeader>
                            {
                                teamMembers.length > 0 ? teamMembers.map( teamMember => <TeamMember key={ teamMember.id } media={ media }
                                                                                                    data={ teamMember }/> ) :
                                    [ 1, 2, 3, 4, 5, 6, 7, 8 ].map( ( i, index ) => <TeamMemberLoader
                                        key={ `teamMember${ i }loader` }/> )
                            }
                        </Wrapper>
                    </div>
                </section>
                <ContactWithUs/>
            </>
        )
    }
}

const mapStateToProps = ( { user, miscellaneous } ) => ( {
    teamMembers: user.teamMembers,
    media: miscellaneous.media
} )

const mapDispatchToProps = dispatch => ( {
        getTeamMember: _ => dispatch( getAllTeamMembers() )
    }
)


export default {
    component: connect( mapStateToProps, mapDispatchToProps )( AboutUs ),
    loadData: ( { dispatch } ) => dispatch( getAllTeamMembers() )
};
