import Button               from "@material-ui/core/Button";
import shadows              from "@material-ui/core/styles/shadows";
import darkClip             from 'assets/Images/darkClip.png'
import bgArt                from 'assets/Images/requestArt.png'
import React                from 'react';
import { connect }          from "react-redux";
import formRender           from "shared/Forms/formRender";
import withFromState        from "shared/Forms/withFormState";
import { FormSubmitter }    from "src/Components/shared/AboutUs/ContactWithUs";
import { SectionHeader }    from "src/Components/ui/SectionHeader/SectionHeader";
import addOfferFrom         from "src/configs/Forms/addOfferFrom";
import { getOffersFilters } from "src/store/actions/offersActions";
import { addOffer }         from "src/store/actions/requestsActions";
import styled               from 'styled-components';

const Wrapper = styled.div`
position: relative;
z-index: 999;
max-width: 1200px;
margin: 2rem auto;
overflow: hidden;
border-radius: 1.5rem;
box-shadow:${ shadows[ 4 ] };
background-color: #fff;
.background__art{
background: url(${ bgArt }) no-repeat;
background-size:cover;
 position: relative;
 &:after{
 content:'';
 display: block;
 width: 100%;
 height: 100%;
 position: absolute;
 background-color: rgba(5,17,22,.8);
 left: 0;
 top: 0;
 }
}
.header__section-request{
position: relative;
z-index: 11;
}
`
const BackgroundWrapper = styled.section`
height: 100vh;
width: 100vw;
position: fixed;
top: 0;
left: 0;
z-index: -1;
.background__wrapper-clip{
width: 100%;
overflow: hidden;
img{
 position:relative ;
 top: -8rem;
width: 100%;
}
}
`


class AddOffer extends React.Component {

    componentDidMount () {
        this.props.loadData()
    }

    nextStep = async ( step ) => {
        const state = await this.props.validateNow( true, step );


        if ( state ) {
            this.props.nextStep()
            this.props.stopEagerValidation()

        }

    }
    addOffer = async () => {
        const state = await this.props.validateNow();
        if ( state ) {
            const value = {
                ...this.props.value,
                sh_services: this.props.value.sh_services.split( ',' ).filter( i => !!i ).reduce( ( acc, serviceId ) => {
                    const index = this.props.services.findIndex( service => +service.id === +serviceId ) + 1
                    return acc ? `${ acc },${ index }` : index + ''
                }, '' )
            }
            this.props.addOffer( value, this.props.history )


        }

    }


    getCallToAction = ( step, nextStep, prevStep ) => {
        switch ( step ) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                return <div key={ step } className="w-100">
                    <div className="col-12 text-center py-2">
                        <FormSubmitter variant="extended" color="primary" type="button" onClick={ () => this.nextStep( step ) }>الخطوه التاليه</FormSubmitter>
                        {
                            step > 1 && <div className="w-100 text-right">
                                <Button variant="text" color="primary" type="button" onClick={ prevStep }>الخطوه السابقه</Button>
                            </div>
                        }
                    </div>
                </div>
            case 6:
                return <>
                    <div className="col-12 text-center py-2">
                        <FormSubmitter variant="extended" color="primary" type="button" onClick={ this.addOffer }>اضافه عرضك </FormSubmitter>
                    </div>
                    <div>
                        <Button variant="text" color="primary" type="button" onClick={ prevStep }>الخطوه السابقه</Button>
                    </div>
                </>

        }
    }

    render () {
        const { offerFilters, handleChange, step, value, lastStep, nextStep, prevStep, form, login, feedback, validateNow, eagerValidation, history } = this.props;
        const {
            getCallToAction

        } = this;
        return (
            <>
                <form noValidate className="row px-4 py-5">
                    {
                        formRender( { body: form, value: value }, handleChange, eagerValidation, {
                            ...offerFilters
                        } )
                    }
                    { getCallToAction( step, nextStep, prevStep ) }
                </form>
            </>
        )
    }
}

const StyledWrapper = ( Component ) => ( props ) => (
    <>
        <BackgroundWrapper>
            <div className="background__wrapper-clip">
                <img src={ darkClip } alt=""/>
            </div>
        </BackgroundWrapper>
        <Wrapper>
            <div className="row">
                <div className="col-lg-7 ">
                    <Component { ...props }/>
                </div>
                <div className="col-lg-5 background__art d-flex align-items-center justify-content-center">
                    <SectionHeader variant="light" className="header__section-request">اضف عرضك</SectionHeader>
                </div>
            </div>
        </Wrapper>
    </>
)
const loadData = ( { dispatch } ) => {
    dispatch( getOffersFilters() )
}

const mapStateToProps = ( { offers: { offerFilters }, miscellaneous: { services } } ) => ( {
    offerFilters,
    services
} )

const mapDispatchToProps = dispatch => ( {
    loadData: () => loadData( { dispatch } ),
    addOffer: ( val, history ) => dispatch( addOffer( val, history ) )
} )

export default {
    component: connect( mapStateToProps, mapDispatchToProps )(
        withFromState( addOfferFrom.formState, addOfferFrom.form )( AddOffer )
    ),
    loadData,
    StyledWrapper
};
