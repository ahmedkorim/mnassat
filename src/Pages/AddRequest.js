import Button                             from "@material-ui/core/Button";
import shadows                            from "@material-ui/core/styles/shadows";
import React                              from 'react';
import { connect }                        from "react-redux";
import formRender                         from "shared/Forms/formRender";
import withFromState                      from "shared/Forms/withFormState";
import { FormSubmitter }                  from "src/Components/shared/AboutUs/ContactWithUs";
import { SectionHeader }                  from "src/Components/ui/SectionHeader/SectionHeader";
import addRequestFrom                     from "src/configs/Forms/addRequestFrom";
import Requests                           from "src/Pages/Requests";
import { addRequest, getRequestsFitlers } from "src/store/actions/requestsActions";
import styled                             from 'styled-components';
import darkClip                           from 'assets/Images/darkClip.png'
import bgArt                              from 'assets/Images/requestArt.png'

const Wrapper = styled.div`
max-width: 1200px;
margin: 2rem auto;
overflow: hidden;
border-radius: 1.5rem;
box-shadow:${ shadows[ 4 ] };
background-color: #fff;
.background__art{
background: url(${ bgArt }) no-repeat;
background-size:cover;
 position: relative;
 &:after{
 content:'';
 display: block;
 width: 100%;
 height: 100%;
 position: absolute;
 background-color: rgba(5,17,22,.8);
 left: 0;
 top: 0;
 }
}
.header__section-request{
position: relative;
z-index: 11;
}
`
const BackgroundWrapper = styled.section`
height: 100vh;
width: 100vw;
position: fixed;
top: 0;
left: 0;
z-index: -1;
.background__wrapper-clip{
width: 100%;
overflow: hidden;
img{
 position:relative ;
 top: -8rem;
width: 100%;
}
}
`


class AddRequest extends React.Component {

    componentDidMount () {
        this.props.loadData()
    }

    nextStep = async () => {
        const state = await this.props.validateNow( true, 1 );


        if ( state ) {
            this.props.nextStep()
            this.props.stopEagerValidation()

        }

    }
    addRequest = async () => {
        const state = await this.props.validateNow();
        if ( state ) {
            this.props.addRequest( this.props.value )

        }

    }


    getCallToAction = ( step, nextStep, prevStep ) => {
        switch ( step ) {
            case 1:
                return <div className="col-12 text-center py-2">
                    <FormSubmitter variant="extended" color="primary" type="button" onClick={ this.nextStep }>الخطوه التاليه</FormSubmitter></div>
            case 2:
                return <>
                    <div className="col-12 text-center py-2">
                        <FormSubmitter variant="extended" color="primary" type="button" onClick={ this.addRequest }>اضافه الطلب</FormSubmitter>
                    </div>
                    <div>
                        <Button variant="text" color="primary" type="button" onClick={ prevStep }>الخطوه السابقه</Button>
                    </div>
                </>

        }
    }

    render () {
        const { requestFilters, handleChange, step, value, lastStep, nextStep, prevStep, form, login, feedback, validateNow, eagerValidation } = this.props;
        const {
            getCallToAction

        } = this;
        return (
            <>
                <form noValidate className="row px-4 py-5">
                    {
                        formRender( { body: form, value: value }, handleChange, eagerValidation, {
                            ...requestFilters
                        } )
                    }
                    { getCallToAction( step, nextStep, prevStep ) }
                </form>
            </>
        )
    }
}

const StyledWrapper = ( Component ) => ( props ) => (
    <>
        <BackgroundWrapper>
            <div className="background__wrapper-clip">
                <img src={ darkClip } alt=""/>
            </div>
        </BackgroundWrapper>
        <Wrapper>
            <div className="row">
                <div className="col-lg-7 ">
                    <Component { ...props }/>
                </div>
                <div className="col-lg-5 background__art d-flex align-items-center justify-content-center">
                    <SectionHeader variant="light" className="header__section-request">اضف طلبك</SectionHeader>
                </div>
            </div>
        </Wrapper>
    </>
)
const loadData = ( { dispatch } ) => {
    dispatch( getRequestsFitlers() )
}

const mapStateToProps = ( { requests: { requestFilters } } ) => ( {
    requestFilters,
} )

const mapDispatchToProps = dispatch => ( {
    loadData: () => loadData( { dispatch } ),
    addRequest: ( val ) => dispatch( addRequest( val ) )
} )

export default {
    component: connect( mapStateToProps, mapDispatchToProps )(
        withFromState( addRequestFrom.formState, addRequestFrom.form )( AddRequest )
    ),
    loadData,
    StyledWrapper
};
