import shadows                       from "@material-ui/core/styles/shadows";
import React                         from 'react';
import { matchRoutes, renderRoutes } from "react-router-config";
import { SectionHeader }             from "src/Components/ui/SectionHeader/SectionHeader";
import styled                        from 'styled-components';

const Wrapper = styled.div`
box-shadow:${ shadows[ 2 ] };
padding: 2rem  1rem;
border-radius:.25rem;
`
const Container = styled.section`
min-height:80vh;
`

class Auth extends React.Component {
    render () {
        const matchedRoute = ( matchRoutes( this.props.route.routes, this.props.location.pathname ) || [] )[ 0 ]
        const name = matchedRoute ? matchedRoute.route.name : null

        return (
            <div className="container">
                <Container className="row justify-content-center align-items-center">
                    <div className="col-lg-8 col-xl-6 col-12">
                        <Wrapper>
                            { name && <SectionHeader> { name }</SectionHeader>
                            }
                            <div>
                                { renderRoutes( this.props.route.routes ) }
                            </div>
                        </Wrapper>
                    </div>
                </Container>
            </div>
        )
    }
}

export default Auth;
