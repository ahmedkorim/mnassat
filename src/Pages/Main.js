import { Button, Icon }                     from "@material-ui/core";
import Typography                           from "@material-ui/core/Typography";
import heroBackground                       from 'assets/Images/heroBackground.png'
import React                                from 'react';
import { connect }                          from "react-redux";
import { withRouter }                       from "react-router";
import { Link }                             from 'react-router-dom';
import { above }                            from "shared/ui/sc-breakpoint";
import { serialize }                        from "shared/URLhelpers";
import withPagination                       from "src/Components/HOC/WithPagination/WithPagination";
import AboutUsHero                          from "src/Components/shared/AboutUs/AboutUsHero";
import Clients                              from "src/Components/ui/Clients";
import DropDown                             from "src/Components/ui/DropDown/DropDown";
import RequestsFilter, { LightTypography }  from "src/Components/ui/Filters/requestsFilters/RequestsFitlers";
import PropertyService                      from "src/Components/ui/PropertyServies/PropertyService";
import { SectionHeader }                    from "src/Components/ui/SectionHeader/SectionHeader";
import { OFFER_FILTERS }                    from "src/configs/requestsFilters";
import Offers                               from "src/Pages/Offers";
import Requests                             from "src/Pages/Requests";
import { getOffersFilters }                 from "src/store/actions/offersActions";
import { darkColor, lightColor, lightGrey } from "src/styles/colors";
import styled, { css }                      from 'styled-components';
import { smoothScroll }                     from "../../helpers/smoothScroll";

export const StyledSectionCallToAction = styled( Button )`
&&{
padding:.5rem 1.3rem;
box-sizing: border-box;
border-radius: 0;
min-height: 45px;
min-width:170px;
margin: 1rem .1rem;
${ ( { black, variant } ) => {
    return black ? variant === 'contained' ? css`
    background-color: ${ darkColor };
    color:${ lightColor }  
  &:hover{
    background-color: ${ darkColor };
  color:${ lightColor }  

  }
` : css`
    background-color: transparent !important;
    color:${ darkColor } 
` : ''
}
    }

`

const HeroSection = styled.header`
background-image: url(${ heroBackground });
.content__wrapper-main-hero{
height: 100vh;
${
    above.md`
    height: 50vh;
    `
    }

}

`


class Main extends React.Component {
    state = {
        filter: {
            property_category: "كل العروض",
            property_action_category: "كل الاغراض",
            property_county_state: "كل البلاد",
            property_city: "كل المدن",
            property_area: "كل المناطق",
        }, variant: "العروض"
    }


    filterChangeHandler = ( target ) => ( value ) => {
        this.setState( {
            filter: {
                ...this.state.filter,
                [ target ]: value
            }
        } )
    }


    componentDidMount () {
        const section = this.props.match.params[ 'section' ];
        if ( section === 'services' && this.ps ) {
            smoothScroll( this.ps, this.props.headerHeight || 50 )
        }
        this.props.getOffersFilters()
    }

    handleVariant = ( e ) => {
        if ( e === 'الطلبات' ) {
            this.props.history.push( '/requests' )
        }
    }

    render () {

        const {
            state: {
                filter
            },
            filterChangeHandler,
            props: {
                clients,
                offerFilters
            },
            handleVariant
        } = this;
        const callToActionLink = serialize( this.state.filter )

        const renderFilters = {
            property_category: offerFilters[ 'property_category' ],
            property_action_category: offerFilters[ 'property_action_category' ],
            property_county_state: offerFilters[ 'property_county_state' ],
            property_city: offerFilters[ 'property_city' ].filter( i => {
                if ( ( i.id + '' ).indexOf( 'default' ) !== -1 ) return 1
                if ( this.state.filter[ 'property_county_state' ] === 'كل البلاد' ) return 1;
                return i.taxonomy_term_meta.country.name === this.state.filter[ 'property_county_state' ]

            } ),
            property_area: offerFilters[ 'property_area' ].filter( i => {
                if ( ( i.id + '' ).indexOf( 'default' ) !== -1 ) return 1
                if ( this.state.filter[ 'property_city' ] === 'كل المدن' ) return 1;
                return i.taxonomy_term_meta.city.name === this.state.filter[ 'property_city' ]

            } ),
        }
        return (
            <>
                <HeroSection>
                    <div className="container">
                        <div className="row justify-content-center align-items-center">
                            <div className="d-flex col-lg-10 col justify-content-center align-items-center content__wrapper-main-hero">
                                <div className="w-100">
                                    <div className="text-light text-center">
                                        <Typography variant="title" component="h3" className="text-light mb-4">منصات العقارية</Typography>
                                        <Typography variant="display2" component="h2" className="text-light mb-4">عرض وطلب الوحدات والخدمات
                                            العقارية</Typography>
                                    </div>
                                    <RequestsFilter
                                        search={ {
                                            buttonText: 'ابحث',
                                            buttonProps: {
                                                to: '/offers?' + callToActionLink,
                                                component: Link
                                            }
                                        } }
                                        extra={ ( r ) => <DropDown
                                            { ...r }
                                            options={ [
                                                {
                                                    name: 'العروض',
                                                    id: 'العروض',
                                                }, {
                                                    name: 'الطلبات',
                                                    id: 'الطلبات',
                                                }
                                            ] }
                                            handleChange={ handleVariant }
                                        >
                                            <LightTypography variant="body2"
                                                             variantcolor={ r.typegraphyColor }
                                            >{ this.state.variant }</LightTypography>
                                        </DropDown> }
                                        filters={ OFFER_FILTERS }
                                        options={ renderFilters }
                                        variant="light"
                                        handler={ filterChangeHandler } { ...filter }/>
                                </div>
                            </div>
                        </div>
                    </div>
                </HeroSection>
                <Offers.component listOnly
                                  filterer={
                                      ( offerList ) => offerList.filter( ( { prop_featured } ) => {
                                          return prop_featured
                                      } )
                                  }
                                  header={ () => <SectionHeader>العروض المميز</SectionHeader> }
                                  footer={ () => <div className="container">
                                      <div className="row justify-content-center">
                                          <div>
                                              <StyledSectionCallToAction color="primary" component={ Link } to="/add-offer" variant="contained">
                                                  <Icon>add</Icon>اضف
                                                  عرض</StyledSectionCallToAction>
                                              <StyledSectionCallToAction variant="outlined" component={ Link } to="/offers" color="primary">المزيد من
                                                  العروض</StyledSectionCallToAction>
                                          </div>
                                      </div>
                                  </div> }
                />
                <Requests.component listOnly wrapperColor={ lightGrey }
                                    header={ () => <SectionHeader>الطلبات المميزه</SectionHeader> }
                                    footer={ () => <div className="container">
                                        <div className="row justify-content-center">
                                            <div>
                                                <StyledSectionCallToAction component={ Link } to="/add-order" variant="contained" black> <Icon>add</Icon>اضف طلب</StyledSectionCallToAction>
                                                <StyledSectionCallToAction component={ Link } to="/requests" variant="outlined" black>المزيد من
                                                    الطلبات</StyledSectionCallToAction>
                                            </div>
                                        </div>
                                    </div> }
                >
                </Requests.component>
                <PropertyService getRef={ node => this.ps = node }/>
                <AboutUsHero
                    noButtons
                />
                <Clients
                    clients={ clients }
                />
            </>
        )
    }
}


const mapStateToProps = ( { offers, ui, user: { clients } } ) => ( {
    headerHeight: ui.headerHeight,
    offerFilters: offers.offerFilters,
    clients
} )


export default {
    component: connect( mapStateToProps, { getOffersFilters } )( withRouter( withPagination( Main ) ) ),
    loadData: ( { dispatch } ) => {
        dispatch( getOffersFilters() );
    }
};




