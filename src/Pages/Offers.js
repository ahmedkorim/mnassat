import React                              from 'react';
import { connect }                        from "react-redux";
import { withRouter }                     from "react-router";
import { Link }                           from "react-router-dom";
import { arrayOnTheFly }                  from "shared/ArrayOnTheFly";
import { deserialize }                    from "shared/URLhelpers";
import withPagination                     from "src/Components/HOC/WithPagination/WithPagination";
import RequestsFilter                     from "src/Components/ui/Filters/requestsFilters/RequestsFitlers";
import Map                                from "src/Components/ui/Map/Map";
import OfferCard                          from "src/Components/ui/OfferCard/OfferCard";
import OfferCardLoader                    from "src/Components/ui/OfferCard/OfferCardLoader";
import { OFFER_FILTERS, REQUEST_FILTERS } from "src/configs/requestsFilters";
import { Wrapper }                        from "src/Pages/Requests";
import { getAllOffers, getOffersFilters } from "src/store/actions/offersActions";

const countLimiter = ( ( arr ) => arr.slice( Math.max( arr.length - 10, 1 ) ) )

class Offers extends React.Component {
    state = {
        filter: {
            property_category: "كل العروض",
            property_action_category: "كل الاغراض",
            property_county_state: "كل البلاد",
            property_city: "كل المدن",
            property_area: "كل المناطق",
        }
    }

    componentDidMount () {
        if ( this.props.listOnly ) return void 0;
        const { current, perPage } = this.props.paginationInfo;
        const params = this.props.history.location.search;
        if ( params && !this.props.listOnly ) {
            const deserializedPrams = deserialize( params )
            const stateParams = Object.keys( this.state.filter )
                .reduce( ( acc, i ) => ( {
                    ...acc,
                    [ i ]: params && deserializedPrams[ i ] || this.state.filter[ i ]
                } ), {} )

            this.setState( {
                filter: stateParams
            } )
        }
        this.props.getOffersList( {
            page: current,
            perPage
        } )
        this.props.getOfferFilters()
    }

    componentWillUpdate ( { paginationInfo: { current, perPage }, loadingList }, nextState, nextContext ) {
        if ( !this.props.listOnly && ( current !== this.props.paginationInfo.current || perPage !== this.props.paginationInfo.perPage ) ) {
            // get the data and represent them
            this.props.getOffersList( {
                page: current,
                perPage
            } )
        }
    }

    filterChangeHandler = ( target ) => ( value ) => {
        this.setState( {
            filter: {
                ...this.state.filter,
                [ target ]: value
            }
        } )
    }

    render () {
        let {
            state: {
                filter
            },
            filterChangeHandler,
            props: {
                listOnly,
                offersList,
                header,
                footer,
                loadingList,
                wrapperColor,
                currentUser,
                filterer,
                paginationInfo: {
                    perPage,
                    current
                },
                offerFilters
            }
        } = this;
        const renderFilters = {
            property_category: offerFilters[ 'property_category' ],
            property_action_category: offerFilters[ 'property_action_category' ],
            property_county_state: offerFilters[ 'property_county_state' ],
            property_city: offerFilters[ 'property_city' ].filter( i => {
                if ( ( i.id + '' ).indexOf( 'default' ) !== -1 ) return 1
                if ( this.state.filter[ 'property_county_state' ] === 'كل البلاد' ) return 1;
                return i.taxonomy_term_meta.country.name === this.state.filter[ 'property_county_state' ]

            } ),
            property_area: offerFilters[ 'property_area' ].filter( i => {
                if ( ( i.id + '' ).indexOf( 'default' ) !== -1 ) return 1
                if ( this.state.filter[ 'property_city' ] === 'كل المدن' ) return 1;
                return i.taxonomy_term_meta.city.name === this.state.filter[ 'property_city' ]

            } ),
        }
        let filtereList = []
        if ( listOnly ) {
            filtereList = filterer ? countLimiter( filterer( offersList ) ) : countLimiter( offersList )
        } else {
            filtereList = offersList.filter( i => {
                const mapedFilterId = Object.keys( this.state.filter ).reduce( ( acc, key ) => {
                    return {
                        ...acc,
                        [ key ]: Object.keys( offerFilters ).filter( offerFitlerKey => offerFitlerKey === key ).map( ( offerFitlerKey ) => {
                            // only one cat per filter
                            const matchedNames = offerFilters[ offerFitlerKey ].find( ( { name } ) => name === this.state.filter[ key ] );
                            return (
                                matchedNames.default ? 'all' : matchedNames[ 'id' ]
                            );
                        } )[ 0 ]
                    }
                }, {} )
                const filterState = Object.keys( mapedFilterId ).reduce( ( acc, key ) => acc && i[ key ] instanceof Array && ( mapedFilterId[ key ] === 'all' ? true : !!( i[ key ].find( val => val === mapedFilterId[ key ] ) ) )
                    , true )
                return filterState

            } )
        }
        const favorites = currentUser.favorites instanceof Object ? Object.values( currentUser.favorites ) : [];
        const renderList = ( filtereList.filter( ( i, index, { length } ) => length !== offersList.length ? ( ( index + 1 ) >= perPage * ( current - 1 ) && ( index + 1 ) <= perPage * ( current ) )
            : i.index > perPage * ( current - 1 ) && i.index <= perPage * ( current ) ) )
            .map( i => {
                const inUserFavorites = favorites.indexOf( i.id ) > -1;
                console.log( inUserFavorites );
                return {
                    ...i,
                    inUserFavorites
                }
            } )

        return (
            <>
                { !listOnly && <Map locations={ offersList } loading={ loadingList }
                                    showInfoWindow={ {
                                        data: offerFilters,
                                        keys: 'property_category'
                                    } }
                                    detials={ ( id ) => {
                                        this.props.history.push( `/details/${ id }?type=offer` )
                                    } }
                /> }
                <Wrapper bgColor={ wrapperColor }>
                    { listOnly && header && header() }
                    <div className="container">
                        {
                            !listOnly && <RequestsFilter
                                filters={ OFFER_FILTERS }
                                options={ renderFilters }
                                search={ {
                                    buttonText: 'اضف عرض',
                                    buttonProps: {
                                        to: '/add-offer',
                                        component: Link
                                    }
                                } }
                                handler={ filterChangeHandler } { ...filter }/>
                        }
                        <div className="row  py-4">
                            { !loadingList ?

                                ( renderList && renderList.map( request => <OfferCard data={ request } key={ request.id }/> ) )
                                : arrayOnTheFly( perPage, 'offer card loader' ).map( ( i, index ) => <OfferCardLoader key={ `order card loader ${ i }` }/> )

                            }
                        </div>
                    </div>
                    { !listOnly && this.props.renderPagination( ( { renderedLength: filtereList ? filtereList.length : 0 } ) ) }
                    { listOnly && footer && footer() }
                </Wrapper>
            </>
        )
    }
}

const mapStateToProps = ( { offers, user } ) => ( {
    offersList: offers.offersList,
    currentUser: user.currentUser,
    loadingList: offers.loadingList,
    offerFilters: offers.offerFilters,
    length: offers.offersList.length
} )
const mapDispatchToProps = dispatch => ( {
    getOffersList: ( conf ) => dispatch( getAllOffers( conf ) ),
    getOfferFilters: _ => dispatch( getOffersFilters() )
} )

export default {
    component: connect( mapStateToProps, mapDispatchToProps )( withRouter( withPagination( Offers ) ) ),
    loadData: ( { dispatch } ) => dispatch( getAllOffers() )
};
