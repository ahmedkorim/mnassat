import { Divider, Icon, IconButton, Tooltip }             from "@material-ui/core";
import React                                              from 'react';
import { connect }                                        from "react-redux";
import { Link }                                           from "react-router-dom";
import ContactTheOwner                                    from "src/Components/ui/ContactTheOwner/ContactTheOwner";
import ImageSwipper                                       from "src/Components/ui/ImageSwipper/ImageSwipper";
import Map                                                from "src/Components/ui/Map/Map";
import { SectionHeader }                                  from "src/Components/ui/SectionHeader/SectionHeader";
import ShareButton                                        from "src/Components/ui/shareButton/ShareButton";
import SpecialProperty                                    from "src/Components/ui/SpecialProperty/specialProperty";
import Spinner                                            from "src/Components/ui/Spinner";
import { StyledSectionCallToAction }                      from "src/Pages/Main";
import Offers                                             from "src/Pages/Offers";
import Requests                                           from "src/Pages/Requests";
import { getAllOffers, getOffersFilters, getSingleOffer } from "src/store/actions/offersActions";
import { getAllRequests, getRequestsFitlers }             from "src/store/actions/requestsActions";
import { toggleFavorites }                                from "src/store/actions/usersActions";
import { greyColor, lightGrey, lightMain, mainColor }     from "src/styles/colors";
import styled                                             from 'styled-components';

const Wrapper = styled.div`
padding-top:3rem;
padding-bottom: 3rem;
position: relative;
overflow: hidden;

.header__details-section{
padding-bottom: 1rem !important;

}

`

const PropertyDetails = styled.ul`
list-style: none;
margin:0;
padding: 0;



li{
width: 80%;
border-radius: .1rem;
display: flex;
padding: .8rem .5rem;
&:nth-of-type(2n -1){
background-color: ${ lightMain };
}
dt,dd{
padding: 0;
margin: 0;
font-weight: normal;
}

dt::after{
content: " : ";
}
dd{
margin-right: .7rem;
}
}
`

const Price = styled.div`
display: flex;
align-items: center;
.price__value{
color: ${ mainColor };
font-weight: bold;

}

.property__price-currency{
font-weight: bold;
font-size:.8rem;
}
`

class ProductDetails extends React.Component {
    constructor ( props ) {
        super( props );
        const { location: { search }, match: { params } } = this.props;
        const type = new URLSearchParams( search ).get( 'type' );
        const id = +params.unit_id
        this.state = {
            type,
            id
        }
    }

    componentWillMount () {
    }

    componentWillUpdate ( nextProps, nextState, nextContext ) {
        const id = +nextProps.match.params.unit_id;
        if ( id !== this.state.id ) {
            this.setState( {
                id
            } )
        }
    }

    componentDidMount () {
        if ( this.state.type === 'offer' ) {
            this.props.getSingleOffer( this.state.id )

        } else {

        }

        this.props.loadData( this.state.type )
    }

    render () {
        let {
            offersList, itemData = {}, loadingSingle, users, offerFilters,
            requestsLists,
            requestFilters,
            toggleFavorites,
            favorites
        } = this.props;
        let images = [];
        const inArr = offersList.find( ( { id } ) => id === this.state.id )
        itemData = inArr ? inArr : Object.values( itemData )[ 0 ]
        let noSlider = false;
        let mapData = null;
        let author = {};
        let DisplayData = {}
        if ( itemData && this.state.type === 'offer' ) {
            const imagesIds = itemData[ 'image_attached' ].replace( / /, '' ).split( ',' )
                .filter( i => i !== "" );
            images = imagesIds.map( id => this.props.media.find( i => i.id == id ) ).filter( i => ( i && i.media_type === "image" ) )

            noSlider = imagesIds.length === 0
            const area = ( offerFilters[ 'property_area' ] || [] ).find( ( { id } ) => id == ( itemData[ 'property_area' ] || [] )[ 0 ] )
            const city = ( offerFilters[ 'property_city' ] || [] ).find( ( { id } ) => id == ( itemData[ 'property_city' ] || [] )[ 0 ] )
            const category = ( offerFilters[ 'property_category' ] || [] ).find( ( { id } ) => id == ( itemData[ 'property_category' ] || [] )[ 0 ] )
            const address = itemData[ 'property_address' ]
            const size = itemData[ 'property_size' ]
            DisplayData = {
                area: ( area || {} ).name,
                city: ( city || {} ).name,
                category: ( category || {} ).name,
                address,
                size,
            }
            mapData = itemData;
            author = users.find( ( { id } ) => id === itemData[ 'author' ] )

        } else {
            itemData = requestsLists.find( i => i.id === this.state.id ) || {}
            if ( itemData ) {
                // console.log( requestFilters );
                const country = ( requestFilters[ 'demand_country' ] || [] ).find( ( { id } ) => id == ( itemData[ 'demand_country' ] || [] )[ 0 ] )
                const city = ( requestFilters[ 'demand_city' ] || [] ).find( ( { id } ) => id == ( itemData[ 'demand_city' ] || [] )[ 0 ] )
                const category = ( requestFilters[ 'demand_category' ] || [] ).find( ( { id } ) => id == ( itemData[ 'demand_category' ] || [] )[ 0 ] )
                const address = itemData[ '_street' ]
                const size = itemData[ 'property_size' ]
                mapData = itemData;
                noSlider = true
                DisplayData = {
                    country: ( country || {} ).name,
                    city: ( city || {} ).name,
                    category: ( category || {} ).name,
                    address,
                    size,
                }
            }
            author = users.find( ( { id } ) => id === itemData[ 'author' ] )

        }
        const loading = loadingSingle[ this.state.id ];
        const pic = images.map( i => i.url )
        const fav = favorites instanceof Object ? Object.values( favorites ) : [];

        const inUserFavorites = fav.indexOf( this.state.id ) > -1;
        const More =      () => {
            const key = this.state.type === 'request' ? 'demand_category' : 'property_action_category';
            return this.state.type === 'request' ? <Requests.component
                listOnly
                filterer={
                    ( offerList ) => offerList.filter( i => {
                        return ( i[ key ] && itemData[ key ] ) && ( i[ key ][ 0 ] === itemData[ key ][ 0 ] )
                    } )
                }
                header={ () => <SectionHeader>عروض قد تهمك </SectionHeader> }
            /> : <Offers.component listOnly
                                   filterer={
                                       ( offerList ) => offerList.filter( i => {
                                           return ( i[ key ] && itemData[ key ] ) && ( i[ key ][ 0 ] === itemData[ key ][ 0 ] )
                                       } )
                                   }
                                   header={ () => <SectionHeader>طلبات قد تهمك </SectionHeader> }
            />
        }
        return (
            <Wrapper>
                <div className="container">
                    {
                        ( ( loading || !itemData ) && this.state.type !== 'request' ) ?
                            <Spinner fixed/> : <>
                                <section className="row">
                                    <article className="col-12 col-lg-8">
                                        <header className="d-flex align-items-center text-right pb-4">
                                            <div>
                                                <div className="d-flex align-items-center"><h1 className="h4">{ itemData[ 'title' ] } </h1>
                                                    <span className="px-2">{ this.state.type === 'offer' ? 'للبيع' : 'شراء' }</span>
                                                </div>
                                                { itemData[ 'property_price' ] &&
                                                <Price> <span className="price__value">{ itemData[ 'property_price' ] } </span> &nbsp; <span
                                                    className="property__price-currency">  ريال سعودي </span></Price> }
                                            </div>
                                            <div className="mr-auto">
                                                <ShareButton url={ `/details/${ this.state.id }?type=${ this.state.type }` }/>
                                                <Tooltip title={ inUserFavorites ? 'احذف من المفضله' : 'اضف الي المفضله' }
                                                         children={ <IconButton color={ inUserFavorites ? 'secondary' : 'default' }
                                                                                onClick={ () => toggleFavorites( this.state.id ) }><Icon>favorite</Icon></IconButton> }
                                                />
                                            </div>
                                        </header>
                                        {
                                            !noSlider && <>
                                                <section style={
                                                    {
                                                        height: 400,
                                                        position: 'relative'
                                                    }
                                                }>
                                                    { itemData[ 'prop_featured' ] && <SpecialProperty/> }
                                                    { pic.length !== 0 ? <ImageSwipper
                                                        imagesArray={ pic }
                                                    /> : <Spinner/> }
                                                </section>
                                                <Divider/>
                                            </>
                                        }
                                        <section className="text-right py-4">
                                            <h4 className="h6 header__details-section font-weight-bold">تفاصيل العرض</h4>
                                            <PropertyDetails>
                                                <li>
                                                    <dt>{ DisplayData[ 'country' ] || DisplayData[ 'city' ] }</dt>
                                                    <dd>{ DisplayData[ 'area' ] || DisplayData[ 'city' ] }</dd>
                                                </li>
                                                {
                                                    DisplayData[ 'address' ] && <li>
                                                        <dt>العنوان</dt>
                                                        <dd>{ DisplayData[ 'address' ] }</dd>
                                                    </li>
                                                }
                                                <li>
                                                    <dt>نوع العقار</dt>
                                                    <dd>{ DisplayData[ 'category' ] }</dd>
                                                </li>
                                                { this.state.type === 'offer' && <li>
                                                    <dt>المساحه</dt>
                                                    <dd>{ DisplayData[ 'size' ] ? DisplayData[ 'size' ] + 'متر مربع' : 'المساحه غير معلنة ' }  </dd>
                                                </li> }
                                                {
                                                    itemData[ 'property_floor_plans' ] && <li>
                                                        <dt>عدد الطوابق</dt>
                                                        <dd>{ itemData[ 'property_floor_plans' ] || '0' }  </dd>
                                                    </li>
                                                }
                                                <div
                                                    dangerouslySetInnerHTML={ {
                                                        __html: itemData[ 'content' ]
                                                    } }
                                                />
                                            </PropertyDetails>
                                        </section>
                                        { itemData[ 'embed_virtual_tour' ] && <section className="text-right py-4">
                                            <h4 className="h6 header__details-section font-weight-bold">تصوير العقار ثري دي</h4>
                                            <div className="my-2" dangerouslySetInnerHTML={ { __html: itemData[ 'embed_virtual_tour' ] } }/>
                                        </section> }
                                        <Divider/>
                                        {
                                            mapData && <section className="text-right py-4">
                                                <h4 className="h6 header__details-section font-weight-bold">الموقع علي الخريطه</h4>
                                                <Map
                                                    zoom={ 9 }
                                                    height={ 350 }
                                                    locations={ [ { ...mapData } ] } loading={ false }/>
                                            </section>
                                        }
                                    </article>
                                    <aside className="col-12 col-lg-4">
                                        <ContactTheOwner
                                            author={ author }
                                        />
                                    </aside>
                                </section>
                                <section>
                                </section>
                            </>
                    }
                </div>
                <More/>
            </Wrapper>
        )
    }
}

const mapDispatchToProps = dispatch => ( {
    getSingleOffer: ( id ) => ( dispatch( getSingleOffer( id ) ) ),
    loadData: () => loadData( { dispatch } ),
    toggleFavorites: ( id ) => dispatch( toggleFavorites( id ) )
} )
const mapStateToProps = ( {
                              offers,
                              miscellaneous,
                              user: {
                                  allUsers, currentUser: {
                                      favorites
                                  }
                              },
                              requests: { requestsLists, requestFilters }
                          } ) => ( {
    itemData: offers.singleFetch,
    loadingSingle: offers.loadingSingle,
    offersList: offers.offersList,
    offerFilters: offers.offerFilters,
    media: miscellaneous.media,
    users: allUsers,
    favorites,
    // requests
    requestsLists,
    requestFilters
} )

const loadData = ( { dispatch } ) => {
    dispatch( getOffersFilters() )
    dispatch( getRequestsFitlers() )
    dispatch( getAllOffers() )
    dispatch( getAllRequests() )
}

export default {
    component: connect( mapStateToProps, mapDispatchToProps )( ProductDetails ),
};
