import Button              from "@material-ui/core/Button";
import { get }             from "axios";
import React               from 'react';
import { connect }         from "react-redux";
import EnhancedTabs        from "src/Components/ui/EnhancedTabs/EnhancedTabs";
import EditProfile         from "src/Components/ui/Profile/EditProfile";
import Favorites           from "src/Components/ui/Profile/Favorites";
import Invoices            from "src/Components/ui/Profile/Invoices";
import Messages            from "src/Components/ui/Profile/Messages";
import PropertyCardProfile from "src/Components/ui/Profile/PropertyCard";
import { invoicesApi }     from "src/configs/urls";
import { getAllOffers }    from "src/store/actions/offersActions";
import { getAllRequests }  from "src/store/actions/requestsActions";
import { getAllMessages }  from "src/store/actions/usersActions";
import { Link }            from "react-router-dom";

import { darkColor, greyColor, lightColor, lightMain } from "src/styles/colors";
import styled                                          from "styled-components";


const Wrapper = styled.div`
background-color: ${ lightColor };
padding: 3rem 0;
text-align: right;
.tab__root-profile{
border-bottom: 1px groove rgba(0,0,0,.3);
box-sizing: border-box !important;
}
`

const UserAvatar = styled.img`
border-radius: .4rem;
margin-bottom: 2rem;
min-width:220px;
display: block;
max-width: 100%;

`

const UserDetails = styled.aside`
.header__user-denials{
font-weight: bold;
font-size:1.2rem;
}
p{
color: ${ greyColor };
margin-bottom: .7rem;
font-size:1.1rem;
}

`
const StyledButton = styled( Button )`
&&{
background-color: ${ lightMain } ;
color: ${ darkColor } ;
padding: .6rem 1.5rem ;
display: block;
width: 100%;
max-width: 270px; 
margin-top: 1rem !important ;
}
`
const StyledTabWrapper = styled.div`

padding: 1rem 0;
height: 77vh;
overflow: auto;
overflow-x:hidden 
`

class Profile extends React.Component {
    state = {
        myInvoices: []
    }

    updateMesssages = ( messages ) => {
        this.setState( {
            myMessages: messages
        } )
    }

    componentWillUpdate ( nextProps, nextState, nextContext ) {
        const search = nextProps.location.search;

        const edit = new URLSearchParams( search ).get( 'edit' ) === 'true';
        if ( edit !== this.state.edit ) {
            this.setState( {
                edit: edit
            } )
        }

    }

    async componentDidMount () {
        this.props.loadData()

        try {
            const { data } = await get( invoicesApi )
            this.setState( { myInvoices: data } )

        } catch ( e ) {
            console.log( e );
        }
    }

    render () {
        const {
            currentUser: {
                name,
                id,
                avatar,
                description,
                favorites,
                mobile,
                email
            },
            currentUser,
            offersList,
            requestsLists: requestsList,
            allUsers,
            messages: myMessages
        } = this.props
        const { myInvoices } = this.state;
        const myFav = currentUser.favorites instanceof Object ? Object.values( currentUser.favorites ) : [];

        const invoices = myInvoices.filter( ( { author } ) => author === id )
        const renderMessages = myMessages
            .filter( ( {
                           message_to_user,
                           message_from_user,
                       } ) => !!message_to_user && !!message_from_user && ( +message_to_user === +currentUser.id || +message_from_user === +currentUser.id ) )

            .map( ( { author, message_from_user, message_to_user, content: { rendered: message }, date, id } ) => {
                const contactId = message_from_user == currentUser.id ? +message_to_user : +message_from_user;
                return ( {
                    messageFrom: allUsers.find( i => +i.id === +contactId ),
                    message_to_user,
                    contactId,
                    message_from_user,
                    message,
                    date,
                    author,
                    id
                } );
            } )

        const myOffers = offersList.filter( ( { author } ) => currentUser.id === author )
        const myRequests = requestsList.filter( ( { author } ) => currentUser.id === author )
        console.log( offersList );
        return (
            <Wrapper>
                <div className="container">
                    <section className="row">
                        <UserDetails className="col-lg-3 col d-flex  align-items-center justify-content-center  justify-content-md-start flex-column mb-5">
                            <div><UserAvatar src={ avatar } alt={ `image of ${ name }` }/></div>
                            <h4 className="header__user-denials mb-3">{ name }</h4>
                            <p>{ email }</p>
                            <p>{ mobile }</p>
                            <p>{ description }</p>
                            <StyledButton
                                component={ Link }
                                to={ this.state.edit ? '/profile' : '/profile?edit=true' }
                            >{
                                this.state.edit ?
                                    "اظهر الانشطه"
                                    : "تعديل البيانات الشخصيه"
                            }</StyledButton>
                        </UserDetails>
                        <div className="col-lg-9 col">
                            {
                                this.state.edit ?
                                    <EditProfile/>
                                    :
                                    <EnhancedTabs
                                        noShadow
                                        value={ 0 }
                                        paddges={ [
                                            {
                                                name: 'عروضي',
                                                label: 'offers',
                                                count: myOffers.length
                                            },
                                            {
                                                name: 'طلباتي',
                                                label: 'orders',
                                                count: myRequests.length
                                            },
                                            {
                                                name: 'مفضله',
                                                label: 'favourites',
                                                count: myFav.length
                                            }, {
                                                name: 'رسايل',
                                                label: 'messages',
                                                count: renderMessages.length
                                            }, {
                                                name: 'فواتير',
                                                label: 'invoices',
                                                count: invoices.length
                                            }
                                        ] }
                                        toolbarClasses={ [ 'p-0' ] }
                                        rootClass={ [ 'justify-content-start', 'tab__root-profile' ] }
                                    >
                                        <StyledTabWrapper>
                                            { myOffers.length > 0 ? myOffers.map( ( { id, featured_img_url, title, property_price, property_county, property_address } ) =>
                                                <PropertyCardProfile
                                                    id={ `profile__offer ${ id }` }
                                                    _id={ id }
                                                    imgSrc={ featured_img_url }
                                                    price={ property_price }
                                                    title={ title }
                                                    address={ `${ property_county } ${ property_address }` }
                                                /> ) : <div className="p-4 text-center font-weight-bold">لا توجد عروض</div> }
                                        </StyledTabWrapper>
                                        <StyledTabWrapper>
                                            { myRequests.length > 0 ? myOffers.map( ( { id, title, demand_city, _street } ) =>
                                                <PropertyCardProfile
                                                    id={ ` profile_requests ${ id }` }
                                                    _id={ id }
                                                    title={ title }
                                                    address={ `${ demand_city } ${ _street }` }
                                                /> ) : <div className="p-4 text-center font-weight-bold">لا توجد طلبات</div> }
                                        </StyledTabWrapper>
                                        <StyledTabWrapper>
                                            <Favorites
                                                favorites={ myFav }
                                                offers={ offersList }
                                                requests={ requestsList }
                                            />
                                        </StyledTabWrapper>
                                        <StyledTabWrapper>
                                            <Messages
                                                messages={ renderMessages }
                                                currentUser={ currentUser }
                                            />
                                        </StyledTabWrapper>
                                        <StyledTabWrapper>
                                            <Invoices
                                                invoices={ invoices }
                                            />
                                        </StyledTabWrapper>
                                    </EnhancedTabs> }
                        </div>
                    </section>
                </div>
            </Wrapper>
        )
    }
}

const loadData = ( { dispatch } ) => {
    dispatch( getAllOffers() )
    dispatch( getAllRequests() )
    dispatch( getAllMessages() )
}


const mapStateToProps = ( { user: { messages, currentUser, allUsers }, offers: { offersList = [] }, requests: { requestsLists = [] } } ) => ( {
    currentUser,
    allUsers, messages,
    offersList,
    requestsLists,
} )

const mapDispatchToProps = ( dispatch ) => ( {
    loadData: () => loadData( { dispatch } )
} )

export default {
    component: connect( mapStateToProps, mapDispatchToProps )( Profile ),
    loadData
}
