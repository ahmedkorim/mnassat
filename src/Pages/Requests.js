import React                                  from 'react';
import { connect }                            from "react-redux";
import { withRouter }                         from "react-router";
import { Link }                               from "react-router-dom";
import { arrayOnTheFly }                      from "shared/ArrayOnTheFly";
import withPagination                         from "src/Components/HOC/WithPagination/WithPagination";
import RequestsFilter                         from "src/Components/ui/Filters/requestsFilters/RequestsFitlers";
import Map                                    from "src/Components/ui/Map/Map";
import OrderCard                              from "src/Components/ui/OrderCard/OrderCard";
import OrderCardLoader                        from "src/Components/ui/OrderCard/OrderCardLoader";
import { REQUEST_FILTERS }                    from "src/configs/requestsFilters";
import { getAllRequests, getRequestsFitlers } from "src/store/actions/requestsActions";
import { darkColor, lightColor }              from "src/styles/colors";
import styled                                 from "styled-components";

export const Wrapper = styled.div`
background-color: ${ ( { bgColor } ) => bgColor || lightColor };
color:${ ( { color } ) => color || darkColor };
padding: 2rem 0;
`

class Requests extends React.Component {
    state = {
        filter: {
            demand_category: "كل العروض",
            demand_action: "كل الاغراض",
            demand_country: "كل البلاد",
            demand_city: "كل المدن",
            demand_neighborhood: "كل المناطق",
        }
    }

    componentDidMount () {
        const { current, perPage } = this.props.paginationInfo;
        this.props.getRequestsList( {
            page: current,
            perPage
        } )
        this.props.getRequestsFilter();

    }

    componentWillUpdate ( { paginationInfo: { current, perPage }, loadingList }, nextState, nextContext ) {
        if ( ( current !== this.props.paginationInfo.current || perPage !== this.props.paginationInfo.perPage ) && !loadingList ) {
            // get the data and represent them
            this.props.getRequestsList( {
                page: current,
                perPage
            } )
        }
    }


    filterChangeHandler = ( target ) => ( value ) => {
        this.setState( {
            filter: {
                ...this.state.filter,
                [ target ]: value
            }
        } )
    }

    render () {
        let {
            state: {
                filter
            },
            filterChangeHandler,
            props: {
                currentUser,
                listOnly,
                requestsLists,
                loadingList,
                wrapperColor,
                footer,
                header,
                paginationInfo: {
                    perPage,
                    current
                },
                requestFilters

            }
        } = this;
        console.log( this.state.filter );
        const renderFilters = {
            ...requestFilters,
            demand_city: requestFilters[ 'demand_city' ],
            demand_neighborhood: requestFilters[ 'demand_neighborhood' ].filter( i => {
                if ( ( i.id + '' ).indexOf( 'default' ) !== -1 ) return 1
                if ( this.state.filter[ 'demand_city' ] === 'كل المدن' ) return 1;
                return i.taxonomy_term_meta.city.name === this.state.filter[ 'demand_city' ]
            } )
        }
        console.log( renderFilters );
        perPage = listOnly ? 8 : perPage;

        const filtereList = requestsLists.filter( i => {

            const mapedFilterId = Object.keys( filter ).reduce( ( acc, key ) => {
                return {
                    ...acc,
                    [ key ]: Object.keys( requestFilters ).filter( filterKey => filterKey === key ).map( ( filterKey ) => {
                        // only one cat per filter
                        const matchedNames = requestFilters[ filterKey ].find( ( { name } ) => name === this.state.filter[ key ] );
                        return (
                            matchedNames.default ? 'all' : matchedNames[ 'id' ]
                        );
                    } )[ 0 ]
                }

            }, {} )
            const filterState = Object.keys( mapedFilterId ).reduce( ( acc, key ) => acc && i[ key ] && ( mapedFilterId[ key ] === 'all' ? true : !!( i[ key ].find( val => val === mapedFilterId[ key ] ) ) )
                , true )
            return filterState

        } )
        const favorites = currentUser.favorites instanceof Object ? Object.values( currentUser.favorites ) : [];

        const renderList = filtereList.filter( ( i, index, { length } ) => length !== requestsLists.length ? ( ( index + 1 ) >= perPage * ( current - 1 ) && ( index + 1 ) <= perPage * ( current ) )
            : i.index > perPage * ( current - 1 ) && i.index <= perPage * ( current ) )
            .map( i => {
                const inUserFavorites = favorites.indexOf( i.id ) > -1;
                console.log( inUserFavorites );
                return {
                    ...i,
                    inUserFavorites
                }
            } )
        return (
            <>
                { !listOnly && <Map locations={ requestsLists } loading={ loadingList }
                                    detials={ ( id ) => {
                                        this.props.history.push( `/details/${ id }?type=order` )
                                    } }
                                    showInfoWindow={ {
                                        data: requestFilters,
                                        keys: 'demand_category'
                                    } }
                /> }
                <Wrapper bgColor={ wrapperColor }>
                    { listOnly && header && header() }
                    <div className="container">
                        {
                            !listOnly && <RequestsFilter
                                filters={ REQUEST_FILTERS }
                                options={ renderFilters }
                                search={ {
                                    buttonText: 'اضف طلب',
                                    buttonProps: {
                                        to: '/add-order',
                                        component: Link
                                    }
                                } }
                                handler={ filterChangeHandler } { ...filter }/>
                        }
                        <div className="row flex-wrap py-4">
                            { !loadingList ?

                                ( renderList && renderList.map( request => <OrderCard data={ request } key={ request.id }/> ) )
                                : arrayOnTheFly( perPage, 'order card place holder' ).map( ( i, index ) => <OrderCardLoader
                                    key={ `order card loader ${ i }` }/> )

                            }
                        </div>
                    </div>
                    { !listOnly && this.props.renderPagination( ( { renderedLength: filtereList ? filtereList.length : 0 } ) ) }
                    { listOnly && footer && footer() }
                </Wrapper>
            </>
        )
    }
}

const mapStateToProps = ( { requests, user: { currentUser } } ) => ( {
    requestsLists: requests.requestsLists,
    loadingList: requests.loadingList,
    requestFilters: requests.requestFilters,
    length: requests.requestsLists.length,
    currentUser

} )
const mapDispatchToProps = dispatch => ( {
    getRequestsList: ( conf ) => dispatch( getAllRequests( conf ) ),
    getRequestsFilter: _ => dispatch( getRequestsFitlers() )

} )
export default {
    component: connect( mapStateToProps, mapDispatchToProps )( withRouter( withPagination( Requests ) ) ),
    loadData: ( { dispatch } ) => dispatch( getAllRequests() )
};
