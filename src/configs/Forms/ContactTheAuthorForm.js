import { limitedCharNumber, matchFiledValue, maxInput, mustBeAnEmail, validLocation } from "shared/Forms/formValidation";
import countryCodes                                                                   from "src/configs/counteryCodes";


export const formState = {
    formValue: {
        content: "",
    },
    formFeedback: {
        content: "",
    }
}
export const form = [
    {
        label: 'الرساله',
        type: 'long_text',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'content',
        step: 1,
    }
]
const contactTheAuthorForm = {
    form,
    formState
}
export default contactTheAuthorForm;
