import { limitedCharNumber, matchFiledValue, mustBeAnEmail, validLocation } from "shared/Forms/formValidation";
import countryCodes                                                         from "src/configs/counteryCodes";

export const formState = {
    formValue: {
        sh_services: "",
        image_to_attach: "",
        image_attached: "",
        prop_featured: "",
        property_address: "",
        _phone_number: "",
        _whatsapp_number: "",
        property_city: "",
        featured_media: "",
        property_size: "",
        property_rooms: "",
        title: "",
        property_price: "",
        property_action_category: "",
        property_county_state: "",
        property_area: "",
        content: "",
        map: {
            _latitude: "",
            _longitude: ""
        },
    },
    formFeedback: {
        sh_services: "",
        image_to_attach: "",
        property_address: "",
        image_attached: "",
        _phone_number: "",
        _whatsapp_number: "",
        property_city: "",
        property_size: "",
        property_rooms: "",
        title: "",
        property_price: "",
        content: "",
        map: "",

    }, step: 1,
    lastStep: 6
}
export const form = [
    {
        label: 'نوع العقار',
        type: 'select',
        _selectValue: 'id',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'property_category',
        step: 1,
        optionsName: 'property_category',
        optionValueKey: 'value'
    }, {
        label: 'الغرض',
        type: 'select',
        _selectValue: 'id',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'property_action_category',
        step: 1,
        optionsName: 'property_action_category',
        optionValueKey: 'value'
    },
    {
        label: 'اسم العرض',
        type: 'text',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'title',
        step: 1,
    }, {
        label: 'سعر العرض',
        type: 'text',
        htmlType: 'number',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'property_price',
        step: 1,
    }, {
        label: 'وصف العرض',
        type: 'long_text',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'content',
        step: 1,
    }, {
        label: 'المساحه (متر مربع)',
        type: 'text',
        htmlType: 'number',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'property_size',
        step: 2,
    }, {
        label: 'عدد الغرف',
        type: 'text',
        htmlType: 'number',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'property_rooms',
        step: 2,
    }, {
        label: 'رقم الهاتف',
        type: 'text',
        htmlType: 'number',
        required: true,
        validators: [ limitedCharNumber( 10, 14 ) ],
        gridClass: 'col-12',
        name: '_phone_number',
        step: 2,

    }, {
        label: 'واتساب',
        type: 'text',
        htmlType: 'number',
        required: true,
        validators: [ limitedCharNumber( 10, 14 ) ],
        gridClass: 'col-12',
        name: '_whatsapp_number',
        step: 2,

    }, {
        label: 'الدولة',
        type: 'select',
        _selectValue: 'id',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'property_county_state',
        step: 3,
        optionsName: 'property_county_state',
        optionValueKey: 'value'
    }, {
        label: 'المدينه',
        type: 'select',
        _selectValue: 'id',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'property_city',
        filterValues: ( options, allValues ) => options.filter( i => {
            if ( !allValues[ 'property_county_state' ] ) return 1;
            return +i.taxonomy_term_meta.country.term_id === +allValues[ 'property_county_state' ]
        } )
        ,
        optionValueKey: 'property_city',
        step: 3,
    }, {
        label: 'المنطقه',
        type: 'select',
        _selectValue: 'id',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'property_area',
        filterValues: ( options, allValues ) => options.filter( i => {
            if ( !allValues[ 'property_city' ] ) return 1;
            return +i.taxonomy_term_meta.city.term_id === +allValues[ 'property_city' ]
        } ),
        optionValueKey: 'property_area',
        step: 3,
    }, {
        label: 'العنوان',
        type: 'text',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'property_address',
        step: 3,
    }, {
        label: 'اختر عنوان الوحده',
        type: 'map',
        required: true,
        validators: [ validLocation ],
        gridClass: 'col-12',
        name: 'map',
        step: 4,
    }, {
        label: 'اختار صوره واحده عللي الاقل',
        type: 'images_picker',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'image_to_attach',
        step: 5,
    }, {
        label: 'اختر ملف الوحده',
        type: 'file_picker',
        validators: [],
        gridClass: 'col-12',
        name: 'image_attached',
        step: 5,
    }, {
        label: 'اختر خدمه',
        type: 'service_select',
        required: true,
        size: 'small',
        validators: [],
        gridClass: 'col-12',
        name: 'sh_services',
        step: 6,
    },
]
const addOfferFrom = {
    form,
    formState
}
export default addOfferFrom;
