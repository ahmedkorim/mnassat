import { limitedCharNumber, matchFiledValue, maxInput, mustBeAnEmail, validLocation } from "shared/Forms/formValidation";
import countryCodes                                                                   from "src/configs/counteryCodes";


export const formState = {
    formValue: {
        title: "",
        demand_category: "",
        content: "",
        demand_city: "",
        demand_country: "",
        _phone_number: "",
        _street: "",
        demand_neighborhood: "",
        _property_size: "",
        demand_action: "",
        map: {
            _latitude: "",
            _longitude: ""
        }
    },
    formFeedback: {
        title: "",
        demand_category: "",//الفئه
        content: "",
        demand_city: "",
        demand_country: "",
        _phone_number: "",
        _street: "",
        demand_neighborhood: "",//الحي
        _property_size: "",
        demand_action: "", // for
        map: "",
    }, step: 1,
    lastStep: 2
}
export const form = [

    {
        label: 'الدولة',
        type: 'select',
        _selectValue: 'id',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'demand_country',
        step: 1,
        optionsName: 'demand_country',
        optionValueKey: 'value'
    }, {
        label: 'المدينه',
        type: 'select',
        _selectValue: 'id',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'demand_city',
        optionValueKey: 'demand_city',
        step: 1,
    },  {
        label: 'الحي',
        type: 'select',
        _selectValue: 'id',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'demand_neighborhood',
        filterValues: ( options, allValues ) => options.filter( i => {
            if ( !allValues[ 'demand_city' ] ) return 1;
            return +i.taxonomy_term_meta.country.term_id === +allValues[ 'demand_city' ]
        } ),
        optionValueKey: "demand_neighborhood",
        step: 1,
    },{
        label: 'رقم الجوال',
        htmlType: 'number',
        required: true,
        validators: [ limitedCharNumber( 10, 14 ) ],
        gridClass: 'col-12',
        name: '_phone_number',
        step: 1,
    }, {
        label: 'اختر الفئه',
        type: 'select',
        _selectValue: 'id',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'demand_category',
        optionValueKey: 'demand_category',
        step: 1,
    }, {
        label: 'العنوان',
        type: 'text',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: '_street',
        step: 1,
    }, {
        label: 'المساحه المطلوبه',
        type: 'text',
        htmlType: 'number',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: '_property_size',
        step: 1,
    }, {
        label: 'الغرض من الوحدة',
        type: 'select',
        _selectValue: 'id',
        required: true,
        gridClass: 'col-12',
        name: 'demand_action',
        optionValueKey: 'demand_action',
        step: 1,
    }, {
        label: 'اختر عنوان الوحده',
        type: 'map',
        required: true,
        validators: [ validLocation ],
        gridClass: 'col-12',
        name: 'map',
        step: 2,
    }, {
        label: 'مسمي الوحدة',
        type: 'text',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'title',
        step: 1,
    }, {
        label: 'وصف الوحدة',
        type: 'long_text',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'content',
        step: 1,
    }
]
const addRequestFrom = {
    form,
    formState
}
export default addRequestFrom;
