import { mustBeAnEmail } from "shared/Forms/formValidation";

export const formState = {
    formValue: {
        name: '',
        email: '',
        message: ''
    },
    formFeedback: {
        email: '',
        name: '',
        message: ''
    }
}
export const form = [ {
        label: 'الاسم',
        type: 'text',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'name'
    }, {
        label: 'البريد الالكتروني',
        type: 'email',
        required: true,
        validators: [ mustBeAnEmail ],
        gridClass: 'col-12',
        name: 'email'
    }, {
        label: 'رسالتك',
        type: 'long_text',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'message'
    } ]
const contactWithUs = {
    form,
    formState
}
export default contactWithUs;
