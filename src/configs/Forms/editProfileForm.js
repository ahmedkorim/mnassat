import { limitedCharNumber, matchFiledValue, mustBeAnEmail } from "shared/Forms/formValidation";
import countryCodes                                          from "src/configs/counteryCodes";


export const formState = {
    formValue: {
        first_name: '',
        last_name: '',
        name: '',
        email: '',
        phone:''
    },
    formFeedback:{
        first_name: '',
        last_name: '',
        name: '',
        email: '',
        phone:''
    }, step: 1,
    lastStep: 1
}
export const form = [
    {
        label: 'اللقب',
        type: 'text',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'name',
        step: 1,
    }, {
        label: 'الاسم الاول ',
        type: 'text',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'first_name',
        step: 1,
    }, {
        label: 'الاسم الاخير ',
        type: 'text',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'last_name',
        step: 1,
    }, {
        label: 'رقم الهاتف',
        type: 'text',
        htmlType: 'number',
        required: true,
        validators: [ limitedCharNumber( 10, 14 ) ],
        gridClass: 'col-12',
        name: 'phone_number',
        step: 1,

    }, {
        label: 'البريد الالكتروني',
        type: 'email',
        required: true,
        validators: [ mustBeAnEmail ],
        gridClass: 'col-12',
        name: 'email',
        step: 1,
    }
]
const editProfile = {
    form,
    formState
}
export default editProfile;
