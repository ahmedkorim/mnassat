export const formState = {
    formValue: {
        username: '',
        password: '',
    },
    formFeedback: {
        username: '',
        password: '',
    }
}
export const form = [
    {
        label: 'اسم المستخدم',
        type: 'text',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'username'
    },
    {
        label: 'كلمة المرور',
        type: 'password',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'password'
    },
]
const loginForm = {
    form,
    formState
}
export default loginForm;
