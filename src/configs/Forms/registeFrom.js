import { limitedCharNumber, matchFiledValue, mustBeAnEmail } from "shared/Forms/formValidation";
import countryCodes                                          from "src/configs/counteryCodes";

export const formState = {
    formValue: {
        country_code: '',
        phone_number: '',
        code: '',
        username: '',
        email: '',
        password: '',
        confirm_password: ''
    },
    formFeedback: {
        code: '',
        phone_number: '',
        country_code: '',
        username: '',
        email: '',
        password: '',
        confirm_password: ''
    }, step: 1,
    lastStep: 3
}
export const form = [
    {
        label: 'اختر البلد',
        type: 'select',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'country_code',
        step: 1,
        options: countryCodes,
        optionValueKey: 'value'
    },
    {
        label: 'الكود',
        type: 'text',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'code',
        step: 2,
    }, {
        label: 'اسم المستخدم',
        type: 'text',
        required: true,
        validators: [ limitedCharNumber( 0, 40 ) ],
        gridClass: 'col-12',
        name: 'username',
        step: 3,
    }, {
        label: 'البريد الالكتروني',
        type: 'email',
        required: true,
        validators: [ mustBeAnEmail ],
        gridClass: 'col-12',
        name: 'email',
        step: 3,
    }, {
        label: 'رقم الهاتف',
        type: 'text',
        htmlType: 'number',
        required: true,
        validators: [ limitedCharNumber( 10, 14 ) ],
        gridClass: 'col-12',
        name: 'phone_number',
        step: 1,

    }, {
        label: 'رقم الهاتف',
        type: 'text',
        htmlType: 'number',
        readOnly: true,
        required: true,
        validators: [ limitedCharNumber( 10, 14 ) ],
        gridClass: 'col-12',
        name: 'phone_number',
        step: 3,

    }, {
        label: 'كلمة المرور',
        type: 'password',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'password',
        step: 3,
    }, {
        label: 'اعد كتابه كلمه المرور',
        type: 'password',
        required: true,
        validators: [ matchFiledValue( 'password', 'confirm password doesn\'t match your password' ) ],
        gridClass: 'col-12',
        name: 'confirm_password',
        step: 3,
    },
]
const registerFrom = {
    form,
    formState
}
export default registerFrom;
