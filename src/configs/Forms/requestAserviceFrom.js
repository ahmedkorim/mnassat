import { mustBeAnEmail } from "shared/Forms/formValidation";

export const formState = {
    formValue: {
        Title: '',
        ClientName: '',
        Email: '',
        Phone: "",
        UserId: '1',
        AppType: '1',
        Content: '',
        "Token": 45
    },
    formFeedback: {
        Title: "",
        ClientName: "",
        Email: "",
        Phone: "",
        Content: "",

    }, step: 1,
    lastStep: 2
}
export const form = [
    {
        label: '',
        type: 'service_select',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'Title',
        step: 1,
    }, {
        label: 'الاسم',
        type: 'text',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'ClientName',
        step: 2,

    },
    {
        label: 'البريد الالكتروني',
        type: 'email',
        required: true,
        validators: [ mustBeAnEmail ],
        gridClass: 'col-12',
        name: 'Email',
        step: 2,
    }, {
        label: 'الجوال',
        type: 'text',
        htmlType: 'number',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'Phone',
        step: 2,
    }, {
        label: 'استفسار',
        type: 'long_text',
        required: true,
        validators: [],
        gridClass: 'col-12',
        name: 'Content',
        step: 2,
    },
]
const requestAService = {
    form,
    formState
}
export default requestAService;
