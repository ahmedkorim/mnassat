import { createMuiTheme } from "@material-ui/core/styles";

const theme = createMuiTheme( {
    direction: 'rtl',
    typography: {
        fontFamily: [
            'Noto Kufi Arabic', 'sans-serif',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ]
    }
} );

export default theme;
