const countryCodes = [
    {
        name: 'مصر +20',
        id: 'egypt20',
        value: 20
    }, {
        name: 'السعودية +966',
        id: 'ksa966',
        value: 966
    }, {
        name: 'البحرين +973',
        id: 'bh973',
        value: 973
    },
]
export default countryCodes;
