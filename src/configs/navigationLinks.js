const navigationLinks = [ {
    title: 'الرئيسية',
    name: 'main',
    path: '/'
}, {
    title: 'العروض',
    name: 'offers',
    path: '/offers',
}, {
    title: 'الطلبات',
    name: 'requests',
    path: '/requests'
}, {
    title: 'الخدمات',
    name: 'services',
    path: '/services'
},
    {
        title: 'من نحن',
        name: 'about-us',
        path: '/about-us'
    }

]

export const callToActionLinks = [ {
    title: 'اضف عرض',
    name: 'add-offer',
    path: '/add-offer'
}, {
    title: 'اضف طلب',
    name: 'add-request',
    path: '/add-order',
}, {
    title: 'اطلب خدمه',
    name: 'services',
    path: '/services'
}
]

export default navigationLinks
