export const OFFER_FILTERS = [
    "property_category",
    "property_action_category",
    "property_county_state",
    "property_city",
    "property_area"
]
export const REQUEST_FILTERS = [
    "demand_category",
    "demand_action",
    "demand_country",
    "demand_city",
    "demand_neighborhood"
]
