import React          from "react";
import { Redirect }   from "react-router";
import PrivateRoute   from "shared/auth/AuthGuard";
import Login          from "src/Components/auth/Login";
import Register       from "src/Components/auth/Register";
import AboutUs        from "src/Pages/AboutUs";
import AddOffer       from "src/Pages/AddOffer";
import AddRequest     from "src/Pages/AddRequest";
import Auth           from "src/Pages/Auth";
import Main           from "src/Pages/Main";
import Offers         from "src/Pages/Offers";
import ProductDetails from "src/Pages/ProductDetails";
import Profile        from "src/Pages/Profile";
import Requests       from "src/Pages/Requests";

if ( module.hot )
    module.hot.accept()

const routes = [
    {
        path: "/offers",
        ...Offers,
        cacheLife: 0

    },
    {
        path: '/requests',
        ...Requests,
        cacheLife: 0

    },
    {
        path: '/about-us',
        ...AboutUs,
        cacheLife: 0
    },
    {
        path: '/profile',
        component: ( props ) => PrivateRoute( Profile.component, props ),
        loadData: Profile.loadData
    }, {
        path: '/details/:unit_id',
        ...ProductDetails
    },
    {
        path: '/add-order',
        component: ( props ) => PrivateRoute( AddRequest.StyledWrapper( AddRequest.component ), props ),
        loadData: AddRequest.loadData

    }, {
        path: '/add-offer',
        component: ( props ) => PrivateRoute( AddOffer.StyledWrapper( AddOffer.component ), props ),
        loadData: AddOffer.loadData
    },
    {
        path: '/auth',
        component: Auth,
        routes: [
            {
                path: '/auth',
                exact: true,
                component: () => <Redirect to='/auth/login'/>,
            },
            {
                name: 'تسجيل الدخول',
                path: '/auth/login',
                component: Login
            },
            {
                name: 'أنشاء حساب جديد',
                path: '/auth/register',
                component: Register
            }
        ]
    },
    {
        path: '/:section?',
        ...Main,
        exact: true
    }, {
        component: () => <div>Page not found</div>
    },
]
export default routes;
