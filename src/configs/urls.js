import { hostName } from 'src/store/store'

export const messagesApi = `${ hostName }:4443/api/wpestate_message`;
export const invoicesApi = "https://gomenassat.com/wp-json/wp/v2/wpestate_invoice?per_page=90";
export const homePage = `${ hostName }:4444`
export const wpHome = `https://gomenassat.com/wp-json/wp/vs`;
