import { ADD_MEDIA, ADD_SERVICE } from "src/store/actions/actionsTypes";

export const getMedia = ( mediaId ) => {
    return async ( dispatch, getStore, { api } ) => {
        if ( !( mediaId instanceof Array ) ) {
            mediaId = [ mediaId ]
        }
        const responseBundle = await Promise.all( mediaId.map( i => new Promise( async ( res, rej ) => {
            try {
                const resp = api.get( `/media/${ i }` )
                res( resp )
            } catch ( e ) {
                res( {
                    data: null
                } )
            }
        } ) ) )
        const media = getStore().miscellaneous.media;
        const data = responseBundle.reduce( ( acc, i ) => Array.isArray( i.data ) ? acc : [ ...acc, i.data ], [] ).map( ( { guid, id, source_url, media_type, alt_text, author, title } ) => ( {
            url: source_url,
            id,
            media_type,
            alt_text: "",
            author
        } ) )

        const payload = ( [ ...data, ...media ] ).reduce( ( acc, i ) => acc.find( ( { id } ) => id === i.id ) ? acc : [ ...acc, i ], [] )
        dispatch( {
            type: ADD_MEDIA,
            payload
        } )


    };
}

export const getServices = _ => async ( dispatch, getStore, { api } ) => {
    let data;
    try {
        const services = getStore().miscellaneous.services;
        if ( services.length < 0 ) return void 0
        const response = await api.get( '/membership_package' )
        data = response.data;
        const mediaId = data.map( ( { featured_media } ) => featured_media )
        await getMedia( mediaId )( dispatch, getStore, { api } )
        dispatch( {
            type: ADD_SERVICE,
            payload: data
        } )

    } catch ( e ) {
        console.log( e );
    } finally {

    }
}
