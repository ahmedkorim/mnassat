import { GET_ALL_OFFERS, GET_OFFER_FILTERS, GET_OFFERS_LOADING, GET_SINGLE_OFFER, GETTING_SINGLE_OFFER } from "src/store/actions/actionsTypes";
import { getMedia }                                                                                      from "src/store/actions/miscellaneousActions";

export const getAllOffers = ( { perPage, page } = { perPage: 20, page: 1 } ) => async ( dispatch, getState, { api } ) => {
    try {
        const store = getState()
        let data = [ ...store.offers.offersList ];
        // is there an index more that the maxim index we are about to fetch ?
        const limitIndex = perPage * page;
        console.log( data.length );
        const fetch = data.find( item => item.index > limitIndex );
        const dataLength = data.length < perPage * page;
        const dataAlreadyThere = data.filter( item => item.index > perPage * ( page - 1 ) && item.index <= limitIndex ).length === perPage;
        if ( data.length === 0 || dataLength || ( fetch && !dataAlreadyThere ) ) {
            dispatch( setGetAllOffersLoadingState( true ) )
            const response = await api.get( `/offers` )
            const rawData = response.data;
            // merging data
            const massagedData = rawData.map( ( {
                                                    id, date, type, author, image_to_attach, image_attached, property_address, property_county, property_size, property_price,
                                                    title
                                                    , property_latitude, property_longitude, _whatsapp_number, hidden_address,
                                                    _phone_number,
                                                    featured_img_url,
                                                    property_category,
                                                    property_status,
                                                    property_action_category,
                                                    property_county_state,
                                                    property_city,
                                                    property_area,
                                                    use_floor_plans,
                                                    content,
                                                    prop_featured,
                                                    embed_virtual_tour,
                                                }, index ) => ( {
                index: index,
                id, date, type, author, image_to_attach, image_attached, property_address, property_county, property_size, property_price

                , latitude: property_latitude, longitude: property_longitude,
                property_category,
                property_action_category,
                property_county_state,
                property_city,
                property_area,
                _phone_number,
                property_status,
                embed_virtual_tour,
                use_floor_plans,
                prop_featured: prop_featured !== '0',
                content: content.rendered,
                _whatsapp_number, hidden_address, featured_img_url,
                title: title.rendered
            } ) )
            data = ( [ ...data, ...massagedData ] ).reduce( ( acc, i ) => {
                const Exists = acc.find( item => item.id === i.id )
                return Exists ? acc : [ ...acc, i ]
            }, [] )

        } else {
            console.log( 'data is here already' );
        }
        console.log( data.length );
        return dispatch( {
            type: GET_ALL_OFFERS,
            payload: data
        } )
    } catch ( e ) {
        console.log( e );
        console.log( e.response );
        return dispatch( {
            type: GET_ALL_OFFERS,
            payload: []

        } )
    } finally {
        dispatch( setGetAllOffersLoadingState( false ) )
    }

}
export const setGetAllOffersLoadingState = loading => ( {
    type: GET_OFFERS_LOADING,
    payload: loading
} )


export const getOffersFilters = _ => async ( dispatch, getStore, { api } ) => {
    try {
        const alreadyThere = getStore().offers.offerFilters[ `property_category` ].length > 1
        if ( alreadyThere ) return void 0;
        const response = await Promise.all( [
            api.get( '/property_category?per_page=90' ),
            api.get( '/property_action_category?per_page=90' ),
            api.get( '/property_county_state?per_page=90' ),
            api.get( '/property_city' ),
            api.get( '/property_area' )
        ] )
        console.log( response );
        const data = response.map( i => i.data.map( ( { id, description, name, count, ...rest } ) => ( { id, description, name, count, ...rest } ) ) );
        const property_category = [ { id: "propertyCategory__default_value", description: "", default: true, name: "كل العروض", count: 0 }, ...data[ 0 ] ];
        const property_action_category = [ {
            id: "propertyActionCategory__default_value",
            description: "",
            default: true,
            name: "كل الاغراض",
            count: 0
        }, ...data[ 1 ] ];
        const property_county_state = [ {
            id: "propertyCountyState__default_value",
            description: "",
            default: true,
            name: "كل البلاد",
            count: 0
        }, ...data[ 2 ] ];
        const property_city = [ { id: "propertyCity__default_value", description: "", default: true, name: "كل المدن", count: 0 }, ...data[ 3 ] ];
        const property_area = [ { id: "propertyArea__default_value", description: "", default: true, name: "كل المناطق", count: 0 }, ...data[ 4 ] ];
        dispatch( {
            type: GET_OFFER_FILTERS,
            payload: {
                property_category,
                property_action_category,
                property_county_state,
                property_city,
                property_area,
            }
        } )
    } catch ( e ) {
        console.log( e );
    }

}

export const getSingleOffer = ( id ) => async ( dispatch, getState, { api } ) => {
    try {
        const store = getState()
        let data = store.offers.singleFetch
        let list = store.offers.offersList;
        if ( !data.id ) {
            dispatch( loadingSingleItem( id, true ) )
            const onList = list.find( i => i.id === id )
            let response = {}
            if ( onList ) {
                response.data = onList
            } else {
                response = await api.get( `/estate_property/${ id }` )

                response.data = ( ( {
                                        id, date, type, author, image_to_attach, image_attached, property_address, property_county, property_size, property_price,
                                        title
                                        , property_latitude, property_longitude, _whatsapp_number, hidden_address,
                                        _phone_number,
                                        featured_img_url,
                                        property_category,
                                        property_status,
                                        property_action_category,
                                        property_county_state,
                                        property_city,
                                        property_area,
                                        use_floor_plans,
                                        content,
                                        embed_virtual_tour,
                                    } ) => {
                        return ( {
                            id, date, type, author, image_to_attach, image_attached, property_address, property_county, property_size, property_price

                            , latitude: property_latitude, longitude: property_longitude,
                            property_category,
                            property_action_category,
                            property_county_state,
                            property_city,
                            property_area,
                            _phone_number,
                            property_status,
                            embed_virtual_tour,
                            use_floor_plans,
                            content: content.rendered,
                            _whatsapp_number, hidden_address, featured_img_url,
                            title: title.rendered
                        } )
                    }
                )( response.data )


            }

            dispatch( {
                type: GET_SINGLE_OFFER,
                payload: {
                    [ id ]: response.data
                }
            } )
            const imagesArray = response.data[ 'image_attached' ];
            console.log( imagesArray );
            getMedia( imagesArray.split( ',' ).filter( i => i.trim() !== "" ) )( dispatch, getState, { api } )
        }
    } catch ( e ) {
        console.log( e );
    } finally {
        dispatch( loadingSingleItem( id, false ) )

    }
}

const loadingSingleItem = ( id, loading ) => ( {
    type: GETTING_SINGLE_OFFER,
    payload: {
        [ id ]: loading
    }
} )
