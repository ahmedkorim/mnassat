import axios                                                                                                  from "axios";
import { getCookie }                                                                                          from "shared/auth/cookies.helper";
import { GET_ALL_REQUESTS, GET_OFFER_FILTERS, GET_OFFERS_LOADING, GET_REQUEST_FILTERS, GET_REQUESTS_LOADING } from "src/store/actions/actionsTypes";
import { hideSpinner, showSnackbar, showSpinner }                                                             from "src/store/actions/uiActions";

export const getAllRequests = ( { perPage, page } = { perPage: 20, page: 1 } ) => async ( dispatch, getState, { api } ) => {
    try {
        const store = getState()
        let data = store.requests.requestsLists;
        const limitIndex = perPage * page;
        const fetch = data.find( item => item.index > limitIndex );
        const dataLength = data.length < perPage * page;
        const dataAlreadyThere = data.filter( item => item.index > perPage * ( page - 1 ) && item.index <= limitIndex ).length === perPage;
        if ( data.length === 0 || dataLength || ( fetch && !dataAlreadyThere ) ) {
            dispatch( setGettingAllRequestsLoadingState( true ) )
            const response = await api.get( `/requests` )
            const rawData = response.data;
            const massagedData = rawData.map( ( {
                                                    _latitude, _longitude, _street, _phone_number, id, date,
                                                    demand_neighborhood, demand_city, demand_category, author, title,
                                                    content,
                                                    demand_action,
                                                    demand_country,

                                                }, index ) => ( {
                latitude: _latitude,
                longitude: _longitude,
                _street,
                _phone_number,
                id,
                date,
                demand_neighborhood,
                demand_city,
                demand_category,
                author,
                content: content.rendered,
                demand_action,
                demand_country,

                title: title.rendered,
                index: index,
            } ) )
            data = ( [ ...data, ...massagedData ] ).reduce( ( acc, i ) => {
                const Exists = acc.find( item => item.id === i.id )
                return Exists ? acc : [ ...acc, i ]
            }, [] )
        }
        return dispatch( {
            type: GET_ALL_REQUESTS,
            payload: data
        } )
    } catch ( e ) {
        console.log( e );
        return dispatch( {
            type: GET_ALL_REQUESTS,
            payload: []

        } )
    } finally {
        dispatch( setGettingAllRequestsLoadingState( false ) )
    }

}
export const setGettingAllRequestsLoadingState = loading => ( {
    type: GET_REQUESTS_LOADING,
    payload: loading
} )


export const getRequestsFitlers = _ => async ( dispatch, getStore, { api } ) => {

    try {
        const alreadyThere = getStore().requests.requestFilters[ `demand_category` ].length > 1

        if ( alreadyThere ) return console.log( 'already there' );
        const response = await Promise.all( [
            api.get( '/demand_category?per_page=90' ),
            api.get( '/demand_action?per_page=90' ),
            api.get( '/demand_country?per_page=90' ),
            api.get( '/demand_city' ),
            api.get( '/demand_neighborhood' ),
        ] )
        const data = response.map( i => i.data.map( ( { id, description, name, count, ...rest } ) => ( { id, description, name, count, ...rest } ) ) );

        const demand_category = [ { id: "demandCategory_default_value", description: "", default: true, name: "كل العروض", count: "0" }, ...data[ 0 ] ];
        const demand_action = [ { id: "demandAction_default_value", description: "", default: true, name: "كل الاغراض", count: "0" }, ...data[ 1 ] ];
        const demand_country = [ { id: "demandCountry_default_value", description: "", default: true, name: "كل البلاد", count: "0" }, ...data[ 2 ] ];
        const demand_city = [ { id: "demandCity_default_value", description: "", default: true, name: "كل المدن", count: "0" }, ...data[ 3 ] ];
        const demand_neighborhood = [ {
            id: "demandNeighborhood_default_value",
            description: "",
            default: true,
            name: "كل المناطق",
            count: "0"
        }, ...data[ 4 ] ];
        dispatch( {
            type: GET_REQUEST_FILTERS,
            payload: {
                demand_category,
                demand_action,
                demand_country,
                demand_city,
                demand_neighborhood,
            }
        } )
    } catch ( e ) {
        console.log();
    }

}


export const addRequest = ( val ) => async ( dispatch, getStore, { axios } ) => {
    const token = getCookie( '__ssr-auth' );
    if ( token && val ) {
        try {
            // send the first chunk
            const firstChunk = {
                "title": val.title,
                "demand_category": [
                    val.demand_category
                ],
                "content": val.content,
                "status": "pending"
            }
            const opt = {
                headers: {
                    Authorization: `Bearer ${ token }`
                }
            }
            dispatch( showSpinner() )
            dispatch( showSnackbar( {
                message: 'جاري اضافه الطلب'
            } ) )
            const { data } = await axios.post( 'https://gomenassat.com/wp-json/wp/v2/wpresid_dproperty', firstChunk, opt )
            const id = data.id
            // sending the second chunk
            const secondChunk = {
                demand_city: val.demand_city,
                demand_country: val.demand_country,
                _phone_number: val._phone_number,
                _street: val._street,
                demand_neighborhood: val.demand_neighborhood,
                _property_size: val._property_size,
                demand_action: val.demand_action,
                _latitude: val.map._latitude,
                _longitude: val.map._longitude,
                "status": "pending"
            }

            console.log( id );
            const resposne = await axios.post( `https://gomenassat.com/wp-json/wp/v2/wpresid_dproperty/${ id }`, firstChunk, opt )
            dispatch( showSnackbar( {
                message: 'تم اضافه الطلب بنجاح'
            } ) )
            dispatch( hideSpinner() )

            console.log( resposne.data );

        } catch ( e ) {
            console.log( e );
        } finally {

        }
    }
}

export const imageUPload = ( axios, token, file ) => {
    return new Promise( async ( res, rej ) => {
        // upload login here
        try {
            if ( !file ) res( '' )
            const form = new FormData();
            form.append( 'file', file )
            form.append( 'action', 'md_support_save' )
            form.append( 'supporttitle', file.name )
            console.log( 'uploading' + file.name );
            const { data } = await axios.post( 'https://gomenassat.com/wp-json/wp/v2/media', form, {
                headers: {
                    Authorization: `Bearer ${ token }`,
                    'Content-Type': 'multipart/form-data'
                }, mode: 'cors'

            } )
            res( data )
        } catch ( e ) {
            console.log( e );
            if ( e.resposne )
                res( e.response )
            res( e )
        }

    } )
}
export const addOffer = ( payload, history ) => async ( dispatch, getStore, { axios } ) => {
    const {
        content,
        image_attached,
        image_to_attach,
        map,
        sh_services,
        ...rest
    } = payload;
    try {
        const token = getCookie( '__ssr-auth' );
        const pdf = image_attached;
        dispatch( showSpinner() );
        dispatch( showSnackbar( {
            message: ' جاري اضافه العرض :يتم الان رفع الملفات الملحقه'
        } ) )
        const [ _pdf, ..._images ] = await Promise.all( [ imageUPload( axios, token, pdf[ 0 ] )
            , ...image_to_attach.map( file => imageUPload( axios, token, file ) ) ] )
        const {
            _latitude, property_latitude,
            _longitude: property_longitude
        } = map

        dispatch( showSnackbar( {
            message: ' جاري اضافه العرض :تم  رفع الملفات جاري اسال العرض'
        } ) )

        const { data } = await axios.post( 'https://gomenassat.com/wp-json/wp/v2/estate_property', {
            ...rest,
            property_latitude,
            sh_services: sh_services.lastIndexOf( ',' ) === sh_services.length - 1 ? sh_services.slice( 0, sh_services.length - 1 ) : sh_services,
            property_longitude,
            image_to_attach: _pdf ? [ _pdf.id, ..._images.map( i => i.id ) ] : [ ..._images.map( i => i.id ) ],
            featured_media: _images[ 0 ].id,
            status: 'pending'
        }, {
            headers: {
                Authorization: `Bearer ${ token }`,
            }
        } )
        // here the offer si added to wp
        //user name mobile cast
        console.log( data );
        dispatch( showSnackbar( {
            message: `جاري اضافه طلبك تحت كود ${ data.id } برجاء استكمال خطوه الدفع`
        } ) )
        if ( typeof window !== "undefined" ) {
            window.open( 'https://gomenassat.com/paytabs-payment?post_id=' + data.id, 'payment_popup', 'height=350,width=600' )
        }
        history.push( '/' )
    } catch ( e ) {
        dispatch( showSnackbar( {
            message: 'حدث خطأ برجاء اعاده المحاوله'
        } ) )
    } finally {
        dispatch( hideSpinner() )
    }
}

