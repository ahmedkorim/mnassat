import {
    GET_APP_DIMENSIONS,
    GET_APP_HEADER_HEIGHT,
    GET_APP_SCROLL, HIDE_SPINNER,
    OPEN_APP_DRAWER,
    SHOW_SPINNER,
    SNACK_BAR_NEW_MESSAGE
} from "src/store/actions/actionsTypes";

export const getAppHederHeight = ( height ) => ( {
    type: GET_APP_HEADER_HEIGHT,
    payload: height
} )

export const getAppScroll = ( scroll ) => ( {
    type: GET_APP_SCROLL,
    payload: scroll
} )

export const getAppDimensions = ( appDimensions ) => ( {
    type: GET_APP_DIMENSIONS,
    payload: appDimensions
} )

export const toggleAppDrawer = () => ( {
    type: OPEN_APP_DRAWER
} )
export const showSnackbar = ( { message, variant = 'success', ...r } = {} ) => dispatch => dispatch( {
    type: SNACK_BAR_NEW_MESSAGE,
    payload: { message, variant, ...r }
} );
export const showSpinner = _ => ( { type: SHOW_SPINNER } )
export const hideSpinner = _ => ( { type: HIDE_SPINNER } )

