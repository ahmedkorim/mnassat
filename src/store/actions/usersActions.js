import { eraseCookie, getCookie, setCookie }                                                                            from "shared/auth/cookies.helper";
import { GET_ALL_CLIENTS, GET_ALL_MESSAGES, GET_ALL_TEAM_MEMBERS, GET_ALL_USERS, SET_USER_DATA, UPDATE_USER_FAVORITES } from "src/store/actions/actionsTypes";
import { getMedia }                                                                                                     from "src/store/actions/miscellaneousActions";
import { hideSpinner, showSnackbar, showSpinner }                                                                       from "src/store/actions/uiActions";


export const logout = _ => dispatch => {
    eraseCookie( '__ssr-auth' );
    eraseCookie( '__ssr-email' );
    eraseCookie( '__ssr-username' );
    eraseCookie( '__ssr-name' );
    dispatch( {
        type: SET_USER_DATA,
        payload: {
            auth: false,
            name: '',
            email: '',
            username: '',
            mobile: "",
            description: "",
            custom_picture: "",
            avatar: "",
            favorites: "",
            id: "",
        }
    } )
}
export const get_Clients = _ => async ( dispatch, getState, { api } ) => {
    try {
        const { data } = await api.get( '/estate_agency?per_page=90' )
        const mediaId = data.map( ( { featured_media } ) => featured_media )
        await getMedia( mediaId )( dispatch, getState, { api } )

        dispatch( {
            type: GET_ALL_CLIENTS,
            payload: data
        } )
    } catch ( e ) {
        // console.log( e );
    }
}
export const getAllTeamMembers = _ => async ( dispatch, getState, { api } ) => {
    try {
        const store = getState()
        let data = store.user.teamMembers;
        if ( data.length === 0 ) {
            const response = await api.get( '/agents' )
            const rawData = response.data;
            const mediaId = rawData.map( ( { featured_media } ) => featured_media )
            await getMedia( mediaId )( dispatch, getState, { api } )

            data = rawData.map( ( {
                                      first_name,
                                      last_name,
                                      agent_position,
                                      agent_email,
                                      agent_phone,
                                      agent_mobile,
                                      agent_skype,
                                      featured_media,
                                      id
                                  } ) => ( {
                first_name,
                last_name,
                agent_position,
                agent_email,
                agent_phone,
                agent_mobile,
                agent_skype,
                featured_media,
                id,
            } ) )
        }

        return dispatch( {
            type: GET_ALL_TEAM_MEMBERS,
            payload: data
        } )
    } catch ( e ) {
        // console.log( e );
        return dispatch( {
            type: GET_ALL_TEAM_MEMBERS,
            payload: []

        } )
    }

}

export const sendService = ( val ) => async ( dispatch, _, { axios } ) => {
    dispatch( showSpinner() )
    dispatch( showSnackbar( {
        message: "جاري ارسال طلبك"
    } ) )
    try {
        const { data } = await axios.post( 'http://162.212.134.81:2335/ApisTicket/api/Ticket/Create', val, {
            headers: {
                "Content-Type": "application/json",
                "X-Menassat-Token": "A531AD051F0A471F8262FD57E70FE3B9"
            }

        } )
        dispatch( showSnackbar( {
            message: "تم ارسال طلبك"
        } ) )
    } catch ( e ) {
        dispatch( showSnackbar( {
            message: " فشل ارسال طلبك برجاء المحاوله مره اخري",
            variant: 'danger'
        } ) )
    } finally {
        dispatch( hideSpinner() )

    }


}

export const userLogin = ( body ) => async ( dispatch, getState, { nativeApi } ) => {
    return new Promise( async ( res, rej ) => {

        try {
            const { data } = await nativeApi.post( '/login', body )
            const { token, user_display_name: username, user_email: email, user_nicename: name, ...r } = data
            setCookie( '__ssr-auth', token, 60 )
            setCookie( '__ssr-email', email, 60 )
            setCookie( '__ssr-username', username, 60 )
            setCookie( '__ssr-name', name, 60 )
            const payload = {
                auth: !!token,
                name,
                email,
                username,
                native: data

            }
            dispatch( {
                type: SET_USER_DATA,
                payload
            } )
            // console.log( token );

            const response = await nativeApi.get( '/me', {
                headers: {
                    Authorization: `Bearer ${ token }`
                }
            } )
            res();

            // console.log( response.data );
            const { mobile, description, custom_picture, avatar_urls, user_favorites: favorites, id } = response.data;
            const _payload = {
                ...payload,

                ...{ mobile, description, custom_picture, avatar: avatar_urls[ '96' ], favorites, id }
            }
            dispatch( {
                type: SET_USER_DATA,
                payload: _payload
            } )
        } catch
            ( e ) {
            if ( e.response ) {
                rej( e.response.data );
                // console.log( e.response.data );
            } else {
                // console.log( e.response );
            }

        }
    } )
}

export const setUserData = _ => async ( dispatch, _, { nativeApi } ) => {
    // console.log( 'auth...' );
    const token = getCookie( '__ssr-auth' );
    if ( token ) {

        const email = getCookie( '__ssr-email' );
        const username = getCookie( '__ssr-username' );
        const name = getCookie( '__ssr-name' );

        const payload = {
            auth: !!token,
            email,
            username,
            name,
        }
        dispatch( {
            type: SET_USER_DATA,
            payload
        } )

        try {
            const response = await nativeApi.get( '/me', {
                headers: {
                    Authorization: `Bearer ${ token }`
                }
            } )
            // console.log( 'get user data ' );
            const { mobile, description, custom_picture, avatar_urls, user_favorites: favorites, id, } = response.data;
            const _payload = {
                ...payload,
                ...{ mobile, description, custom_picture, avatar: avatar_urls[ '96' ], favorites, id },
                native: response.data
            }
            dispatch( {
                type: SET_USER_DATA,
                payload: _payload
            } )

        } catch ( e ) {
            // console.log( e );
        }

    }

}
export const getAllUsers = _ => async ( dispatch, _, { api } ) => {
    try {
        const { data } = await api.get( '/users' )
        dispatch( {
            payload: data,
            type: GET_ALL_USERS
        } )
    } catch ( e ) {
        // console.log( e.response );
    }
}

export const getAllMessages = _ => ( dispatch, getState, { api } ) => {
    return new Promise( async ( r, _ ) => {
        try {
            const { data } = await api.get( '/wpestate_message' )
            dispatch( {
                type: GET_ALL_MESSAGES,
                payload: data

            } )
            r()
        } catch ( e ) {
            _( e.response )
        }
    } )
}

export const toggleFavorites = ( id ) => async ( dispatch, getState, { axios, api } ) => {
    try {
        const token = getCookie( '__ssr-auth' );

        const { data } = await axios.post( 'https://gomenassat.com/wp-json/wp/v2/users/me', {
            'user_favorites': id
        }, {
            headers: {
                Authorization: `Bearer ${ token }`
            }
        } )
        // console.log( data );
        dispatch( {
            type: UPDATE_USER_FAVORITES,
            payload: data.user_favorites
        } )
    } catch ( e ) {

    }
}
export const sendMessage = ( { to, content } ) => ( dispatch, getState, { axios, api } ) => {
    return new Promise( async ( r, _ ) => {

        const token = getCookie( '__ssr-auth' );
        const from = getState().user.currentUser.id;

        if ( token && from ) {
            const messageBody = {
                message_to_user: to,
                message_from_user: from,
                content,
                title: "تم ارسال عن طريق تطبيق الويب",
                status: 'publish'
            }
            try {
                const response = await axios.post( 'https://gomenassat.com/wp-json/wp/v2/wpestate_message', messageBody,
                    {
                        headers: {
                            Authorization: `Bearer ${ token }`,
                        }
                    } )
                // console.log( response.data );

                await getAllMessages()( dispatch, getState, { axios, api } )
                r( 'sent' )
            } catch ( e ) {
                // console.log( e );
                _()
            }

        }
    } )

}
