import { combineReducers }  from 'redux';
import miscellaneousReducer from "src/store/reducers/miscellaneousReducer";
import offersReducer        from "src/store/reducers/offersReducers";
import requestsReducer      from "src/store/reducers/requestsReducer";
import uiReducer            from "src/store/reducers/uiRedcuer";
import usersReducer         from "src/store/reducers/usersReducer";

export default combineReducers( {
    user: usersReducer,
    requests: requestsReducer,
    offers: offersReducer,
    ui: uiReducer,
    miscellaneous: miscellaneousReducer
} )
