import { ADD_MEDIA, ADD_SERVICE } from "src/store/actions/actionsTypes";

const initialState = {
    media: [],
    services: [],
};

const miscellaneousReducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case ADD_MEDIA:
            return {
                ...state,
                media: action.payload
            }
        case ADD_SERVICE:
            return {
                ...state,
                services: action.payload
            }
        default :
            return {
                ...state
            }
    }

}
export default miscellaneousReducer;
