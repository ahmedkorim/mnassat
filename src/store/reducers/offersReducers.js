import { GET_ALL_OFFERS, GET_OFFER_FILTERS, GET_OFFERS_LOADING, GET_SINGLE_OFFER, GETTING_SINGLE_OFFER } from "src/store/actions/actionsTypes";

const initialState = {
    offersList: [],
    singleFetch: {},
    loadingSingle: {},
    loadingList: false,
    offerFilters: {
        property_category: [ { id: "propertyCategory__default_value", description: "", default: true, name: "كل العروض", count: 0 } ],
        property_action_category: [ { id: "propertyActionCategory__default_value", description: "", default: true, name: "كل الاغراض", count: 0 } ],
        property_county_state: [ { id: "propertyCountyState__default_value", description: "", default: true, name: "كل البلاد", count: 0 } ],
        property_city: [ { id: "propertyCity__default_value", description: "", default: true, name: "كل المدن", count: 0 } ],
        property_area: [ { id: "propertyArea__default_value", description: "", default: true, name: "كل المناطق", count: 0 } ],
    }
};

const offersReducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case GETTING_SINGLE_OFFER:
            return {
                ...state,
                loadingSingle: {
                    ...state.loadingSingle,
                    ...action.payload
                },
            }

        case GET_SINGLE_OFFER:
            return {
                ...state,
                singleFetch: {
                    ...state.singleFetch,
                    ...action.payload
                }
            }
        case GET_OFFER_FILTERS:
            return {
                ...state,
                offerFilters: {
                    ...state.offerFilters,
                    ...action.payload,
                }
            }
        case GET_OFFERS_LOADING:
            return {
                ...state,
                loadingList: action.payload
            }
        case GET_ALL_OFFERS :
            return {
                ...state,
                offersList: action.payload
            }
        default :
            return {
                ...state
            }
    }

}
export default offersReducer;
