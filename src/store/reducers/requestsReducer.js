import { GET_ALL_REQUESTS, GET_REQUEST_FILTERS, GET_REQUESTS_LOADING } from "src/store/actions/actionsTypes";

const initialState = {
    requestsLists: [],
    loadingList: false,
    requestFilters: {
        demand_category: [ { id: "demandCategory_default_value", description: "", default: true, name: "كل العروض", count: "0" } ],
        demand_action: [ { id: "demandAction_default_value", description: "", default: true, name: "كل الاغراض", count: "0" } ],
        demand_country: [ { id: "demandCountry_default_value", description: "", default: true, name: "كل البلاد", count: "0" } ],
        demand_city: [ { id: "demandCity_default_value", description: "", default: true, name: "كل المدن", count: "0" } ],
        demand_neighborhood: [ { id: "demandNeighborhood_default_value", description: "", default: true, name: "كل المناطق", count: "0" } ],
    }
};

const requestsReducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case GET_REQUEST_FILTERS:
            return {
                ...state,
                requestFilters: {
                    ...state.offerFilters,
                    ...action.payload,
                }
            }

        case GET_REQUESTS_LOADING:
            return {
                ...state,
                loadingList: action.payload
            }
        case GET_ALL_REQUESTS :
            return {
                ...state,
                requestsLists: action.payload
            }
        default :
            return {
                ...state
            }
    }

}
export default requestsReducer;
