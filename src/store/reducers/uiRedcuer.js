import {
    CLOSE_TOAST_MESSAGE,
    GET_APP_DIMENSIONS,
    GET_APP_HEADER_HEIGHT,
    GET_APP_SCROLL, HIDE_SPINNER,
    OPEN_APP_DRAWER, SHOW_SPINNER,
    SNACK_BAR_NEW_MESSAGE
} from "src/store/actions/actionsTypes";

const initialState = {
    headerHeight: null,
    scroll: 0,
    height: 0,
    width: 0,
    spinner: false,
    drawerOpen: false,
    snackbar: {
        open: false,
        message: '',
        variant: ''
    }
}


const uiReducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case OPEN_APP_DRAWER:
            return {
                ...state,
                drawerOpen: !state.drawerOpen
            }
        case HIDE_SPINNER:
            return {
                ...state,
                spinner: false
            }
        case SHOW_SPINNER:
            return {
                ...state,
                spinner: true
            }
        case GET_APP_SCROLL:
            return {
                ...state,
                scroll: action.payload
            }
        case CLOSE_TOAST_MESSAGE:
            return {
                ...state,
                snackbar: {
                    open: false,
                    message: '',
                    variant: ''
                }
            }
        case  SNACK_BAR_NEW_MESSAGE:
            console.log( action );
            return {
                ...state,
                snackbar: {
                    ...state.snackbar,
                    duration: action.payload.duration || 5000,
                    open: action.payload.open || true,
                    message: action.payload.message,
                    variant: action.payload.variant
                }
            }
        case GET_APP_DIMENSIONS:
            return {
                ...state,
                ...action.payload
            }
        case  GET_APP_HEADER_HEIGHT:
            return {
                ...state,
                headerHeight: action.payload
            }
        default:
            return {
                ...state
            }
    }
}

export default uiReducer
