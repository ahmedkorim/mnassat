import {
    GET_ALL_MESSAGES,
    GET_ALL_INVOICES,
    GET_ALL_TEAM_MEMBERS,
    SET_USER_DATA,
    GET_ALL_USERS,
    UPDATE_USER_FAVORITES,
    GET_ALL_CLIENTS
} from "src/store/actions/actionsTypes";

const initialState = {
    teamMembers: [],
    allUsers: [],
    messages: [],
    invoices: [],
    clients: [],
    currentUser: {
        auth: false,
        name: '',
        email: '',
        username: '',
        mobile: "",
        description: "",
        custom_picture: "",
        avatar: "",
        favorites: "",
        id: "",
    }
}
const usersReducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case UPDATE_USER_FAVORITES:
            return {
                ...state,
                currentUser: {
                    ...state.currentUser,
                    favorites: action.payload
                }
            }
        case GET_ALL_CLIENTS:
            return {
                ...state,
                clients: action.payload
            }
        case GET_ALL_MESSAGES:
            return {
                ...state,
                messages: action.payload
            }
        case GET_ALL_INVOICES:
            return {
                ...state,
                invoices: action.payload
            }
        case GET_ALL_USERS:
            return {
                ...state,
                allUsers: action.payload
            }
        case SET_USER_DATA:
            return {
                ...state,
                currentUser: {
                    ...state.currentUser,
                    ...action.payload
                }
            }
        case   GET_ALL_TEAM_MEMBERS:
            return {
                ...state,
                teamMembers: action.payload
            }

        default:
            return state
    }
}

export default usersReducer
