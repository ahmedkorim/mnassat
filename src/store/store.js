import axios                                     from 'axios';
import { applyMiddleware, compose, createStore } from "redux";
import thunk                                     from 'redux-thunk';
import { getCookie }                             from "shared/auth/cookies.helper";
import rootReducer                               from './reducers';

const state = {};

export const hostName = process.env.NODE_ENV === 'development' ? 'http://localhost' : 'http://162.241.134.28';

const headers = process.env.NODE_ENV === 'development' ? {
    Authorization: 'Bearer ' + getCookie( '__ssr-auth' )
} : {}

export const nativeApi = axios.create( {
    baseURL: hostName + ':4444',
    headers: { ...headers }
} )
const getStore = ( { preloadState } = { preloadState: state } ) => { // Todo : auth flow dependency
    const api = axios.create( {
        baseURL: hostName + ':4443/api',
        headers: {
            ...headers
        }
    } )

    const middleware = [ thunk.withExtraArgument( { api, nativeApi, axios } ) ];

    if ( typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION__ ) {
        const redDevTools = ( window.__REDUX_DEVTOOLS_EXTENSION__() )
        return createStore(
            rootReducer,
            preloadState,
            // applyMiddleware( thunk )
            compose( applyMiddleware( ...middleware ), redDevTools ),
        );
    } else {
        return createStore(
            rootReducer,
            preloadState,
            // applyMiddleware( thunk )
            compose( applyMiddleware( ...middleware ) ),
        );
    }


};

export default getStore

