import { createGlobalStyle } from "styled-components";

export const GlobalStyles = createGlobalStyle`
body{
    min-height: 100vh;
}

input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    /* display: none; <- Crashes Chrome on hover */
    -webkit-appearance: none;
    margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
}

.google__maps-infowindow-PC{
                           
                                
                                    max-width: 480px;
                            padding: .5rem;
    text-align: right;
                              
                                    background: #ffffff;
                                    box-shadow: 3px 0px 13px 5px rgba(29, 29, 29, 0.05);
                                
                                    border-radius: 0px;
           
}
.google__maps-api-price{
    position: absolute;
    bottom: 0px;
    right: 14px;         
                  font-size: 14px!important;

    color: #3C90BE;
    height: 35px;
    border-top: 1px solid #f0f0f0;
    width: 370px;
    padding: .7rem 0 ;
    box-sizing: border-box
}
.google__maps-api-cat{
                        width: 100%;
                        float: right;
                        display: inline;
                        font-size: 14px!important;
                        color: #768082;
                        padding: 0px 14px 0px 0px;
                        text-transform: lowercase;
                        background-color: #fff;
                        box-sizing: border-box;
                        -moz-box-sizing: border-box;
                        
}

input[type=number] {
    -moz-appearance:textfield; /* Firefox */
}
.MainDrawerPaper{
width:240px !important;
background-color: #fff;
height: 100vh;
.content__MainDrawerPaper{
width: 240px;
}
}
.option__content{
font-size:.8rem;
}

*{
font-family: 'Noto Kufi Arabic', sans-serif;
}
@media (min-width:1440px) {
.container{
min-width:1440px !important;
}
}
button:focus , a:focus , *:focus{
outline: none !important;
}
.light__typography{
}

.no-script{
position: absolute;
z-index: 34;
top: 0;
left:0;
background-color: yellow;
text-align: center;
margin: auto;
left:30vw; 
}
`
