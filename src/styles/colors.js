export const mainColor = '#243484';
export const darkMainColor = '#0A2128';

export const lightMain = '#CED1E5';
export const lightGrey = '#EAEBEF'
export const   lightColor = '#F8F8F8'
export const accentColor = '#F4AB05';
export const darkColor = '#0D171D';
export const greyColor = '#787878';
export const gradient = {
    blue: '#2235D0',
    foschia: '#6F1CB6'
}
