const Express = require( "express" );
const fs = require( "fs" );
const path = require( "path" );
const cors = require( 'cors' );
const app = Express()
app.use( cors() )
app.use( Express.static( path.join( __dirname, '.', 'build', 'client' ) ) )
app.use( Express.static( path.join( __dirname, '.', 'build', 'client', 'images' ) ) )
app.use( Express.static( path.join( __dirname, '.', 'public' ) ) )
const axios = require( 'axios' );
const LocaleCache = new Map();

const cache = ( duration = 0, api = false, cb ) => ( req, res, next ) => {
    console.log( 'cacheing' );
    if ( duration !== 0 ) {
        const path = req.path;
        console.log( 'updated' );
        let key = '__express__' + ( req.originalURL || req.url );
        console.log( key, duration );
        let cachedResponse = LocaleCache.get( key )
        if ( cachedResponse ) {
            console.log( ' in cache' );
            res.set( cachedResponse.headers )
            res.send( cachedResponse.body )
        } else {
            console.log( 'not in cache' );
            res.sendAndCash = ( { body, headers = {} } ) => {
                console.log( `cache life ${ duration }` );
                LocaleCache.set( key, { body, headers } )
                setTimeout( async () => {
                    console.log( 'CLEARING CACHE' );
                    console.log( cb )
                    LocaleCache.delete( key )
                    if ( cb ) {
                        console.log( 'redownload data' );
                        const res = await cb();
                        console.log( 'data updateing done' );
                    }

                }, duration * 1000 * 60 )
                res.send( body )
            }
            next()
        }
    } else {
        next()
    }
}

const getData = ( endPoint, page = 1 ) => axios.get( endPoint + `&page=${ page }` )

const controllerCashEndPoint = ( endPoints = [] ) => async ( req, res ) => {
    let _body = {};
    let _headers = {}
    await Promise.all( endPoints.map( ( endPoint ) => {
        return new Promise( async resolve => {
            try {


                const { data, headers } = await getData( `https://gomenassat.com/wp-json/wp/v2/${ endPoint }?per_page=90` )
                _body = { ..._body, [ endPoint ]: data }
                const totalPages = headers[ 'x-wp-totalpages' ];
                const totalListitems = headers[ 'x-wp-total' ];
                let arrPromises = []
                let i = 2;
                for ( i; i <= totalPages; i++ ) {
                    console.log( 'getting page ' + i );
                    arrPromises.push( getData( `https://gomenassat.com/wp-json/wp/v2/${ endPoint }?per_page=90`, i ) )
                }
                const responses = await Promise.all( arrPromises );
                const responsesData = responses.map( i => i.data ).reduce( ( acc, i ) => [ ...acc, ...i ], [] );

                _body = { ..._body, [ endPoint ]: [ ..._body[ endPoint ], ...responsesData ] };

                _headers = {
                    [ 'x-wp-total' ]: totalListitems
                }
                resolve()
            } catch ( e ) {
                resolve()
            }
        } )

    } ) )
    _headers = endPoints.length === 1 ? _headers : {}
    _body = endPoints.length === 1 ? _body[ endPoints ] : _body;
    res.set( _headers )
    if ( res.sendAndCash ) {
        res.sendAndCash( { body: _body, headers: _headers } )

    } else {
        res.set( _headers )
        res.send( _body )

    }

}


app.get( '/api/offers', cache( 30, false, () => axios.get( 'http://localhost:4443/api/offers' ) ), controllerCashEndPoint( [ 'estate_property' ] ) )
app.get( '/api/agents', cache( 30, false, () => axios.get( 'http://localhost:4443/api/agents' ) ), controllerCashEndPoint( [ 'estate_agent' ] ) )
app.get( '/api/requests', cache( 30, false, () => axios.get( 'http://localhost:4443/api/requests' ) ), controllerCashEndPoint( [ 'wpresid_dproperty' ] ) )
app.get( '/api/users', cache( 30, false, () => axios.get( 'http://localhost:4443/api/users' ) ), controllerCashEndPoint( [ 'users' ] ) )
app.get( '/api/demand_city', cache( 60 * 24, false, () => axios.get( 'http://localhost:4443/api/demand_city' ) ), controllerCashEndPoint( [ 'demand_city' ] ) )
app.get( '/api/demand_neighborhood', cache( 60 * 24, false, () => axios.get( 'http://localhost:4443/api/demand_neighborhood' ) ), controllerCashEndPoint( [ 'demand_neighborhood' ] ) )
app.get( '/api/property_city', cache( 60 * 24, false, () => axios.get( 'http://localhost:4443/api/property_city' ) ), controllerCashEndPoint( [ 'property_city' ] ) )
app.get( '/api/property_area', cache( 60 * 24, false, () => axios.get( 'http://localhost:4443/api/property_area' ) ), controllerCashEndPoint( [ 'property_area' ] ) )


app.get( '/api/wpestate_message', controllerCashEndPoint( [ 'wpestate_message' ] ) )


const bootStrap = async () => {
    await Promise.all( [
        axios.get( 'http://localhost:4443/api/offers' ),
        axios.get( 'http://localhost:4443/api/agents' ),
        axios.get( 'http://localhost:4443/api/requests' ),
        axios.get( 'http://localhost:4443/api/users' ),
        axios.get( 'http://localhost:4443/api/wpestate_message' ),
        axios.get( 'http://localhost:4443/api/demand_city' ),
        axios.get( 'http://localhost:4443/api/demand_neighborhood' ),
        axios.get( 'http://localhost:4443/api/property_city' ),
        axios.get( 'http://localhost:4443/api/property_area' ),
        // axios.get( 'http://localhost:4443/api/requests_filter' ),
        //         // axios.get( 'http://localhost:4443/api/offers_filter' ),
    ] )
    console.log( 'APP CACHED THE API' );
}


app.get( '/api/**', cache( 30, true ), async ( req, res, next ) => {
    const url = 'https://gomenassat.com' + req.url.replace( 'api', 'wp-json/wp/v2' )
    const response = await axios.get( url )

    const headers = {
        [ 'x-wp-total' ]: response.headers[ 'x-wp-total' ],
        [ 'x-wp-totalpages' ]: response.headers[ 'x-wp-totalpages' ]
    }
    res.set( headers )
    res.sendAndCash( { body: response.data, headers } )

} )
app.listen( 4443, ( err ) => {
    if ( err ) return console.log( err );
    bootStrap()
    console.log( 'server started http://localhost:4443' );
} )
// todo use nginx
