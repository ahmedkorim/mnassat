const HtmlWebpackPlugin = require( 'html-webpack-plugin' );
const webpack = require( 'webpack' );
const UglifyJsWebpackPlugin = require( 'uglifyjs-webpack-plugin' )
const path = require( 'path' );
const devServer = require( './build-utils/webpackDevServer.config' )
const CopyPlugin = require( 'copy-webpack-plugin' );
const builldPlugins = [
    new webpack.optimize.AggressiveMergingPlugin(),//Merge chunks
    new UglifyJsWebpackPlugin( {
            uglifyOptions: {
                compress: {
                    drop_console: true,
                }
            }

        } )
]
module.exports = ( { entry, mode } = {} ) => {
    console.log( mode );
    const entryFile = entry === 'ssr' ? './index.ssr.js' : './index.hmr.js'
    console.log( entryFile );
    return {
        entry: entryFile,
        mode: mode !== 'prod' ? 'development' : 'production',
        output: {
            path: path.join( __dirname, '.', 'build', 'client' ),
            filename: 'bundle.js',
            publicPath: '/'
        },
        resolve: {
            descriptionFiles: [ 'package.json' ],
            alias: {
                'react-dom': path.resolve( path.join( __dirname, '.', 'node_modules', '@hot-loader/react-dom' ) ),
                'src': path.resolve( __dirname, '.', 'src' ),
                'react-hot-loader': path.resolve( __dirname, 'node_modules', 'react-hot-loader' ),
                'assets': path.resolve( __dirname, 'src', 'assets' ),
                'target': path.resolve( __dirname, 'node_modules' ),
                'shared': path.resolve( __dirname, 'shared' ),
            },
        },
        devServer: {
            ...devServer
        },
        stats: 'errors-only',
        devtool: 'source-map',
        module: {
            rules: [
                {
                    test: /\.js$/,
                    // exclude: es5 ? /node_modules\/(?!(@material-ui|react-spring)\/).*/ : /node_modules|packages/,
                    ...( mode === 'prod' ? {} : { exclude: /node_modules/ } ),
                    use: [ {
                        loader: 'babel-loader?cacheDirectory',
                        options: {
                            presets: [ [ "@babel/preset-env", {
                                targets: {
                                    "chrome": "58",
                                    "ie": "11"
                                }
                            } ],
                                "@babel/preset-react" ],
                            plugins: [
                                ...( mode === 'prod' ? [] : [ "react-hot-loader/babel" ] ),
                                [
                                    "@babel/plugin-proposal-class-properties",
                                    {
                                        "loose": true
                                    }
                                ],

                            ],

                        }
                    }
                    ]
                },
                {
                    test: /\.(png|jpe?g|gif|svg)$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: '[path][name].[ext]',
                                outputPath: 'images',
                                context: 'src/assets/Images'
                            },
                        },
                    ],
                }
            ]
        }, plugins: [
            new HtmlWebpackPlugin( {
                template: "public/index.html"
            } ),
            new webpack.DefinePlugin( {
                'process.env.NODE_ENV': JSON.stringify( mode !== 'prod' ? 'development' : 'production' )
            } ),
            new CopyPlugin( [
                { from: './src/assets/Images/mapAssets', to: './Images/mapAssets' },
            ] ),
            new webpack.NamedModulesPlugin(),
            ...( mode === 'prod' ? builldPlugins : [] )
        ]
    }
}
