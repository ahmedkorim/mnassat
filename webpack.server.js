const HtmlWebpackPlugin = require( 'html-webpack-plugin' );
const webpack = require( 'webpack' );
const UglifyJsWebpackPlugin = require( 'uglifyjs-webpack-plugin' )
const path = require( 'path' )
module.exports = ( { entry, mode } = {} ) => ( {
    entry: './server.js',
    mode: 'development',
    target: "node",
    output: {
        filename: 'bundle.js',
        path: path.join( __dirname, 'build', 'server' )
    },
    stats: 'errors-only',
    resolve: {
        alias: {
            'react-dom': path.resolve( path.join( __dirname, '.', 'node_modules', '@hot-loader/react-dom' ) ),
            'src': path.resolve( __dirname, '.', 'src' ),
            'react-hot-loader': path.resolve( __dirname, 'node_modules', 'react-hot-loader' ),
            'assets': path.resolve( __dirname, 'src', 'assets' ),
            'target': path.resolve( __dirname, 'node_modules' ),
            'shared': path.resolve( __dirname, 'shared' ),
        }
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude:  /node_modules|packages/,
                use: [ {
                    loader: 'babel-loader?cacheDirectory',
                    options: {
                        presets: [ [ "@babel/preset-env", {
                            targets: {
                                "chrome": "58",
                                "ie": "11"
                            }
                        } ],
                            "@babel/preset-react" ],
                        plugins: [
                            ...( mode === 'prod' ? [] : [ "react-hot-loader/babel" ] ),
                            [
                                "@babel/plugin-proposal-class-properties",
                                {
                                    "loose": true
                                }
                            ],

                        ],

                    }
                }
                ]
            },
            {
                test: /\.(png|jpe?g|gif|svg)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[path][name].[ext]',
                            outputPath: 'images',
                            context: 'src/assets/Images'
                        },
                    },
                ],
            },
        ]
    }, plugins: [
        new UglifyJsWebpackPlugin(),
        new webpack.DefinePlugin( {
            'process.env.NODE_ENV': JSON.stringify( mode !== 'prod' ? 'development' : 'production' )
        } ),
    ]
} )
